<?php

use App\Models\Imagem;
use App\Models\InformacoesUtilizador;
use App\Models\Saga;
use App\Models\Utilizador;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();
        Model::unguard();

        $user = Utilizador::create(['nome' => 'Administrador', 'login' => 'admin', 'email' => 'admin@gmail.com', 'password' => 'admin' ]);;
        $user->Permissoes()->create(['is_Admin' => 1, 'is_Revisor' => 1, 'is_Redator' => 1, 'is_Editor_Noticias' => 1, 'is_Midia_Uploader'  => 1, 'is_Fan_Area' => 1 ]);
        $informacoesDados = InformacoesUtilizador::$defaults; $informacoesDados['nick'] = 'Admin';
        $user->Informacoes()->create($informacoesDados);
        $user->Informacoes->RelacaoImagem()->create(Imagem::$defaults);

        $user = Utilizador::create(['nome' => 'Tiago Pereira', 'login' => 'WhiteHakki', 'email' => 'whitehakki@gmail.com', 'password' => '321' ]);;
        $user->Permissoes()->create(['is_Admin' => 1, 'is_Revisor' => 1, 'is_Redator' => 1, 'is_Editor_Noticias' => 1, 'is_Midia_Uploader'  => 1, 'is_Fan_Area' => 1]);
        $user->Informacoes()->create([ 'nick' => 'WhiteHakki', 'dataNascimento' => '1995-07-03', 'twitter' => null, 'facebook' => null, 'googleplus'  => null, 'youtube' => null ]);
        $user->Informacoes->RelacaoImagem()->create(Imagem::$defaults);

        $formato = $user->Formatos()->create(['descricao' => 'HD']);
        $formato->RelacaoImagem()->create(['nome' => 'formato_HD.png', 'imgur' => false, 'deletehash' => '']);
        $formato = $user->Formatos()->create(['descricao' => 'HQ']);
        $formato->RelacaoImagem()->create(['nome' => 'formato_HQ.png', 'imgur' => false, 'deletehash' => '']);
        $formato = $user->Formatos()->create(['descricao' => 'MQ']);
        $formato->RelacaoImagem()->create(['nome' => 'formato_MQ.png', 'imgur' => false, 'deletehash' => '']);
        $formato = $user->Formatos()->create(['descricao' => 'BQ']);
        $formato->RelacaoImagem()->create(['nome' => 'formato_BQ.png', 'imgur' => false, 'deletehash' => '']);
        $formato = $user->Formatos()->create(['descricao' => 'Stream']);
        $formato->RelacaoImagem()->create(['nome' => 'formato_Stream.png', 'imgur' => false, 'deletehash' => '']);

        $saga = $user->Sagas()->create(['titulo' => 'East Blue', 'numero' => '1']);
        $saga->RelacaoImagem()->create(Imagem::$defaults);
        $saga = $user->Sagas()->create(['titulo' => 'Baroque Works', 'numero' => '2']);
        $saga->RelacaoImagem()->create(Imagem::$defaults);

        Model::reguard();
    }
}
