<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaFanfics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fanfics', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('slug')->unique()->nullable();

            $table->string('autor');
            $table->string('link_original');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('fanficsstatus', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fanfic_id')->unsigned()->index();
            $table->foreign('fanfic_id')->references('id')->on('fanfics')->onDelete('cascade');

            $table->boolean('em_andamento')->default(false);
            $table->boolean('concluida')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tags_fanfics', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('fanfic_tag', function (Blueprint $table) {
            $table->integer('fanfic_id')->unsigned()->index();
            $table->foreign('fanfic_id')->references('id')->on('fanfics')->onDelete('cascade');

            $table->integer('fanfictag_id')->unsigned()->index();
            $table->foreign('fanfictag_id')->references('id')->on('tags_fanfics')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fanfic_tag');
        Schema::drop('tags_fanfics');
        Schema::drop('fanficsstatus');
        Schema::drop('fanfics');
    }
}
