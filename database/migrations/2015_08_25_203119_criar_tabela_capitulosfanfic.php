<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaCapitulosfanfic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capitulosfanfic', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('slug')->unique()->nullable();

            $table->integer('numero');
            $table->string('link_original');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('fanfic_id')->unsigned()->index();
            $table->foreign('fanfic_id')->references('id')->on('fanfics')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('capitulosfanfic');
    }
}
