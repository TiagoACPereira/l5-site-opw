<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaCanaisvlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canaisvlog', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');
            $table->string('slug')->unique()->nullable();

            $table->string('channel')->nullable();
            $table->boolean('channel_id')->default(false);

            $table->longText('channel_info');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('canaisvlog');
    }
}
