<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('nome');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tagables', function (Blueprint $table) {
            $table->integer('tag_id'); //nome da class Tag
            $table->integer('tagable_id');
            $table->string('tagable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tagables');
        Schema::drop('tags');
    }
}
