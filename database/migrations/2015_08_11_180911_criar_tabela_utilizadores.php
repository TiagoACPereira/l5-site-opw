<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaUtilizadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('settings');
            $table->boolean('active')->default(true);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('usersInformations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('nick')->nullable();
            $table->string('slug')->unique()->nullable();

            $table->date('dataNascimento')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('googleplus')->nullable();
            $table->string('youtube')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('usersPermissions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->boolean('is_Admin')->default(false);
            $table->boolean('is_Revisor')->default(false);
            $table->boolean('is_Redator')->default(false);
            $table->boolean('is_Editor_Noticias')->default(false);
            $table->boolean('is_Midia_Uploader')->default(false);
            $table->boolean('is_Fan_Area')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usersPermissions');
        Schema::drop('usersInformations');
        Schema::drop('users');
    }
}
