<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaConteudos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conteudos', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('strConteudo');
            $table->boolean('forceHost')->default(false);
            $table->boolean('imgur')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('conteudables', function (Blueprint $table) {
            $table->integer('conteudo_id');
            $table->integer('conteudable_id');
            $table->string('conteudable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conteudables');
        Schema::drop('conteudos');
    }
}
