<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->boolean('isDead')->default(false);
            $table->timestamps();
        });

        Schema::create('linkables', function (Blueprint $table) {
            $table->integer('link_id');
            $table->integer('linkable_id');
            $table->string('linkable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('linkables');
        Schema::drop('links');
    }
}
