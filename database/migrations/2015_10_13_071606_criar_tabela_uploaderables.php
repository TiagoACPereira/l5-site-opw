<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaUploaderables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaderables', function (Blueprint $table) {
            $table->integer('utilizador_id'); //nome da class uploader
            $table->integer('uploaderable_id');
            $table->string('uploaderable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uploaderables');
    }
}
