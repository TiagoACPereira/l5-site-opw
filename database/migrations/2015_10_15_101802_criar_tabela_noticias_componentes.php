<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaNoticiasComponentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias_componentes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('noticia_id')->unsigned()->index();
            $table->foreign('noticia_id')->references('id')->on('noticias')->onDelete('cascade');

            $table->integer('posicao');
            $table->boolean('noticia_completa')->default(false);

            $table->string('tipo_componente');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('noticia_componenteables', function (Blueprint $table) {
            $table->integer('noticia_componente_id');
            $table->integer('noticia_componenteable_id');
            $table->string('noticia_componenteable_type');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('noticia_componenteables');
        Schema::drop('noticias_componentes');
    }
}
