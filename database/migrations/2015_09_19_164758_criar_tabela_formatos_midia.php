<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaFormatosMidia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formatosmidia', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('formato_id')->unsigned()->index();
            $table->foreign('formato_id')->references('id')->on('formatos')->onDelete('cascade');

            $table->integer('index')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('formatomidiables', function (Blueprint $table) {
            $table->integer('formato_midia_id');
            $table->integer('formatomidiable_id');
            $table->string('formatomidiable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('formatomidiables');
        Schema::drop('formatosmidia');
    }
}
