<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaImagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagens', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome')->nullable()->default(null);
            $table->boolean('imgur')->default(false);
            $table->string('deletehash')->default('');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('imageables', function (Blueprint $table) {
            $table->integer('imagem_id');
            $table->integer('imageable_id');
            $table->string('imageable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imageables');
        Schema::drop('imagens');
    }
}
