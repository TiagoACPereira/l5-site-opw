<?php

return [
    'client_id' => env('IMGUR_CLIENT_ID', 'CliendId'),

    'client_secret' => env('IMGUR_CLIENT_SECRET', 'ClientSecret'),

    'imgur_url' => env('IMGUR_URL', 'imgur.com'),
];
