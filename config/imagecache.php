<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */
   
    'route' => 'img',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => array(
        public_path('upload/temp'),
        public_path('upload/slider'),
        public_path('upload/parceiros'),
        public_path('upload/avatars'),
        public_path('upload/sistema'),
        public_path('upload/infos'),
        public_path('upload/fanfics/capitulos/'),
        public_path('upload/teorias/'),
        public_path('upload/letras/'),
        public_path('upload/tutoriais/'),
        public_path('upload/saga/'),
        public_path('upload/formato/'),
        public_path('upload/episodios/'),
        public_path('upload/especiais/'),
        public_path('upload/filmes/'),
        public_path('upload/ovas/'),
        public_path('upload/plataformas/'),
        public_path('upload/jogos/'),
        public_path('upload/hentais/'),
        public_path('upload/osts/'),
        public_path('upload/volumes/'),
        public_path('upload/capitulos/'),
        public_path('upload/noticias/'),
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */
   
    'templates' => array(
//        'small' => 'Intervention\Image\Templates\Small',
//        'medium' => 'Intervention\Image\Templates\Medium',
//        'large' => 'Intervention\Image\Templates\Large',
        'lista' => 'App\Image\Filters\FiltroLista',
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */
   
    'lifetime' => 43200,

);
