@inject('dados', 'App\Dados')
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="WhiteHakki">

    {!! SEO::generate() !!}


    <link rel="shortcut icon" href="favicon.ico">

    {!! Html::style( elixir('css/all.css') ) !!} {!! Html::script( elixir('js/topo.js') ) !!}
</head>
<body>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&amp;appId=409028339208131";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <iframe style="width:100%; height:60px; margin:0; padding:0; border:0; outline:none;" name="playerincorporado"
            src="http://radioblast.com.br/player/blue"></iframe>
    <div id="tudo">
        <div id="topo" style="background-image:url('imagens/topo/topo{{mt_rand(1, 3)}}.png');">
            <div id="last-ep">

                <a href="?pagina=midia&tipo=episodio&numero={{ $dados->ultimoEpisodio()['numero'] }}">
                    <img alt="img_ultimo_episodio" style="border:0;"
                         src="../uploads/screen/{{ $dados->ultimoEpisodio()['imagem'] }}"
                         width="150px" height="113px">
                </a>
            </div>
            <div id="last-ep-num">{{ $dados->ultimoEpisodio()['numero'] }}</div>
            <div id="last-cap">
                <a href="?pagina=midia&tipo=manga&numero={{ $dados->ultimoManga()['numero'] }}">
                    <img alt="img_ultimo_capitulo" style="border:0;"
                         src="../uploads/manga/cap/{{ $dados->ultimoManga()['imagem'] }}"
                         width="150px" height="113px"/>
                </a>
            </div>
            <div id="last-cap-num">{{ $dados->ultimoManga()['numero'] }}</div>
            <div id="arquivo-de-news">
                <a href="{{ route('arquivo de noticias') }}" class="arquivo"></a>
            </div>
            <div id="topo-logo">
                <a href="{{ Request::url() }}" class="logo"></a>
            </div>
            <div id="headlines">
                @if ($dados->listarUltimasNoticias())
                    @foreach ($dados->listarUltimasNoticias() as $headline)
                        <span class="data">&nbsp;&nbsp;&nbsp;[{{ $headline['criado'] }}]</span>
                        <a class="azul" href="?pagina=comentario&noticia={{ $headline['id'] }}">
                            <span class="titulo">{{ $headline['titulo'] }}</span>
                        </a>
                        <br/>
                    @endforeach
                @else
                    nada cadastrado ainda
                @endif
            </div>
            <div id="fast-menu">
                <span class="principal">
                    <a href="{{ route('noticias') }}">Principal</a>
                </span>
                <span class="episodios">
                    <a href="{{ route('sagas') }}">Episódios</a>
                </span>
                <span class="mangas">
                    <a href="{{ route('mangas') }}">Mangás</a>
                </span>
                <span class="leitor-online">
                    <a href="http://leitor.onepieceworld.com.br/" target="_blank">Leitor Online</a>
                </span>
                <span class="equipe">
                    <a href="{{ route('equipe') }}">Equipe</a>
                </span>
                <span class="galeria">
                    <a href="http://galeria.onepieceworld.com.br/galeria/" target="_blank">Galeria</a>
                </span>
                <span class="contato">
                    <a href="{{ route('faleconosco') }}">Contato</a>
                </span>
            </div>
            <div id="social">
                <a href="https://www.facebook.com/onepieceworldbrasil" target="_blank" class="facebook"></a>
                <a href="https://twitter.com/OnePiece_World" target="_blank" class="twitter"></a>
                <a href="https://plus.google.com/100738943550785679412" target="_blank" class="google"></a>
                <a href="http://www.youtube.com/OnePiece_World" target="_blank" class="utube"></a>
            </div>
            <div id="histats"></div>
            <form method="post" action="http://poll.pollcode.com/63672418">
                <div id="enquete">
                    <span class="pergunta" style="display: block; text-align: -webkit-center;">O que você achou do sistema de comentários?</span>
                    <img alt="Disqus Logo"
                         src="http://upload.wikimedia.org/wikipedia/commons/5/5b/Disqus_logo_official_-_blue_on_transparent_background.png"
                         width="54px" style="float:right; opacity:0.3;"/>
                    <br>
                    <span class="respostas">
                        <input type="radio" name="answer" value="1" id="answer636724181">
                        <label for="answer636724181">Ótimo! Vou já comentar!</label>
                        <br/> <br/>
                        <input type="radio" name="answer" value="2" id="answer636724182">
                        <label for="answer636724182">Tanto faz! Não vou usar mesmo!</label>
                        <br/><br/>
                        <input type="radio" name="answer" value="3" id="answer636724183">
                        <label for="answer636724183">Tira isso logo do site!</label>
                        <br/>
                    </span>
                </div>
                <div id="enquete-botoes">
                    <input type="image" src="imagens/enquete/botao.png" value=" Vote " class="votar">
                    <input type="image" src="imagens/enquete/botao.png" name="view" value=" View " class="resultado">
                </div>
            </form>
            <div id="last-bio">
                @if ($dados->listarUltimasBiografias())
                    @foreach ($dados->listarUltimasBiografias() as $bio)
                        <a href="{{ route('informacao', ['id' => $bio['id']]) }}">{{ $bio['criado'] }} {{ $bio['nomesecao'] }}</a>
                        <br/>
                    @endforeach
                @else
                    nada cadastrado ainda
                @endif
            </div>
            <div id="bio-screen">
                @if ($dados->listaUltimaBiografia())
                    <a href="{{ route('biografia', ['id' => $dados->listaUltimaBiografia()['id']]) }}">
                        <img src="uploads/biografias/{{ $dados->listaUltimaBiografia()['ipersonagem'] }}"
                             style="width:160px; position:absolute; top: -30px; left: -35px; ">
                    </a>
                @endif
            </div>
            <div id="last-video">
                <div style="text-align: center;">
                    <div class="youtube" id="2VSoqEaHBhY" style="width: 186px; height: 107px;"></div>
                </div>
            </div>
            <div id="last-info">
                @if ($dados->listarUltimasInformacoes())
                    @foreach ($dados->listarUltimasInformacoes() as $info)
                        <a href="{{ route('informacao', ['id' => $info['id']]) }}">[{{ $info['criado'] }}] {{ $info['nomesecao'] }}</a>
                        <br/>
                    @endforeach
                @else
                    nada cadastrado ainda
                @endif
            </div>
            <div id="publicidade-topo">
                <iframe id="id01_495071"
                        src="http://www.play-asia.com/paOS-38-19-1%2Cdccdb6%2Cdotted%2C0%2C0%2C0%2C0%2Ce4dbc9%2CFFFFFF%2Cleft%2C0%2C1-39-1-49-en-76-5-36-one+piece-70-4k7e-6-2-78-1o-29-13_333-90-wb9f-40-1-44-728px.html?inlinelogo=1&url=iframe_banner"
                        style="border-style: dotted; border-width: 1px; border-color: #dccdb6; padding: 0; margin: 0;"
                        scrolling="no" frameborder="1" width="728px" height="101"></iframe>
                <script type="text/javascript"> var t = "";
                    t += window.location;
                    t = t.replace(/#.*$/g, "").replace(/^.*:\/*/i, "").replace(/\./g, "[dot]").replace(/\//g, "[obs]").replace(/-/g, "[dash]");
                    t = encodeURIComponent(encodeURIComponent(t));
                    var iframe = document.getElementById("id01_495071");
                    iframe.src = iframe.src.replace("iframe_banner", t); </script>
            </div>
        </div>
        @include('partials.menu_lateral', ['dados' => $dados])
        <div id="aoladodomenu1">
            <div id="slideshow">
                <div id="slider">
                    <div id="slides">
                        @if ($dados->listarSlides())
                            @foreach ($dados->listarSlides() as $slide)
                                <a href="{{ $slide['url'] }}" target="_blank">
                                    <img src="uploads/slider/{{$slide['imagem']}}"/>
                                </a>
                            @endforeach
                        @else
                            nada cadastrado ainda
                        @endif
                    </div>
                </div>
            </div>
            <div id="conteudo">
                <div id="conteudo-topo"></div>
                <div id="conteudo-bg">
                    @yield('conteudo')
                </div>
                <div id="conteudo-footer"></div>
                <div id="pub-conteudo">
                    <div id="pub-conteudo-img">
                        <iframe id="id01_305711"
                                src="http://www.play-asia.com/paOS-38-19-0%2Cdccdb6%2Cdotted%2C0%2C0%2C0%2C0%2Ce4dbc9%2CFFFFFF%2Cleft%2C0%2C0-39-2-49-en-76-3-36-one+piece-30-2-70-3lnp-6-2-78-1o-29-13_255_333-90-wb9f-40-5-44-300px.html?url=iframe_banner"
                                style="border-style: dotted; border-width: 0; border-color: #dccdb6; padding: 0; margin: 0; scrolling: no; frameborder: 0;"
                                scrolling="no" frameborder="0" width="300px" height="254"></iframe>
                        <script type="text/javascript">
                            var t = "";
                            t += window.location;
                            t = t.replace(/#.*$/g, "").replace(/^.*:\/*/i, "").replace(/\./g, "[dot]").replace(/\//g, "[obs]").replace(/-/g, "[dash]");
                            t = encodeURIComponent(encodeURIComponent(t));
                            var iframe = document.getElementById("id01_305711");
                            iframe.src = iframe.src.replace("iframe_banner", t);
                        </script>
                    </div>
                </div>
            </div>
            <div id="menu2">
                <div id="parceiros"></div>
                <div id="menu2-bg">
                    <div id="parca">
                        @if($dados->listarParceiros())
                            @foreach ($dados->listarParceiros() as $parceiro)
                                <?php //TODO: ADICIONAR VISITAR PHP ?>
                                <a href="visitar.php?id={{ $parceiro['id'] }}" target="_blank">
                                    <img src="uploads/parceiros/{{ $parceiro['botao'] }}" width="88" height="31"
                                         title="{{ $parceiro['parceiro'] }} - {{ $parceiro['hits'] }} cliques">
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="menu2-footer"></div>
                <div id="facebook"></div>
                <div id="menu2-bg">
                    <div class="fb-like-box" data-href="https://www.facebook.com/onepieceworldbrasil" data-width="194"
                         data-height="390" data-show-faces="true" data-header="false" data-stream="false"
                         data-show-border="false"></div>
                </div>
                <div id="menu2-footer"></div>
            </div>
        </div>
        <div id="footer"></div>
    </div>
    {!! Html::script( elixir('js/footer.js') ) !!}
</body>
</html>