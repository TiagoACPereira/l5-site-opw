@extends('auth.app')

@section('content')
    <div class="container" style="max-width: 400px;">
        <br />
        <style>
            body { padding-top: 40px; padding-bottom: 40px; background-color: #eee; } .form-signin { max-width: 330px; padding: 15px; margin: 0 auto; } .form-signin .form-signin-heading, .form-signin .checkbox { margin-bottom: 10px; } .form-signin .checkbox { font-weight: normal; } .form-signin .form-control { position: relative; height: auto; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 10px; font-size: 16px; } .form-signin .form-control:focus { z-index: 2; } .form-signin input[type="email"] { margin-bottom: -1px; border-bottom-right-radius: 0; border-bottom-left-radius: 0; } .form-signin input[type="password"] { margin-bottom: 10px; border-top-left-radius: 0; border-top-right-radius: 0; }
        </style>
        <form class="form-signin" method="POST" action="{{ route('sistema::login') }}">
            {!! csrf_field() !!}
            <h2 class="form-signin-heading">{{ trans('layout.auth.login.panel-heading') }}</h2>

            <label for="login" class="sr-only">{{ trans('layout.auth.login.label-login-input') }}</label>
            <input type="text" id="login" name="login" class="form-control" placeholder="{{ trans('layout.auth.login.label-login-input') }}" required autofocus
                   value="{{ old('login') }}">
            <label for="inputPassword" class="sr-only">{{ trans('layout.auth.login.label-password-input') }}</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="{{ trans('layout.auth.login.label-password-input') }}" required>

            <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me" name="remember">{{ trans('layout.auth.login.label-remember-check') }}
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">{{ trans('layout.auth.login.button-submit') }}</button>
        </form>

        @include('errors.list')
        <br />
    </div><!-- /.container -->
    <br />
@stop