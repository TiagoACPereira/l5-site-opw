@extends('sistema.app')

@section('content')
    {!! Form::model( $usuario = new \App\Models\Utilizador, [ 'method' => 'POST', 'route' => 'sistema::usuarios::guardar']) !!}
    <h2 style="padding-bottom: 15px;">{{ trans('layout.auth.register.panel-heading') }}</h2>
    <div style="max-width: 330px;">
        <label for="nome" class="label label-default">{{ trans('layout.auth.register.label-name-input') }}</label>
        <input type="text" class="form-control" placeholder="{{ trans('layout.auth.register.label-name-input') }}" name="nome" value="{{ old('nome') }}">
        <br/>
        <label for="email" class="label label-default">{{ trans('layout.auth.register.label-email-input') }}</label>
        <input type="email" class="form-control" placeholder="{{ trans('layout.auth.register.label-email-input') }}" name="email" value="{{ old('email') }}">
        <br/>
        <label for="login" class="label label-default">{{ trans('layout.auth.register.label-login-input') }}</label>
        <input type="text" class="form-control" placeholder="{{ trans('layout.auth.register.label-login-input') }}" name="login" value="{{ old('login') }}">
        <br/>
        <label for="password" class="label label-default">{{ trans('layout.auth.register.label-password-input') }}</label>
        <input type="password" class="form-control" placeholder="{{ trans('layout.auth.register.label-password-input') }}" name="password">
        <br/>
        <label for="password_confirmation" class="label label-default">{{ trans('layout.auth.register.label-confirm-password-input') }}</label>
        <input type="password" class="form-control" placeholder="{{ trans('layout.auth.register.label-confirm-password-input') }}" name="password_confirmation">
        <br/>
        <div>
            <button type="submit" class="btn btn-primary">{{ trans('layout.auth.register.button-submit') }}</button>
            <a class="btn btn-primary" href="{{ route('sistema::usuarios::index') }}" role="button">{{ trans('layout.auth.register.button-cancel') }}</a>
        </div>
    </div>
    {!! Form::close() !!}

    @include('errors.list')

@stop

@section('footer')
@stop