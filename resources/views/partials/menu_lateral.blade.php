<div id="menu1">
    <div id="menu-principal"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            <li><a href="{{ route('noticias') }}">Home / News</a></li>
            <li><a href="{{ route('equipe') }}">Equipe</a></li>
            <li><a href="{{ route('faleconosco') }}">Fale Conosco</a></li>
            <li><a href="{{ route('arquivo de noticias') }}">Arquivo de Notícias</a></li>
            <li><a href="{{ route('linkoff') }}">Reportar Link Off</a></li>
            <li><a href="http://www.toei-anim.co.jp/tv/onep" target="_blank">Site Oficial</a></li>
        </ul>
    </div>
    <div id="menu-multimidia"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            <li><a href="{{ route('sagas') }}">Episódios</a></li>
            <li><a href="{{ route('epionline') }}"><b>Episódios Online</b></a> <i><span
                            style="opacity:0.6">(beta)</span></i></li>
            <li><a href="{{ route('mangas') }}">Mangá</a></li>
            <li><a href="{{ route('filmes') }}">Filmes</a></li>
            <li><a href="{{ route('especiais') }}">Especiais</a></li>
            <li><a href="{{ route('ovas') }}">OVAs</a></li>
            <li><a href="http://leitor.onepieceworld.com.br/" target="_blank">Leitor de Mangás</a></li>
            <li><a href="http://galeria.onepieceworld.com.br/galeria/" target="_blank">Galeria de Imagens</a>
            </li>
            <li><a href="{{ route('hentais') }}">Hentai Doujinshi</a> <font color=#b62849>[+18]</font></li>
            <li><a href="{{ route('jogos') }}">Jogos</a></li>
            <li><a href="{{ route('roms') }}">ROMs</a></li>
            <li><a href="{{ route('ost') }}">OST</a></li>
            <li><a href="{{ route('letras') }}">Letras / Lyrics</a></li>
        </ul>
    </div>
    <div id="menu-episodios"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            @forelse ($dados->listarSagas() as $saga)
                <li>
                    <a href="{{ route('saga', ['id' => $saga['id']]) }}">{{ $saga['titulo'] }}</a>
                </li>
            @empty
                nada cadastrado ainda
            @endforelse
        </ul>
    </div>
    <div id="menu-infos"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            <li><a href="{{ route('informacao', ['id' => '1bfec4dc7448a2b31a0f9e70b18f29d18bc72ff6']) }}">Alabasta</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '89b631d936359f9a6b18d2e7233fc4249cb3d02e']) }}">As
                    Árvores Adão e
                    Eva</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '5a0af4cd6016c4af6fac308dfde89857dd912acb']) }}">Den
                    Den Mushi</a></li>
            <li><a href="{{ route('informacao', ['id' => '2763a3eaad64db0beeea21e2ca7aa5e1a6ebf9cd']) }}">Determinação
                    dos D.</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '467415ab7274eb6b9f3cce1cdd7e1bd565c7e4f8']) }}">Doskoi
                    Panda</a></li>
            <li><a href="{{ route('informacao', ['id' => '322570fdae4259ce11ad04c7b7ea998ff812c3b3']) }}">Eiichiro
                    Oda (o
                    autor)</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => 'ddbdd9e4109828635af622b2de0b9d96352d23a7']) }}">Frutas
                    do Diabo</a>
                <font
                        color=#b62849>|</font> <a href="{{ route('todasfrutas') }}">Lista</a></li>
            <li><a href="{{ route('informacao', ['id' => 'a2ce9558005ac5b32f585d0566bcebd78d49ddae']) }}">Gyojin
                    & Ningyo</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '0c0894193a1563e3c50c3f0051ff778b2378e3d9']) }}">Mangá
                    no Brasil</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '4f7532e6a4fa805cb3343e96ee7ef2e140f49f01']) }}">Mangá
                    no Japão</a></li>
            <li>
                <a href="{{ route('informacao', ['id' => '4e19124df6979218c4c00e4b872ce61b12a32bca']) }}">Meitou</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '8b22aefd3b366acf8a373cc958de527d1033c903']) }}">Pandaman</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '1ff77bed3a43519430ff88b4e23465e0170e1bfb']) }}">Resumo
                    dos
                    Epis./Cap.</a>
            </li>
            <li><a href="{{ route('informacao', ['id' => '783300d72d96336a2b44095cda260ccf46f60e93']) }}">Seiyuus
                    (Os
                    Dubladores)</a>
            </li>
            <li><a href="{{ route('todainfo') }}"><b>+++ Informações +++</b></a></li>
        </ul>
    </div>
    <div id="menu-biografias"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            @if ($dados->listarBiografias())
                @foreach ($dados->listarBiografias() as $bio)
                    <li>
                        <a href="{{ route('biografia', ['id' => $bio['id']]) }}">{{ $bio['nomesecao'] }}</a>
                    </li>
                @endforeach
                <li><a href="{{ route('todabio') }}"><b>+++ biografias +++</b></a></li>
            @else
                nada cadastrado ainda
            @endif
        </ul>
    </div>
    <div id="menu-fanarea"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            <li><a href="{{ route('amv') }}">AMVs</a></li>
            <li><a href="http://galeria.onepieceworld.com.br/galeria/index.php/Fanarts">Fanarts</a></li>
            <li><a href="{{ route('fanfics') }}">Fanfics</a></li>
            <li><a href="{{ route('vlog') }}">Vlog</a></li>
        </ul>
    </div>
    <div id="menu-tutoriais"></div>
    <div id="menu1-bg">
        <ul style="margin: 0; padding: 0; list-style-type: none;">
            @forelse($dados->listarTutoriais() as $tuto)
                <li>
                    <a href="{{ route('tutorial', ['id' => $tuto['id']]) }}">{{ $tuto['nomesecao'] }}</a>
                </li>
            @empty
                nada cadastrado ainda
            @endforelse
        </ul>
    </div>
    <div id="menu-footer"></div>
</div>
