@extends('app')

@section('conteudo')
    <h2>Not&iacute;cias da One Piece World&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span style="color:#55c8dd; font-size:18px; text-align:right; opacity:0.8;">
            (27/09/2007 - {{ date('d/m/Y') }})
        </span>
    </h2>
    @forelse( $noticias as $noticia )
        <div id="news">
            <div id="news-topo">
                <h1>{{ $noticia['titulo'] }}</h1>

                <div id="news-data">Data: {{ $noticia['criado'] }}</div>
                <div id="news-usuario">Por: {{ $noticia['autor'] }}</div>
            </div>
            <div id="news-avatar">
                <img src="../imagens/avatar/{{ $noticia['avatar'] }}" width="148" height="85"/>
            </div>
            <div id="news-bg">
                <div id="news-conteudo">
                    {!! $noticia['conteudo'] !!}
                </div>
            </div>
            <div id="news-footer">
                <div id="share">
                    <div class="fb-like"
                         data-href="http://www.onepieceworld.com.br/?pagina=comentario&noticia={{ $noticia['id'] }}"
                         data-send="false" data-layout="button_count" data-width="110" data-show-faces="false"
                         data-font="segoe ui"></div>
                </div>
                <div id="coment2"><a href="?pagina=comentario&noticia={{ $noticia['id'] }}"
                                     data-disqus-identifier="{{ $noticia['id'] }}" class="branco">comente aqui</a></div>
            </div>
        </div>
    @empty
    @endforelse

    <center><a class="arquivo" href="?pagina=arquivodenoticias"><b>(Arquivo de Not&iacute;cias)</b></a></center>
@stop