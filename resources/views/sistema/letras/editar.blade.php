@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Teoria
        </h1>
    </div>

    {!! Form::model( $letra, [ 'method' => 'PATCH', 'route' => ['sistema::letras::actualizar', $letra->slug ]]) !!}

    @include('sistema.letras.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::letras::index',
        'deleteNameRoute' => 'sistema::letras::apagar',
        'slug' => $letra->slug
    ])

    {!! Form::close() !!}
@stop