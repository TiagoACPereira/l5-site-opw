{!! Form::hidden('id', $letra->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">Título</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => 'Título' ]) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.guardar_imagens_links-form')

    <div class="container" style="padding-left: 0; padding-right: 0;">
        @include('sistema.partials.recaptcha')

        @include('sistema.partials.imagem_hospedagem-form', ['label' => 'Hospedagem de Imagens', 'model' => $letra])
    </div>

    <div class="col-xs-12">
        <label class="label label-default" for="strConteudo">Conteudo</label>
        {!! Form::textarea('strConteudo', null, ['data-control' => 'summernote', 'hidden' => '']) !!}
    </div>
</div>

@section('footer')
    @include('sistema.partials.script_summernote')
@stop