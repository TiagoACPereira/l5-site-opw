@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Letra
        </h1>
    </div>

    {!! Form::model( $letra = new \App\Models\Letra, ['method' => 'POST', 'route' => 'sistema::letras::guardar']) !!}

    @include('sistema.letras.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::letras::index'])

    {!! Form::close() !!}

@stop
