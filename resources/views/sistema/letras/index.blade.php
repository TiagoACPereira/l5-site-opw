@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Letras Cadastradas
            <span class="label label-{{ ($numero_letras == 0)? 'danger' : 'primary' }}">{{ $numero_letras }}</span>
            <br/>
            <small><a href="{{ route('sistema::letras::criar') }}">Adicionar Nova Letra</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($letras as $letra)
            <li class="list-group-item {{ ($letra->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>Seção:</strong> {{ $letra->titulo }} |
                    <strong>Cadastrado: </strong> {{ $letra->created_at->diffForHumans() }} |
                    <strong><small>{{ (!$letra->isPublished())? ('A publicar '. $letra->publish_at->diffForHumans()) : ('Publicado '. $letra->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::letras::editar', 'deleteNameRoute' => 'sistema::letras::apagar', 'slug' => $letra->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhuma Letra!</strong>
            </li>
        @endforelse
    </ul>

@stop