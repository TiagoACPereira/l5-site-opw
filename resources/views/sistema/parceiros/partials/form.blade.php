{!! Form::hidden('id', $parceiro->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6">
        <label for="inputNome" class="label label-default">Nome</label>
        {!! Form::text('nome', null, [ 'class' => 'form-control', 'placeholder' => 'Nome', 'required' => '' ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="inputUrl" class="label label-default">Url</label>
        {!! Form::url('url', null, [ 'class' => 'form-control', 'placeholder' => 'URL', 'required' => '' ] ) !!}
        <br/>
    </div>

    @include('sistema.partials.imagem-form', ['label' => 'Hospedagem de Parceiro', 'model' => $parceiro])

    @include('sistema.partials.recaptcha')
</div>