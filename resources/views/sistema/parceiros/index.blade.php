@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Parceiros Cadastrados
            <span class="label label-{{ ($numero_parceiros == 0)? 'danger' : 'primary' }}">{{ $numero_parceiros }}</span>
            <br/>
            <small><a href="{{ route('sistema::parceiros::criar') }}">Adicionar Novo Parceiro</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($parceiros as $parceiro)
            <li class="list-group-item clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <img src="{{ $parceiro->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Parceiro:</strong> {{ $parceiro->nome }} |
                    <strong>Url:</strong>
                    {!! Html::link($parceiro->url) !!} |
                    <strong>Hits:</strong> {{ $parceiro->hits }} |
                    <strong>Cadastrado: </strong> {{ $parceiro->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::parceiros::editar', 'deleteNameRoute' => 'sistema::parceiros::apagar', 'slug' => $parceiro->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhum parceiro!</strong>
            </li>
        @endforelse
    </ul>

@stop