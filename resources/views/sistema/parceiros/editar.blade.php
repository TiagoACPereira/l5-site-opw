@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Parceiro
        </h1>
    </div>

    {!! Form::model( $parceiro, [ 'method' => 'PATCH', 'route' => ['sistema::parceiros::actualizar', $parceiro->id ], 'files' => 'true'  ]) !!}
        <div class="row">
            <div class="col-md-12">
                <label class="label label-default" for="oldImage">Banner Antigo</label>
                <br/>
                <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                     src="{{ $parceiro->getImagem()->getLink() }}" id="oldImage">
            </div>
        </div>

        @include('sistema.parceiros.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::parceiros::index',
            'deleteNameRoute' => 'sistema::parceiros::apagar',
            'slug' => $parceiro->slug
        ])

    {!! Form::close() !!}

@stop
