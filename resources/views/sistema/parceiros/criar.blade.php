@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Parceiro
        </h1>
    </div>

    {!! Form::model( $parceiro = new \App\Models\Parceiro, [ 'method' => 'POST', 'route' => 'sistema::parceiros::guardar', 'files' => 'true']) !!}
        @include('sistema.parceiros.partials.form')

        @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::parceiros::index'])
    {!! Form::close() !!}

@stop
