<span class="pull-right"><button type="button"  onclick="$(this).closest('div').hide('slow', function() {$(this).empty();}); return false;" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></span>
<h4>Criar Plataforma {{$tipo_plataforma}}</h4>
<hr/>
{!! Form::model( $plataforma = new \App\Models\Plataforma, [ 'method' => 'POST', 'route' => 'sistema::midia::plataformas::guardar', 'files' => 'true'  ]) !!}

@include('sistema.plataformas.partials.form', ['tipo' => $tipo_plataforma])

<div class="row">
    <div class="col-md-12" style="padding-bottom: 15px;">
        <button type="submit" id="save" class="btn btn-primary btn-block">Criar <span class='glyphicon glyphicon-ok'></span></button>
    </div>
</div>

{!! Form::close() !!}
