{!! Form::hidden('id', $plataforma->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}
{!! Form::hidden('tipo', $tipo) !!}

<div class="row">

    <div class="col-sm-6 col-md-12">
        <label for="nome" class="label label-default">Nome</label>
        {!! Form::text('nome', null, [ 'class' => 'form-control', 'placeholder' => 'Nome', 'required' => ''] ) !!}
        <br/>
    </div>

    @include('sistema.partials.imagem-form', ['label' => 'Imagem de Plataforma', 'model' => $plataforma, 'classInputImagem' => 'col-md-12 col-sm-6',
    'classInputHost' => 'col-md-12 col-sm-6'])

    @include('sistema.partials.recaptcha', ['recaptchaClass' => 'col-md-12 col-sm-6'])

</div><!-- /.row -->