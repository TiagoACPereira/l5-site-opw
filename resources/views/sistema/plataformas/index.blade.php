@extends('sistema.app')

@section('content')
    <div class="page-header">
        <h1>
            Plataformas Jogos/Roms Cadastrados
            <span class="label label-{{ ($numero_plataformas == 0)? 'danger' : 'primary' }}">{{ $numero_plataformas }}</span><br/>
        </h1>
    </div>
    @forelse($plataformas as $plataforma_tipo => $tipo_plataformas)
        <div class="well" id="plataforma_criar_{{$plataforma_tipo}}" style="display: none; margin-bottom: 10px;">
        </div>
    @empty @endforelse
    <div class="row">
        @forelse($plataformas as $plataforma_tipo => $tipo_plataformas)
            <div class="col-md-6">
                <h3>
                    <strong>Plataforma {{$plataforma_tipo}}</strong><br/>
                    <small><a href="" onclick="_loadView('plataforma_criar_{{$plataforma_tipo}}', '{{route('sistema::midia::plataformas::criar', $plataforma_tipo)}}'); return false;">Adicionar Plataforma {{$plataforma_tipo}}</a></small>
                </h3>
                <ul class="list-group">
                    @forelse($tipo_plataformas as $plataforma)
                        <li class="list-group-item clearfix">
                            <span>
                                <span style="display: inline-block;">
                                    <img style="max-height: 30px;" class="img-thumbnail"
                                         src="{{ $plataforma->getImagem()->getLink('lista') }}" id="oldImage">
                                    <strong>Nome:</strong> {{ $plataforma->nome }}
                                </span>

                                <span class="pull-right" style="vertical-align: middle;">
                                    <button onclick="_show('jogos_plataforma{{$plataforma->id}}'); return false;" type="button" class="btn btn-default btn-sm dropdown-toggle"> <span class="label label-{{ ($plataforma->numeroJogos == 0)? 'danger' : 'primary' }}">{{ $plataforma->numeroJogos}}</span> {{$plataforma_tipo}}s</button>

                                    <?php $random = uniqid(); ?>
                                    <strong>
                                        <a class="btn btn-default btn-sm" onclick="_loadView('editar_plataforma{{$plataforma->id}}', '{{route('sistema::midia::plataformas::editar', $plataforma->slug)}}'); return false;" role="button" href="#">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            ALTERAR
                                        </a>
                                    </strong>
                                    {!! Form::open( [ 'route' => ['sistema::midia::plataformas::apagar', $plataforma->slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="_delete_{{$random}}(); return false;">
                                            <span class='glyphicon glyphicon-trash'></span> REMOVER
                                        </button>
                                    {!! Form::close() !!}
                                    <script>
                                        function _delete_{{$random}}() { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); }
                                    </script>
                                </span>
                            </span>
                        </li>
                        <li class="list-group-item clearfix" id="editar_plataforma{{$plataforma->id}}" style="display: none; background-color: #f5f5f5">

                        </li>

                        @if($plataforma->numeroJogos != 0)
                            <?php $msgTemp = $plataforma->numeroJogos . " ". $plataforma_tipo ." nesta plataforma!!"; ?>
                        @else
                            <?php $msgTemp = "Não foi registado nenhuma " . $plataforma_tipo . " nesta plataforma!"; ?>
                        @endif

                        <li  class="list-group-item jogos_plataforma{{$plataforma->id}} clearfix" style="display: none; background-color: #f5f5f5">
                            <span style="display: inline-block; padding: 6px 12px;">
                                <strong>{{ $msgTemp }}</strong>
                            </span>
                            <span class="pull-right">
                                <a class="btn btn-default btn-sm" role="button"
                                   href="{{ route('sistema::midia::plataformas::jogos::criar', $plataforma->slug) }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Inserir {{$plataforma_tipo}}
                                </a>
                            </span>
                        </li>

                        @foreach($plataforma->jogos as $jogo)
                            <li  class="list-group-item jogos_plataforma{{$plataforma->id}} clearfix" style="display: none; background-color: #f5f5f5">
                                <span style="display: inline-block;">
                                    <img src="{{ $jogo->getImagem()->getLink('lista') }}" id="oldImage">
                                    <strong>Título:</strong> {{ $jogo->titulo }}
                                </span>
                                <span class="pull-right">
                                    <?php $random = uniqid(); ?>
                                    <strong>
                                        <a class="btn btn-default btn-sm" role="button" href="{{ route('sistema::midia::plataformas::jogos::editar', [$plataforma->slug, $jogo->slug]) }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            ALTERAR
                                        </a>
                                    </strong>
                                    {!! Form::open( [ 'route' => ['sistema::midia::plataformas::jogos::apagar', $jogo->slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
                                    <button class="btn btn-danger btn-sm" onclick="_delete(); return false;">
                                        <span class='glyphicon glyphicon-trash'></span> REMOVER
                                    </button>
                                    {!! Form::close() !!}

                                    <script>
                                        function _delete() { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); }
                                    </script>
                                </span>
                            </li>
                        @endforeach
                    @empty
                        <li class="list-group-item">
                            <strong>Não foi registado nenhum Plataforma {{$plataforma_tipo}}!</strong>
                        </li>
                    @endforelse
                </ul>
            </div>
        @empty @endforelse
    </div>

    @include('sistema.partials.script_loadView')
    <script>
        function _show($elementID) {
            $("." + $elementID).toggle('slow');
        }
    </script>

@stop