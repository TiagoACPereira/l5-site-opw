<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="WhiteHakki">

    {!! SEO::generate() !!}

    @include('partials.favicon')

    {!! Html::style( elixir('css/all.css') ) !!}
    {!! Html::style( elixir('css/sistema/all.css') ) !!}
    {!! Html::script( elixir('js/topo.js') ) !!}
    {!! Html::script( elixir('js/sistema/topo.js') ) !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style> #toTop{ position: fixed; bottom: 40px; right: 40px; cursor: pointer; display: none; opacity: 0.95; -moz-opacity: 0.95; -webkit-opacity: 0.95; z-index:99999; } #toTop .fa {margin-right: 5px;} </style>
    <script>$(document).ready(function(){ $(window).bind('scroll', function() { var navHeight = 20; if($(window).scrollTop() >= navHeight && !($('nav').hasClass('navbar-fixed-top'))){ $('nav').addClass('navbar-fixed-top'); $('nav').after('<br/>'); } if($(window).scrollTop() < navHeight && $('nav').hasClass('navbar-fixed-top')){ $('nav').removeClass('navbar-fixed-top'); $('nav').nextUntil(':not(br)').remove(); } }); }); </script>

</head>
<body style="background-image: url('/imagens/sistema/background.jpg'); min-width: 360px;">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">{{ trans('layout.sistema.app.navbar.toggle') }}</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{route('sistema::index')}}">
                        <span><img alt="Brand" style="height: 26px;" src="/imagens/logo.png"> {{ trans('layout.sistema.app.navbar.brand') }}</span>
                    </a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('layout.sistema.app.navbar.fan') }} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('sistema::amvs::index')}}">{{ trans('layout.sistema.app.navbar.amv') }}</a></li>
                                <li><a href="{{route('sistema::canaisvlog::index')}}">{{ trans('layout.sistema.app.navbar.vlog') }}</a></li>
                                <li><a href="{{route('sistema::fanfics::index')}}">{{ trans('layout.sistema.app.navbar.fanfic') }}</a></li>
                                <li><a href="{{route('sistema::teorias::index')}}">{{ trans('layout.sistema.app.navbar.theories') }}</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('sistema::biografias')}}">{{ trans('layout.sistema.app.navbar.bio') }}</a></li>
                        <li><a href="{{route('sistema::informacoes::index')}}">{{ trans('layout.sistema.app.navbar.info') }}</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('layout.sistema.app.navbar.multimedia') }} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('sistema::midia::sagas::index')}}">{{ trans('layout.sistema.app.navbar.episodes') }}</a></li>
                                <li><a href="{{route('sistema::midia::especiais::index')}}">{{ trans('layout.sistema.app.navbar.specials') }}</a></li>
                                <li><a href="{{route('sistema::midia::filmes::index')}}">{{ trans('layout.sistema.app.navbar.movies') }}</a></li>
                                <li><a href="{{route('sistema::midia::formatos::index')}}">{{ trans('layout.sistema.app.navbar.formats') }}</a></li>
                                <li><a href="{{route('sistema::midia::hentais::index')}}">{{ trans('layout.sistema.app.navbar.hentais') }}</a></li>
                                <li><a href="{{route('sistema::midia::plataformas::index')}}">{{ trans('layout.sistema.app.navbar.games') }}</a></li>
                                <li><a href="{{route('sistema::letras::index')}}">{{ trans('layout.sistema.app.navbar.lyrics') }}</a></li>
                                <li><a href="{{route('sistema::midia::volumes::index')}}">{{ trans('layout.sistema.app.navbar.manga') }}</a></li>
                                <li><a href="{{route('sistema::midia::ovas::index')}}">{{ trans('layout.sistema.app.navbar.ova') }}</a></li>
                                <li><a href="{{route('sistema::midia::osts::index')}}">{{ trans('layout.sistema.app.navbar.ost') }}</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('sistema::noticias::index')}}">{{ trans('layout.sistema.app.navbar.news') }}</a></li>
                        <li><a href="{{route('sistema::parceiros::index')}}">{{ trans('layout.sistema.app.navbar.partners') }}</a></li>
                        <li><a href="{{route('sistema::slider::index')}}">{{ trans('layout.sistema.app.navbar.slider') }}</a></li>
                        <li><a href="{{route('sistema::tutoriais::index')}}">{{ trans('layout.sistema.app.navbar.tutorials') }}</a></li>
                        <li><a href="{{route('sistema::usuarios::index')}}">{{ trans('layout.sistema.app.navbar.users') }}</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{route('sistema::logout')}}">{{ trans('layout.sistema.app.navbar.logout') }}</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        @yield('content')

        <br />
    </div><!-- /.container -->

    <br />
    <script> $(document).ready(function(){ $('body').append('<button id="toTop" type="button" class="btn btn-info btn-quare btn-sm"><i class="glyphicon glyphicon-arrow-up"></i></button>'); $(window).scroll(function () { if ($(this).scrollTop() != 0) { $('#toTop').fadeIn(); } else { $('#toTop').fadeOut(); } }); $('#toTop').click(function(){ $("html, body").animate({ scrollTop: 0 }, 600); return false; }); }); </script>

    {!! Html::script( elixir('js/footer.js') ) !!}
    {!! Html::script( elixir('js/sistema/footer.js') ) !!}
    @include('flash')
    @yield('footer')
</body>
</html>