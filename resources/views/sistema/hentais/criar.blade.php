@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Hentai
        </h1>
    </div>

    {!! Form::model( $hentai = new \App\Models\Hentai, [ 'method' => 'POST', 'route' => 'sistema::midia::hentais::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.hentais.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::hentais::index'])

    {!! Form::close() !!}

@stop