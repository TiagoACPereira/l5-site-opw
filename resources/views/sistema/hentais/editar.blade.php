@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Hentai
        </h1>
    </div>

    {!! Form::model( $hentai, ['method' => 'PATCH', 'route' => ['sistema::midia::hentais::actualizar', $hentai->id ], 'files' => 'true'  ]) !!}

    <div class="col-md-12">
        <label class="label label-default" for="oldImage">Hentai Antigo</label>
        <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
             src="{{ $hentai->getImagem()->getLink() }}" id="oldImage">
    </div>

    @include('sistema.hentais.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::midia::hentais::index',
        'deleteNameRoute' => 'sistema::midia::hentais::apagar',
        'slug' => $hentai->slug
    ])

    {!! Form::close() !!}

@stop
