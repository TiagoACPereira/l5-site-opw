@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Hentais Cadastrados
            <span class="label label-{{ ($numero_hentais == 0)? 'danger' : 'primary' }}">{{ $numero_hentais }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::hentais::criar') }}">Adicionar Novo Hentai</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($hentais as $hentai)
            <li class="list-group-item clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <img src="{{ $hentai->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Titulo:</strong> {{ $hentai->titulo }} |
                    <strong>Cadastrado: </strong> {{ $hentai->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::hentais::editar', 'deleteNameRoute' => 'sistema::midia::hentais::apagar', 'slug' => $hentai->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhum hentai!</strong>
            </li>
        @endforelse
    </ul>
@stop