@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Teoria
        </h1>
    </div>

    {!! Form::model( $teoria = new \App\Models\Teoria, ['method' => 'POST', 'route' => 'sistema::teorias::guardar']) !!}

    @include('sistema.teorias.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::teorias::index'])

    {!! Form::close() !!}

@stop
