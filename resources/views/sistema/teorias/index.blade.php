@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Teorias Cadastradas
            <span class="label label-{{ ($numero_teorias == 0)? 'danger' : 'primary' }}">{{ $numero_teorias }}</span>
            <br/>
            <small><a href="{{ route('sistema::teorias::criar') }}">Adicionar Nova Teoria</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($teorias as $teoria)
            <li class="list-group-item {{ ($teoria->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>Seção:</strong> {{ $teoria->titulo }} |
                    <strong>Cadastrado: </strong> {{ $teoria->created_at->diffForHumans() }} |
                    <strong><small>{{ (!$teoria->isPublished())? ('A publicar '. $teoria->publish_at->diffForHumans()) : ('Publicado '. $teoria->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::teorias::editar', 'deleteNameRoute' => 'sistema::teorias::apagar', 'slug' => $teoria->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhuma Teoria!</strong>
            </li>
        @endforelse
    </ul>

@stop