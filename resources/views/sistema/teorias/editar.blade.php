@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Teoria
        </h1>
    </div>

    {!! Form::model( $teoria, [ 'method' => 'PATCH', 'route' => ['sistema::teorias::actualizar', $teoria->slug ]]) !!}

    @include('sistema.teorias.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::teorias::index',
        'deleteNameRoute' => 'sistema::teorias::apagar',
        'slug' => $teoria->slug
    ])

    {!! Form::close() !!}
@stop