@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.amvs.index.header') }}
            <span class="label label-{{ ($numero_amvs == 0)? 'danger' : 'primary' }}">{{ $numero_amvs }}</span>
            <br/>
        </h1>
    </div>

    {!! Form::model( $amv = new \App\Models\Amv, ['method' => 'POST', 'route' => 'sistema::amvs::guardar']) !!}
    {!! Form::hidden('user_id', Auth::user()['id']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="input-group">
                <input type="text" name="video_id" class="form-control" placeholder="{{ trans('layout.sistema.amvs.index.input-youtube-id') }}" required>
                <span class="input-group-btn"><button class="btn btn-primary" type="submit">{{ trans('layout.sistema.amvs.index.button-submit') }}</button></span>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <br/>

    <ul class="list-group">
        @forelse($amvs as $amv)
            <li class="list-group-item clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>{{ trans('layout.sistema.amvs.index.amv.title') }}</strong> {!! Html::link($amv->link, $amv->titulo) !!} |
                    <strong>{{ trans('layout.sistema.amvs.index.amv.author') }}</strong> {{ $amv->autor }} |
                    <strong>{{ trans('layout.sistema.amvs.index.amv.registered') }}</strong> {{ $amv->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    <?php $random = uniqid(); ?> {!! Form::open( [ 'route' => ['sistema::amvs::apagar', $amv->id ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!} <button class="btn btn-danger btn-sm" onclick="_delete(); return false;"> <span class='glyphicon glyphicon-trash'></span> {{ trans('layout.sistema.amvs.index.amv.button-remove') }} </button> {!! Form::close() !!}
                    <script> function _delete() { swal({ title: "{!! trans('layout.sistema.amvs.index.amv.delete-warning.title') !!}", text: "{!! trans('layout.sistema.amvs.index.amv.delete-warning.message') !!}", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); } </script>
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>{{ trans('layout.sistema.amvs.index.amv.empty') }}</strong>
            </li>
        @endforelse
    </ul>

@stop