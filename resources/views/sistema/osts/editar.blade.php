@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Ost
        </h1>
    </div>

    {!! Form::model( $ost, ['method' => 'PATCH', 'route' => ['sistema::midia::osts::actualizar', $ost->id ], 'files' => 'true'  ]) !!}

    <div class="col-md-12">
        <label class="label label-default" for="oldImage">Ost Antigo</label>
        <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
             src="{{ $ost->getImagem()->getLink() }}" id="oldImage">
    </div>

    @include('sistema.osts.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::midia::osts::index',
        'deleteNameRoute' => 'sistema::midia::osts::apagar',
        'slug' => $ost->slug
    ])

    {!! Form::close() !!}

@stop
