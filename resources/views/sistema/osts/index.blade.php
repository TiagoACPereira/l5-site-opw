@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Osts Cadastrados
            <span class="label label-{{ ($numero_osts == 0)? 'danger' : 'primary' }}">{{ $numero_osts }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::osts::criar') }}">Adicionar Novo Ost</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($osts as $ost)
            <li class="list-group-item clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <img src="{{ $ost->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Titulo:</strong> {{ $ost->titulo }} |
                    <strong>Cadastrado: </strong> {{ $ost->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::osts::editar', 'deleteNameRoute' => 'sistema::midia::osts::apagar', 'slug' => $ost->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhum ost!</strong>
            </li>
        @endforelse
    </ul>
@stop