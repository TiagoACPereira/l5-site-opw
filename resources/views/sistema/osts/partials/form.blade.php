{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-md-4 col-sm-6">
        <label for="titulo" class="label label-default">Titulo</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => 'Titulo' ] ) !!}
        <br/>
    </div>

    <div class="col-md-4 col-sm-6">
        <label for="album" class="label label-default">Álbum</label>
        {!! Form::text('album', null, [ 'class' => 'form-control', 'placeholder' => 'Álbum' ] ) !!}
        <br/>
    </div>

    <div class="col-md-4 col-sm-6">
        <label for="uploaders[]" class="label label-default">Uploaders</label>
        <select class="form-control select2-todo" data-placeholder="Selecione os Uploaders" name="uploaders[]" multiple>
            @forelse($uploadersList as $valeu => $nome)
                @if(old('uploaders') != null)
                    @forelse(old('uploaders') as $uploaderValeu)
                        @if($uploaderValeu == $valeu)
                            <option selected value="{{$valeu}}">{{$nome}}</option>
                        @else
                            <option value="{{$valeu}}">{{$nome}}</option>
                        @endif
                    @empty
                    @endforelse
                @elseif($ost != null)
                    @if( $ost->isUploader($valeu))
                        <option selected value="{{$valeu}}">{{$nome}}</option>
                    @else
                        <option value="{{$valeu}}">{{$nome}}</option>
                    @endif
                @else
                    <option value="{{$valeu}}">{{$nome}}</option>
                @endif
            @empty
            @endforelse
        </select>
        <br/>
    </div>

    <div class="col-md-4 col-sm-6">
        <label for="formato" class="label label-default">Formato</label>
        <select class="form-control select2-todo-tags" data-placeholder="Selecione ou adicione Formato" name="formato">
            @if(old('formato') != null)
                <?php $tempFormato = old('formato'); ?>
            @elseif($ost->formato != null)
                <?php $tempFormato = $ost->formato->id; ?>
            @else
                <?php $tempFormato = null ?>
            @endif

            @forelse($formatosList as $valeu => $nome)
                @if($tempFormato == $valeu)
                    <option selected value="{{$valeu}}">{{$nome}}</option>
                @else
                    <option value="{{$valeu}}">{{$nome}}</option>
                @endif
            @empty
            @endforelse

            @if (substr(old('formato'), 0, 5) == 'nova:')
                <option selected value="{{old('formato')}}">{{substr(old('formato'), 5)}} (nova)</option>
            @endif
        </select>
        <br/>
    </div>

    <div class="col-md-4 col-sm-6">
        <label for="tamanho" class="label label-default">Tamanho</label>
        <select class="form-control select2-todo-tags" data-placeholder="Selecione ou adicione Tamanho" name="tamanho">
            @if(old('tamanho') != null)
                <?php $tempTamanho = old('tamanho'); ?>
            @elseif($ost->tamanho != null)
                <?php $tempTamanho = $ost->tamanho->id; ?>
            @else
                <?php $tempTamanho = null ?>
            @endif

            @forelse($tamanhosList as $valeu => $nome)
                @if($tempTamanho == $valeu)
                    <option selected value="{{$valeu}}">{{$nome}}</option>
                @else
                    <option value="{{$valeu}}">{{$nome}}</option>
                @endif
            @empty
            @endforelse

            @if (substr(old('tamanho'), 0, 5) == 'nova:')
                <option selected value="{{old('tamanho')}}">{{substr(old('tamanho'), 5)}} (nova)</option>
            @endif
        </select>
        <br/>
    </div>

    <div class="col-md-4 col-sm-6">
        <label for="idioma" class="label label-default">Idioma</label>
        <select class="form-control select2-todo-tags" data-placeholder="Selecione ou adicione Idioma" name="idioma">
            @if(old('idioma') != null)
                <?php $tempIdioma = old('idioma'); ?>
            @elseif($ost->idioma != null)
                <?php $tempIdioma = $ost->idioma->id; ?>
            @else
                <?php $tempIdioma = null ?>
            @endif

            @forelse($idiomasList as $valeu => $nome)
                @if($tempIdioma == $valeu)
                    <option selected value="{{$valeu}}">{{$nome}}</option>
                @else
                    <option value="{{$valeu}}">{{$nome}}</option>
                @endif
            @empty
            @endforelse

            @if (substr(old('idioma'), 0, 5) == 'nova:')
                <option selected value="{{old('idioma')}}">{{substr(old('idioma'), 5)}} (nova)</option>
            @endif
        </select>
        <br/>
    </div>

    @include('sistema.partials.imagem-form', ['label' => 'Imagem de Ost', 'model' => $ost, 'classInputImagem' => 'col-md-4 col-sm-6', 'classInputHost' => 'col-md-4 col-sm-6'])

    @include('sistema.partials.recaptcha', ['recaptchaClass' => 'col-sm-6 col-md-4 clearfix'])

    <div class="col-xs-12 container-fluid">
        <div class="panel panel-group">
            <div class="panel panel-info" id="mirrors-panel">
                <div class="panel-heading"
                     onclick="$(this).closest('div #mirrors-panel').children('div .panel-body').toggle('slow'); $(this).children('strong').children('.glyphicon').toggle(); $(this).blur(); return false;">
                    <strong style="display: inline-block; padding: 6px 12px;">
                        <i class="glyphicon glyphicon-eye-close" style="display: none;"></i>
                        <i class="glyphicon glyphicon-eye-open"></i>
                        Mirror's
                    </strong>
                </div>
                <div class="panel-body" style="display: none;" id="links">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-xs-12">
                            <button onclick="_loadViewAppend($(this).closest('#links').children('div #links-append'), '{{route('sistema::forms::mirror::criar',['inputname' => 'mirrors[]', 'placeholder' => 'Mirror' ] )}}'); $(this).blur(); return false;"
                                    type="button" class="btn btn-info btn-block">Adiciona Link <span
                                        class='glyphicon glyphicon-tasks'></span>
                            </button>
                        </div>
                    </div>
                    <div id="links-append" class="row">
                        @if(old('mirrors') != null)
                            @forelse(old('mirrors') as $key => $val)
                                @include('sistema.partials.links-input-form', ['valeuInput' => $val, 'inputname' => 'mirrors[]', 'placeholder' => 'Mirror'])
                            @empty
                            @endforelse
                        @elseif($ost != null)
                            @forelse( $ost->mirrors as $key => $val)
                                @include('sistema.partials.links-input-form', ['valeuInput' => $val->url, 'inputname' => 'mirrors[]', 'placeholder' => 'Mirror'])
                            @empty
                            @endforelse
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('sistema.partials.script_loadView')

    <script type="text/javascript">

        $(".select2-todo").select2({
            theme: 'bootstrap',
            tags: false,
            width: null,
            allowClear: true
        });

        $(".select2-todo-tags").select2({
            theme: 'bootstrap',
            tags: true,
            width: null,
            createTag: function(newTag) {
                return {
                    id: 'nova:' + newTag.term,
                    text: newTag.term + ' (nova)'
                };
            }
        });
    </script>

</div>

@include('sistema.partials.script_loadView')