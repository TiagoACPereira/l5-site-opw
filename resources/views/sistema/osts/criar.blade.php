@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Ost
        </h1>
    </div>

    {!! Form::model( $ost = new \App\Models\Ost, [ 'method' => 'POST', 'route' => 'sistema::midia::osts::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.osts.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::osts::index'])

    {!! Form::close() !!}

@stop