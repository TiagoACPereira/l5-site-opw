@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Informação
        </h1>
    </div>

    {!! Form::model( $info, [ 'method' => 'PATCH', 'route' => ['sistema::informacoes::actualizar', $info->slug ]]) !!}

    @include('sistema.informacoes.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::informacoes::index',
        'deleteNameRoute' => 'sistema::informacoes::apagar',
        'slug' => $info->slug
    ])

    {!! Form::close() !!}
@stop