@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Informação
        </h1>
    </div>

    {!! Form::model( $info = new \App\Models\Info, ['method' => 'POST', 'route' => 'sistema::informacoes::guardar']) !!}

    @include('sistema.informacoes.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::informacoes::index'])

    {!! Form::close() !!}

@stop
