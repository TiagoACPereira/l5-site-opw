@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Informações Cadastrados
            <span class="label label-{{ ($numero_infos == 0)? 'danger' : 'primary' }}">{{ $numero_infos }}</span>
            <br/>
            <small><a href="{{ route('sistema::informacoes::criar') }}">Adicionar Nova Informação</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($infos as $info)
            <li class="list-group-item {{ ($info->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>Seção:</strong> {{ $info->titulo }} |
                    <strong>Cadastrado: </strong> {{ $info->created_at->diffForHumans() }} |
                    <strong><small>{{ (!$info->isPublished())? ('A publicar '. $info->publish_at->diffForHumans()) : ('Publicado '. $info->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::informacoes::editar', 'deleteNameRoute' => 'sistema::informacoes::apagar', 'slug' => $info->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhuma Informação!</strong>
            </li>
        @endforelse
    </ul>

@stop