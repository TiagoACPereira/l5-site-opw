@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Sagas Cadastrados
            <span class="label label-{{ ($numero_sagas == 0)? 'danger' : 'primary' }}">{{ $numero_sagas }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::sagas::criar') }}">Adicionar Nova Saga</a></small>
        </h1>
    </div>

    <ul class="list-group">
        @forelse($sagas as $saga)
            <li class="list-group-item {{ ($saga->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block;">
                    <img src="{{ $saga->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Saga:</strong> {{ $saga->titulo }} |
                    <strong>Cadastrado: </strong> {{ $saga->created_at->diffForHumans() }}
                    <strong><small>{{ (!$saga->isPublished())? ('A publicar '. $saga->publish_at->diffForHumans()) : ('Publicado '. $saga->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    <button onclick="_loadView('li_saga_{{$saga->slug}}', '{{route('sistema::midia::sagas::mostrar', $saga->slug)}}'); return false;" type="button"
                            class="btn btn-default btn-sm dropdown-toggle">
                        <span class="label label-{{ ($saga->numeroEpisodios() == 0)? 'danger' : 'primary' }}">{{ $saga->numeroEpisodios() }}</span>
                        EPISÓDIOS
                    </button>
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::sagas::editar', 'deleteNameRoute' => 'sistema::midia::sagas::apagar', 'slug' => $saga->slug ])
                </span>
            </li>

            <div id="li_saga_{{$saga->slug}}" style="display: none;">

            </div>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhuma Saga!</strong>
            </li>
        @endforelse
    </ul>

    @include('sistema.partials.script_loadView')
@stop