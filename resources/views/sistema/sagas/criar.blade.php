@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Saga
        </h1>
    </div>

    {!! Form::model( $saga = new \App\Models\Saga, [ 'method' => 'POST', 'route' => 'sistema::midia::sagas::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.sagas.partials.form')


    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::sagas::index'])

    {!! Form::close() !!}

@stop