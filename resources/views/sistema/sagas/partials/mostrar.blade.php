@if($saga->numeroEpisodios() != 0)
    <?php $msgTemp = $saga->numeroEpisodios() . " episódios nesta saga!!"; ?>
@else
    <?php $msgTemp = "Não foi registado nenhum episódio nesta saga!"; ?>
@endif

<li  class="list-group-item clearfix" style=" background-color: #f5f5f5">
    <span style="display: inline-block; padding: 6px 12px;">
        <strong>{{ $msgTemp }}</strong>
    </span>
    <span class="pull-right">
        <a class="btn btn-default btn-sm" role="button"
           href="{{ route('sistema::midia::sagas::episodios::criar', $saga->slug) }}">
            <span class="glyphicon glyphicon-edit"></span>
            Inserir Episódio
        </a>
    </span>
</li>

@foreach($saga->episodios as $episodio)
    <li class="list-group-item {{ ($episodio->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix" style="background-color: #f5f5f5">
        <span style="display: inline-block; padding: 6px 12px;">
            <strong>Episodio:</strong> {{ $episodio->str_numero }} |
            <strong>Título:</strong> {{ $episodio->titulo }} |
            <strong>Cadastrado: </strong> {{ $episodio->created_at->diffForHumans() }} |
            <strong><small>{{ (!$episodio->isPublished())? ('A publicar '. $episodio->publish_at->diffForHumans()) : ('Publicado '. $episodio->publish_at->diffForHumans()) }}</small></strong>
        </span>
        <span class="pull-right">
            <?php $random = uniqid(); ?>
            <strong>
                <a class="btn btn-default btn-sm" role="button"
                   href="{{ route('sistema::midia::sagas::episodios::editar', [$saga->slug, $episodio->slug]) }}">
                    <span class="glyphicon glyphicon-edit"></span>
                    ALTERAR
                </a>
            </strong>
            {!! Form::open( [ 'route' => ['sistema::midia::sagas::episodios::apagar', $episodio->slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
            <button class="btn btn-danger btn-sm" onclick="_delete(); return false;">
                <span class='glyphicon glyphicon-trash'></span> REMOVER
            </button>
            {!! Form::close() !!}

            <script> function _delete() { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); } </script>
        </span>
    </li>
@endforeach
