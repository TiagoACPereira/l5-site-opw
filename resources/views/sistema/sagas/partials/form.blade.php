{!! Form::hidden('id', $saga->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">Título</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => 'Titulo', 'required' => '' ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="numero" class="label label-default">Numero</label>
        {!! Form::number('numero', null, [ 'class' => 'form-control', 'placeholder' => '0' ]) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.imagem-form', ['label' => 'Hospedagem de Imagem de Saga', 'model' => $saga])

    @include('sistema.partials.recaptcha')

</div><!-- /.row -->