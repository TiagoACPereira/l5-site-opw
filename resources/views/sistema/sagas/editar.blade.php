@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Saga
        </h1>
    </div>

    {!! Form::model( $saga, ['method' => 'PATCH', 'route' => ['sistema::midia::sagas::actualizar', $saga->id ], 'files' => 'true'  ]) !!}
    <div class="row">
        <div class="col-md-12">
            <label class="label label-default" for="oldImage">Imagem Saga Antiga</label>
            <img style="padding-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                 src="{{ $saga->getImagem()->getLink() }}" id="oldImage">
        </div>
    </div><!-- /.row -->
    @include('sistema.sagas.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::midia::sagas::index',
        'deleteNameRoute' => 'sistema::midia::sagas::apagar',
        'slug' => $saga->slug
    ])

    {!! Form::close() !!}

@stop
