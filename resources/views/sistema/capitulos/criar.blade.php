@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.capitulos.criar.header') }} {{$volume->numero}}
            <br/>
            <small>{{ trans('layout.sistema.capitulos.criar.sub-header') }}</small>
        </h1>
    </div>

    {!! Form::model( $capitulo = new \App\Models\Capitulo, ['method' => 'POST', 'route' => ['sistema::midia::volumes::capitulos::guardar', $volume->slug], 'files' => 'true'  ]) !!}

    @include('sistema.capitulos.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::volumes::index'])

    {!! Form::close() !!}

@stop
