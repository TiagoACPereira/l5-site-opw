@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.capitulos.editar.header') }} {{$volume->numero}}
            <br/>
            <small>{{ trans('layout.sistema.capitulos.editar.sub-header') }} {{$capitulo->numero}}</small>
        </h1>
    </div>

    {!! Form::model( $capitulo, [ 'method' => 'PATCH', 'route' => ['sistema::midia::volumes::capitulos::actualizar', $capitulo->slug ], 'files' => 'true'  ]) !!}

        <div class="row">
            <div class="col-md-12">
                <label class="label label-default" for="oldImage">{{ trans('layout.sistema.capitulos.editar.label-image') }}</label>
                <br/>
                <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                     src="{{ $capitulo->getImagem()->getLink() }}" id="oldImage">
            </div>
        </div>

        @include('sistema.capitulos.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::midia::volumes::index',
            'deleteNameRoute' => 'sistema::midia::volumes::capitulos::apagar',
            'slug' => $capitulo->slug
        ])

    {!! Form::close() !!}

@stop
