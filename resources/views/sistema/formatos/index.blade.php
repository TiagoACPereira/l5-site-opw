@extends('sistema.app')

@section('content')
    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.formatos.index.header') }}
            <span class="label label-{{ ($numero_formatos == 0)? 'danger' : 'primary' }}">{{ $numero_formatos }}</span>
            <br/>
            <small><a href="" onclick="_loadView('formato_criar', '{{route('sistema::midia::formatos::criar')}}'); return false;">{{ trans('layout.sistema.formatos.index.sub-header') }}</a></small>
        </h1>
    </div>
    <div class="well" id="formato_criar" style="display: none;">
    </div>
    <br/>

    <ul class="list-group">
        @forelse($formatos as $formato)
            <li class="list-group-item clearfix">
                <span style="display: inline-block;">
                    <img src="{{ $formato->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>{{ trans('layout.sistema.formatos.index.label-title') }}</strong> {{ $formato->descricao }} |
                    <strong>{{ trans('layout.sistema.formatos.index.label-registered') }}</strong> {{ $formato->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    <?php $random = uniqid(); ?>
                        <strong>
                            <a class="btn btn-default btn-sm" onclick="_loadView('formato{{$formato->id}}', '{{route('sistema::midia::formatos::editar', ['id' => $formato->slug])}}'); return false;" role="button" href="#">
                                <span class="glyphicon glyphicon-edit"></span>
                                {{ trans('layout.sistema.formatos.index.button-edit') }}
                            </a>
                        </strong>
                    {!! Form::open( [ 'route' => ['sistema::midia::formatos::apagar', $formato->slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="_delete_{{$random}}(); return false;">
                            <span class='glyphicon glyphicon-trash'></span> {{ trans('layout.sistema.formatos.index.button-delete') }}
                        </button>
                    {!! Form::close() !!}
                    <script>
                        function _delete_{{$random}}() { swal({ title: "{!! trans('layout.sistema.formatos.index.chapter.delete-warning-title') !!}", text: "{!! trans('layout.sistema.formatos.index.chapter.delete-warning-message') !!}", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); }
                    </script>
                </span>
            </li>
            <li id="formato{{$formato->id}}" class="list-group-item  clearfix" style="display: none; background-color: #f5f5f5">
            </li>
        @empty
            <li class="list-group-item">
                <strong>{{ trans('layout.sistema.formatos.index.empty') }}</strong>
            </li>
        @endforelse
    </ul>

    @include('sistema.partials.script_loadView')
    <script>
        function _show($elementID) {
            $("#" + $elementID).toggle('slow');
        }
    </script>

@stop
