<span class="pull-right"><button type="button"  onclick="$(this).closest('li').hide('slow', function() {$(this).empty();}); return false;" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></span>
{!! Form::model( $formato, ['method' => 'PATCH', 'route' => ['sistema::midia::formatos::actualizar', $formato->slug ], 'files' => 'true'  ]) !!}

<div class="row">
    <div class="col-md-12">
        <label class="label label-default" for="oldImage">{{ trans('layout.sistema.formatos.partials.editar.label-image') }}</label>
        <img style="padding-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
             src="{{ $formato->getImagem()->getLink() }}" id="oldImage">
    </div>
</div>

@include('sistema.formatos.partials.form')

<div class="row">
    <div class="col-md-12" style="padding-bottom: 15px;">
        <button type="submit" class="btn btn-primary btn-block">
            {{ trans('layout.sistema.formatos.partials.editar.label-save') }} <span class='glyphicon glyphicon-ok'></span></button>
    </div>
    {!! Form::close() !!}
</div><!-- /.row -->

{!! Form::close() !!}
