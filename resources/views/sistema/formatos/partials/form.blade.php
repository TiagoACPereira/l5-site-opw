{!! Form::hidden('id', $formato->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-12">
        <label for="descricao" class="label label-default">{{ trans('layout.sistema.formatos.partials.form.input-name') }}</label>
        {!! Form::text('descricao', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.formatos.partials.form.input-name'), 'required' => ''] ) !!}
        <br/>
    </div>

    @include('sistema.partials.imagem-form', ['label' => trans('layout.generic.label-image-host'), 'model' => $formato])

    @include('sistema.partials.recaptcha')

</div><!-- /.row -->