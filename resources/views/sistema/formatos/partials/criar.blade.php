<span class="pull-right"><button type="button"  onclick="$(this).closest('div').hide('slow', function() {$(this).empty();}); return false;" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></span>
{!! Form::model( $formato = new \App\Models\Formato, [ 'method' => 'POST', 'route' => 'sistema::midia::formatos::guardar', 'files' => 'true'  ]) !!}

@include('sistema.formatos.partials.form')

<div class="row">
    <div class="col-md-12" style="padding-bottom: 15px;">
        <button type="submit" id="save" class="btn btn-primary btn-block">{{ trans('layout.sistema.formatos.partials.criar.label-create') }} <span class='glyphicon glyphicon-ok'></span></button>
    </div>
</div>

{!! Form::close() !!}
