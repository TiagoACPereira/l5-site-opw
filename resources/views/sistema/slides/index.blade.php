@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Slides Cadastrados
            <span class="label label-{{ ($numero_slides == 0)? 'danger' : 'primary' }}">{{ $numero_slides }}</span>
            <br/>
            <small><a href="{{ route('sistema::slider::criar') }}">Adicionar Novo Slide</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($slides as $slide)
            <li class="list-group-item {{ ($slide->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block;">
                    <img src="{{ $slide->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Slide:</strong> {{ $slide->informacao }} |
                    <strong>Cadastrado: </strong> {{ $slide->created_at->diffForHumans() }}
                    <strong><small>{{ (!$slide->isPublished())? ('A publicar '. $slide->publish_at->diffForHumans()) : ('Publicado '. $slide->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::slider::editar', 'deleteNameRoute' => 'sistema::slider::apagar', 'slug' => $slide->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhum slide!</strong>
            </li>
        @endforelse
    </ul>
@stop