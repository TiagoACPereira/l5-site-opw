@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Slide
        </h1>
    </div>

    {!! Form::model( $slide, ['method' => 'PATCH', 'route' => ['sistema::slider::actualizar', $slide->id ], 'files' => 'true'  ]) !!}

    <div class="col-md-12">
        <label class="label label-default" for="oldImage">Slide Antigo</label>
        <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
             src="{{ $slide->getImagem()->getLink() }}" id="oldImage">
    </div>

    @include('sistema.slides.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::slider::index',
        'deleteNameRoute' => 'sistema::slider::apagar',
        'slug' => $slide->slug
    ])

    {!! Form::close() !!}

@stop
