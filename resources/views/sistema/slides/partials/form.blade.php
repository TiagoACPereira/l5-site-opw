{!! Form::hidden('id', $slide->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6">
        <label for="informacao" class="label label-default">Nome</label>
        {!! Form::text('informacao', null, [ 'class' => 'form-control', 'placeholder' => 'Nome', 'required' => '' ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="url" class="label label-default">Url</label>
        {!! Form::url('url', null, [ 'class' => 'form-control', 'placeholder' => 'URL' , 'required' => ''] ) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.imagem-form', ['label' => 'Hospedagem Slider', 'model' => $slide])

    @include('sistema.partials.recaptcha')

</div>