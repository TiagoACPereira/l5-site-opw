@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Slide
        </h1>
    </div>

    {!! Form::model( $slide = new \App\Models\Slide, [ 'method' => 'POST', 'route' => 'sistema::slider::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.slides.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::slider::index'])

    {!! Form::close() !!}

@stop