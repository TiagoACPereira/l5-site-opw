@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Plataforma: {{ $plataforma->nome }}
            <br/>
            <small>Novo {{ $plataforma->tipo }}</small>
        </h1>
    </div>

    {!! Form::model( $jogo = new \App\Models\Jogo, ['method' => 'POST', 'route' => ['sistema::midia::plataformas::jogos::guardar', $plataforma->slug], 'files' => 'true'  ]) !!}

    @include('sistema.jogos.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::plataformas::index'])

    {!! Form::close() !!}

@stop
