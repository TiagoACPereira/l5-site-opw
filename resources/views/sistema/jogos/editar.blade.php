@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Plataforma: {{ $plataforma->nome }}
            <br/>
            <small>Editar {{ $plataforma->tipo }}: {{$jogo->titulo}}</small>
        </h1>
    </div>

    {!! Form::model( $jogo, [ 'method' => 'PATCH', 'route' => ['sistema::midia::plataformas::jogos::actualizar', $jogo->slug ], 'files' => 'true'  ]) !!}

    <div class="row">
        <div class="col-md-12">
            <label class="label label-default" for="oldImage">Imagem Antiga</label>
            <br/>
            <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                 src="{{ $jogo->getImagem()->getLink() }}" id="oldImage">
        </div>
    </div>

        @include('sistema.jogos.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::midia::plataformas::index',
            'deleteNameRoute' => 'sistema::midia::plataformas::jogos::apagar',
            'slug' => $jogo->slug
        ])

    {!! Form::close() !!}

@stop
