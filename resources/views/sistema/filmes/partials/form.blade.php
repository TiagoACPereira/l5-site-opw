<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">{{ trans('layout.sistema.filmes.partials.form.input-title') }}</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.filmes.partials.form.input-title') ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="numero" class="label label-default">{{ trans('layout.sistema.filmes.partials.form.input-number') }}</label>
        {!! Form::number('numero', null, [ 'class' => 'form-control', 'placeholder' => '0' ] ) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.imagem-form', ['label' => trans('layout.generic.label-image-host'), 'model' => $filme])

    @include('sistema.partials.recaptcha')
</div>

@include('sistema.formatosmidia.partials.container', ['model' => $filme])

@include('sistema.partials.script_loadView')