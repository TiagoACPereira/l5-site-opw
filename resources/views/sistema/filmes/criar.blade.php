@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.filmes.criar.header') }}
            <br/>
            <small>{{ trans('layout.sistema.filmes.criar.sub-header') }}</small>
        </h1>
    </div>

    {!! Form::model( $filme = new \App\Models\Filme, ['method' => 'POST', 'route' => 'sistema::midia::filmes::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.filmes.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::filmes::index'])

    {!! Form::close() !!}

@stop
