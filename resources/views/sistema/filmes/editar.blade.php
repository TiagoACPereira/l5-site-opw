@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.filmes.editar.header') }}
            <br/>
            <small>{{ trans('layout.sistema.filmes.editar.sub-header') }} {{$filme->titulo}}</small>
        </h1>
    </div>

    {!! Form::model( $filme, [ 'method' => 'PATCH', 'route' => ['sistema::midia::filmes::actualizar', $filme->slug ], 'files' => 'true'  ]) !!}

    <div class="row">
        <div class="col-md-12">
            <label class="label label-default" for="oldImage">{{ trans('layout.sistema.filmes.editar.label-image') }}</label>
            <br/>
            <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                 src="{{ $filme->getImagem()->getLink() }}" id="oldImage">
        </div>
    </div>

        @include('sistema.filmes.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::midia::filmes::index',
            'deleteNameRoute' => 'sistema::midia::filmes::apagar',
            'slug' => $filme->slug
        ])

    {!! Form::close() !!}

@stop
