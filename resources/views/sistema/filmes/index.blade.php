@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>{{ trans('layout.sistema.filmes.index.header') }}
            <span class="label label-{{ ($numero_filmes == 0)? 'danger' : 'primary' }}">{{ $numero_filmes }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::filmes::criar') }}">{{ trans('layout.sistema.filmes.index.sub-header') }}</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($filmes as $filme)
            <li class="list-group-item {{ ($filme->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <img src="{{ $filme->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>{{ trans('layout.sistema.filmes.index.label-title') }}</strong> {{ $filme->titulo }} |
                    <strong>{{ trans('layout.sistema.filmes.index.label-registered') }} </strong> {{ $filme->created_at->diffForHumans() }}
                    <strong><small>{{ (!$filme->isPublished())? (trans('layout.generic.label-publishing') .' '. $filme->publish_at->diffForHumans()) : (trans('layout.generic.label-published') .' '. $filme->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::filmes::editar', 'deleteNameRoute' => 'sistema::midia::filmes::apagar', 'slug' => $filme->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>{{ trans('layout.sistema.filmes.index.empty') }}</strong>
            </li>
        @endforelse
    </ul>
@stop