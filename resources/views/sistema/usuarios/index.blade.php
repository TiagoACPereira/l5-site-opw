@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Usuarios Cadastrados
            <span class="label label-{{ ($numero_usuarios == 0)? 'danger' : 'primary' }}">{{ $numero_usuarios }}</span>
            <br/>
            <small><a href="{{ route('sistema::usuarios::criar') }}">Adicionar Novo Usuario</a></small>
        </h1>
    </div>
    <ul class="list-group">
    @forelse($usuarios as $usuario)
        <li class="list-group-item clearfix">
            <span style="display: inline-block; padding: 6px 12px;">
                <strong>Nome:</strong> {{ $usuario->nome }} |
                <strong>Cadastrado: </strong> {{ $usuario->created_at->diffForHumans() }}
            </span>
            <span class="pull-right">
                @if($usuario->active)
                    @include('sistema.partials.alterar_desactivar_delete_buttons', [ 'alterarNameRoute' => 'sistema::usuarios::editar', 'desactivarNameRoute' => 'sistema::usuarios::desactivar' ,'deleteNameRoute' => 'sistema::usuarios::apagar', 'slug' => $usuario->id ])
                @else
                    @include('sistema.partials.alterar_activar_delete_buttons', [ 'alterarNameRoute' => 'sistema::usuarios::editar', 'activarNameRoute' => 'sistema::usuarios::activar' ,'deleteNameRoute' => 'sistema::usuarios::apagar', 'slug' => $usuario->id ])
                @endif
            </span>
        </li>
    @empty
        <li class="list-group-item">
            <strong>Não foi registado nenhum utilizador!</strong>
        </li>
    @endforelse
    </ul>

@stop