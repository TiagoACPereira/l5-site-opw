@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Usuário
        </h1>
    </div>

    {!! Form::model( $usuario, [ 'method' => 'PUT', 'route' => ['sistema::usuarios::actualizar', $usuario->id ], 'files' => 'true'  ]) !!}
    {!! Form::hidden('id', $usuario->id) !!}

    <div class="row">
        <div class="col-md-6">
            <label for="nome" class="label label-default">Nome</label>
            {!! Form::text('nome', null, [ 'class' => 'form-control', 'placeholder' => 'Nome' ] ) !!}
            <br/>
        </div>

        <div class="col-md-6">
            <label for="email" class="label label-default">Email</label>
            {!! Form::email('email', null, [ 'class' => 'form-control', 'placeholder' => 'Email' ] ) !!}
            <br/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label for="login" class="label label-default">Login</label>
            {!! Form::text('login', null, [ 'class' => 'form-control', 'placeholder' => 'Login' ] ) !!}
            <br/>
        </div>

        <div class="col-md-6">
            <label for="password" class="label label-default">Nova Password</label>
            <input type="password" name="password" class="form-control" placeholder="Nova Password">
            <br/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label for="password_confirmation" class="label label-default">Confirmar Nova Password</label>
            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar Nova Password">
            <br/>
        </div>

        <div class="col-md-6">
            <label for="nick" class="label label-default">Nick</label>
            {!! Form::text('nick', null, [ 'class' => 'form-control', 'placeholder' => 'Nick' ] ) !!}
            <br/>
        </div>
    </div>


    @if($link_imagem != null)
        <div class="row">
            <div class="col-md-12">
                <label class="label label-default" for="oldImage">Imagem Antiga</label>
                <img style="padding: 5px; max-width: 200px; margin-bottom:15px;"  class="img-responsive center-block img-thumbnail"
                     src="{{ $link_imagem }}" id="oldImage">
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    @endif


    <div class="row">
        <div class="col-sm-6">
            <label class="label label-default" for="avatar">Novo Avatar</label>
            <input type="file" name="avatar">
            <br/>
        </div>

        <div class="col-sm-6">
            <label class="label label-default" for="dataNascimento">Data Nascimento</label>
            {!! Form::input('date', 'dataNascimento', null, ['class' => 'form-control']) !!}
            <br/>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <label for="twitter" class="label label-default">Twitter</label>
            {!! Form::text('twitter', null, [ 'class' => 'form-control', 'placeholder' => 'Twitter' ] ) !!}
            <br/>
        </div>

        <div class="col-sm-6">
            <label for="facebook" class="label label-default">Facebook</label>
            {!! Form::text('facebook', null, [ 'class' => 'form-control', 'placeholder' => 'Facebook' ] ) !!}
            <br/>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <label for="googleplus" class="label label-default">Googleplus</label>
            {!! Form::text('googleplus', null, [ 'class' => 'form-control', 'placeholder' => 'Googleplus' ] ) !!}
            <br/>
        </div>

        <div class="col-sm-6">
            <label for="youtube" class="label label-default">Youtube</label>
            {!! Form::text('youtube', null, [ 'class' => 'form-control', 'placeholder' => 'Youtube' ] ) !!}
            <br/>
        </div>
    </div>


    @if(Auth::user()->isAdmin())
        <div class="row">
            <div class="col-md-12">
                <label class="label label-default">Permissões</label>
                <div class="container" style="margin-top: 10px;">
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    {!! Form::checkbox('admin', 1, $usuario->isAdmin()); !!}
                                </span>
                                <input type="text" class="form-control" value="Admin" style="max-width: 150px"
                                       onclick="toggleCheckbox('admin')" readonly>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    {!! Form::checkbox('revisor', 1, $usuario->isRevisor()); !!}
                                </span>
                                <input type="text" class="form-control" value="Revisor" style="max-width: 150px"
                                       onclick="toggleCheckbox('revisor')" readonly>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    {!! Form::checkbox('redator', 1, $usuario->isRedator()); !!}
                                </span>
                                <input type="text" class="form-control" value="Redator" style="max-width: 150px"
                                       onclick="toggleCheckbox('redator')" readonly>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    {!! Form::checkbox('editorNoticias', 1, $usuario->isEditorNoticias()); !!}
                                </span>
                                <input type="text" class="form-control" value="Editor Notícias" style="max-width: 150px"
                                       onclick="toggleCheckbox('editorNoticias')" readonly>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    {!! Form::checkbox('midiaUploader', 1, $usuario->isMidiaUploader()); !!}
                                </span>
                                <input type="text" class="form-control" value="Mídia Uploader" style="max-width: 150px"
                                       onclick="toggleCheckbox('midiaUploader')" readonly>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    {!! Form::checkbox('fanArea', 1, $usuario->isFanArea()); !!}
                                </span>
                                <input type="text" class="form-control" value="Fan Area" style="max-width: 150px"
                                       onclick="toggleCheckbox('fanArea')" readonly>
                            </div>
                            <br/>
                            <script>
                                function toggleCheckbox(name) { var x = document.getElementsByName(name); var i; for (i = 0; i < x.length; i++) { if (x[i].type == "checkbox") { x[i].checked = !x[i].checked; } } }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('sistema.partials.recaptcha')

    @endif
    <div class="row">
        <div class="col-md-6" style="padding-bottom: 15px;">
            <input type="submit" value="Guardar" class="btn btn-primary btn-block">
        </div>
        <div class="col-md-6">
            <a class="btn btn-danger btn-block" href="{{ route('sistema::usuarios::index') }}" role="button">Cancelar</a>
        </div>
    </div>

    {!! Form::close() !!}
@stop