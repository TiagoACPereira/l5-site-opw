@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.episodios.criar.header') }} {{$saga->titulo}}
            <br/>
            <small>{{ trans('layout.sistema.episodios.criar.sub-header') }}</small>
        </h1>
    </div>

    {!! Form::model( $episodio = new \App\Models\Episodio, ['method' => 'POST', 'route' => ['sistema::midia::sagas::episodios::guardar', $saga->slug], 'files' => 'true'  ]) !!}

    @include('sistema.episodios.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::sagas::index'])

    {!! Form::close() !!}

@stop
