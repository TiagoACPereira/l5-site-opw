<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">{{ trans('layout.sistema.episodios.partials.form.input-title') }}</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.episodios.partials.form.input-title') ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="numero" class="label label-default">{{ trans('layout.sistema.episodios.partials.form.input-number') }}</label>
        {!! Form::number('numero', null, [ 'class' => 'form-control', 'placeholder' => '0' ] ) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.imagem-form', ['label' => trans('layout.generic.label-image-host'), 'model' => $episodio])

    @include('sistema.partials.recaptcha')

    <div class="col-sm-6">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    {{ trans('layout.sistema.episodios.partials.form.label-double-episode') }}
                    <div class="material-switch pull-right">
                        {!! Form::checkbox('isDuplo', null, $episodio->isDuplo(), ['id' => 'isDuplo']); !!}
                        <label for="isDuplo" class="label-success"></label>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    {{ trans('layout.sistema.episodios.partials.form.label-filler') }}
                    <div class="material-switch pull-right">
                        {!! Form::checkbox('isFiller', null, $episodio->isFiller(), ['id' => 'isFiller']); !!}
                        <label for="isFiller" class="label-success"></label>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>

@include('sistema.formatosmidia.partials.container', ['model' => $episodio])

@include('sistema.partials.script_loadView')