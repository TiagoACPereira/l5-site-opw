@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.episodios.editar.header') }} {{$saga->titulo}}
            <br/>
            <small>{{ trans('layout.sistema.episodios.editar.sub-header') }} {{$episodio->str_numero}}</small>
        </h1>
    </div>

    {!! Form::model( $episodio, [ 'method' => 'PATCH', 'route' => ['sistema::midia::sagas::episodios::actualizar', $episodio->slug ], 'files' => 'true'  ]) !!}

        <div class="row">
            <div class="col-md-12">
                <label class="label label-default" for="oldImage">{{ trans('layout.sistema.episodios.editar.label-image') }}</label>
                <br/>
                <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                     src="{{ $episodio->getImagem()->getLink() }}" id="oldImage">
            </div>
        </div>

        @include('sistema.episodios.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::midia::sagas::index',
            'deleteNameRoute' => 'sistema::midia::sagas::episodios::apagar',
            'slug' => $episodio->slug
        ])

    {!! Form::close() !!}

@stop
