<?php $random = uniqid(); ?>
<strong><a class="btn btn-default btn-sm" role="button"
           href="{{ route($alterarNameRoute, $slug) }}"><span class="glyphicon glyphicon-edit"></span>
        ALTERAR</a></strong>
{!! Form::open( [ 'route' => [$activarNameRoute, $slug ], 'method' => 'PATCH', 'style' => 'display: inline;', 'id' => 'activar_'.$random]) !!}
    <button type="submit" class="btn btn-success btn-sm" onclick="_activar('activar_{{$random}}'); return false;">
        <span class='glyphicon glyphicon-play'></span> ACTIVAR
    </button>
{!! Form::close() !!}

{!! Form::open( [ 'route' => [$deleteNameRoute, $slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => 'delete_'.$random]) !!}
    <button type="submit" class="btn btn-danger btn-sm" onclick="_delete('delete_{{$random}}'); return false;">
        <span class='glyphicon glyphicon-trash'></span> REMOVER
    </button>
{!! Form::close() !!}

<script>
    function _delete(name) { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById(name).submit(); } }); }
    function _activar(name) { swal({ title: "Activar", text: "Deseja continuar?", type: "info", showCancelButton: true, confirmButtonColor: "#6EC868", confirmButtonText: "Activar", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById(name).submit(); } }); }
</script>
