<div class="{{ $dateClass or 'col-sm-6' }}">
    <label class="label label-default" for="date_publish_at">Dia da Publicação <small>(Opcional)</small></label>
    {!! Form::input('date', 'date_publish_at', null, ['class' => 'form-control']) !!}
    <br/>
</div>

<div class="{{ $timeClass or 'col-sm-6' }}">
    <label class="label label-default" for="time_publish_at">Horário da Publicação <small>(Opcional)</small></label>
    {!! Form::input('time', 'time_publish_at', null, ['class' => 'form-control']) !!}
    <br/>
</div>