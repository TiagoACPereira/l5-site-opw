<div class="{{$classInputHost or 'col-sm-6 clearfix'}}" {!! $extra or '' !!}>
        <label class="label label-default">{{ $label or 'Hospedagem Imagem' }}</label>
        <br/>
    <div>
        <div class="btn-group" data-toggle="buttons">
            <style scoped="scoped"> .btn span.glyphicon {opacity: 0; } .btn.active span.glyphicon { opacity: 1;} </style>
            <label class="btn btn-default {{ ($model->isMyHost()) ? 'active' : ''}}" style="min-width: 140px;">
                {!! Form::radio('imageUploadHost', 'myHost', $model->isMyHost()) !!}
                <span class="glyphicon glyphicon-ok"></span>
                <span class="">&nbsp;&nbsp;&nbsp;&nbsp;Local</span>
            </label>
            <label class="btn btn-default {{ ($model->isImgur()) ? 'active' : '' }}" style="min-width: 140px;">
                {!! Form::radio('imageUploadHost', 'imgur', $model->isImgur()) !!}
                <span class="glyphicon glyphicon-ok"></span>
                <span class="">&nbsp;&nbsp;&nbsp;&nbsp;Imgur</span>
            </label>
        </div>
        </div>
</div>