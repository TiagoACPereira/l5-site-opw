<div class="{{ $recaptchaClass or 'col-sm-6 col-md-6 clearfix' }}">
    <label for="g-recaptcha-response" class="label label-default">Recaptcha</label>
    {!! Recaptcha::render([ 'lang' => app()->getLocale() ]) !!}
    <br/>
</div>