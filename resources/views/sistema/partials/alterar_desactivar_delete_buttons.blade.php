<?php $random = uniqid(); ?>
<strong><a class="btn btn-default btn-sm" role="button"
           href="{{ route($alterarNameRoute, $slug) }}"><span class="glyphicon glyphicon-edit"></span>
        ALTERAR</a></strong>
{!! Form::open( [ 'route' => [$desactivarNameRoute, $slug ], 'method' => 'PATCH', 'style' => 'display: inline;', 'id' => 'desactivar_'.$random]) !!}
    <button type="submit" class="btn btn-warning btn-sm" onclick="_desactivar('desactivar_{{$random}}'); return false;">
        <span class='glyphicon glyphicon-pause'></span> DESACTIVAR
    </button>
{!! Form::close() !!}


{!! Form::open( [ 'route' => [$deleteNameRoute, $slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => 'delete_'.$random]) !!}
    <button type="submit" class="btn btn-danger btn-sm" onclick="_delete('delete_{{$random}}'); return false;">
        <span class='glyphicon glyphicon-trash'></span> REMOVER
    </button>
{!! Form::close() !!}

<script>
    function _delete(name) { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById(name).submit(); } }); }
    function _desactivar(name) { swal({ title: "Desactivar", text: "Deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Desactivar", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById(name).submit(); } }); }
</script>
