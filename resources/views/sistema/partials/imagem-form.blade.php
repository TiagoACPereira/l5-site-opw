<?php $random = '_'. uniqid() ?>
<div class="{{$classInputImagem or 'col-sm-6'}}">
    <a href="#imagem{{$random}}" class="label label-default" role="tab" data-toggle="tab">Imagem Local</a>
    <a href="#inputImagemUrl{{$random}}" class="label label-default" role="tab" data-toggle="tab">Imagem URL</a>
    <div class="tab-content">
        <div role="tabpanel" class="imagem-local tab-pane fade in active" id="imagem{{$random}}">
            <input name="imagem" type="file" value="{{old('imagem')}}" class="form-control">
        </div>
        <div role="tabpanel" class="imagem-url tab-pane fade" id="inputImagemUrl{{$random}}">
            <input name="inputImagemUrl" type="url" class="form-control" placeholder="Link de Imagem">
        </div>
    </div>
    <br/>
    <script>
        $("input[name*='inputImagemUrl']")
                .keyup(function() {
                    var $input = $(this).closest('.tab-content')
                            .children('.imagem-local')
                            .children("input[name*='imagem']");
                    if($(this).val() != ''){
                        $input.replaceWith($input.val('').clone(true));
                    }
                });

        $("input[name*='imagem']").change(function (){
            $(this).closest('.tab-content').children('.imagem-url')
                    .children("input[name*='inputImagemUrl']").val('');
        });
    </script>
</div>


@if(!isset($classInputHost))
    @include('sistema.partials.imagem_hospedagem-form', ['label' => $label, 'model' => $model, 'extra' => 'style="height: 74px;"'])
@else
    @include('sistema.partials.imagem_hospedagem-form', ['label' => $label, 'model' => $model, 'extra' => 'style="height: 74px;"', 'classInputHost' => $classInputHost])
@endif