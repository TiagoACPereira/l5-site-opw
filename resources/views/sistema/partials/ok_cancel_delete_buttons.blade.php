
<div class="row">
    <div class="col-md-4" style="padding-bottom: 15px;">
        <button type="submit" class="btn btn-primary btn-block">
            Guardar <span class='glyphicon glyphicon-ok'></span></button>
    </div>
    {!! Form::close() !!}
    <div class="col-md-4" style="padding-bottom: 15px;">
        <a class="btn btn-warning btn-block" href="{{ route($cancelNameRoute) }}"
           role="button">Cancelar <span class='glyphicon glyphicon-remove-sign'></span></a>
    </div>
    <div class="col-md-4" style="padding-bottom: 15px;">
        {!! Form::open( [ 'route' => [$deleteNameRoute, $slug], 'method' => 'DELETE', 'id' => 'removerForm']) !!}
        <button type="submit" class="btn btn-danger btn-block"
                onclick="_delete(); return false;">
            Remover <span class='glyphicon glyphicon-trash'></span></button>
    </div>
</div><!-- /.row -->

<script>
    function _delete() { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("removerForm").submit(); } }); }
</script>