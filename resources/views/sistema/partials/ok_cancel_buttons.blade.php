<div class="row">
    <div class="col-md-6" style="padding-bottom: 15px;">
        <button type="submit" id="save" class="btn btn-primary btn-block">Criar <span class='glyphicon glyphicon-ok'></span></button>
    </div>
    <div class="col-md-6">
        <a class="btn btn-warning btn-block" href="{{ route($cancelNameRoute) }}" role="button">Cancelar <span class='glyphicon glyphicon-remove'></span></a>
    </div>
</div>