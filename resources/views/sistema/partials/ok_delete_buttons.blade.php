<?php $random = uniqid(); ?>
<strong>
    <a class="btn btn-default btn-sm" role="button" href="{{ route($alterarNameRoute, $slug) }}">
        <span class="glyphicon glyphicon-edit"></span>
        ALTERAR
    </a>
</strong>
{!! Form::open( [ 'route' => [$deleteNameRoute, $slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
    <button type="submit" class="btn btn-danger btn-sm" onclick="_delete_{{$random}}(); return false;">
        <span class='glyphicon glyphicon-trash'></span> REMOVER
    </button>
{!! Form::close() !!}

<script>
    function _delete_{{$random}}() { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); }
</script>
