<div class="col-sm-6">
    <label class="label label-default">Extras</label>
    <ul class="list-group">
        <li class="list-group-item">
            <small>Guardar imagens submetivas por link no host</small>
            <div class="material-switch pull-right">
                {!! Form::checkbox('forceHost', 1, true, ['id' => 'forceHost']); !!}
                <label for="forceHost" class="label-success"></label>
            </div>
        </li>
    </ul>
</div>