<script>

    (function (factory) { /* global define */ if (typeof define === 'function' && define.amd) { /* AMD. Register as an anonymous module.*/ define(['jquery'], factory); } else if (typeof module === 'object' && module.exports) { /* Node/CommonJS*/ module.exports = factory(require('jquery')); } else { /* Browser globals*/ factory(window.jQuery); } }(function ($) { /* Extends plugins for adding hello. - plugin is external module for customizing.*/ $.extend($.summernote.plugins, { /** @param {Object} context - context object has status of editor. */
            'spoiler': function (context) {
                var ui = $.summernote.ui;
                context.memo('button.spoiler', function () {
                    var button = ui.button({
                        contents: '<i class="fa fa-toggle-on"/>',
                        tooltip: 'spoiler',
                        click: function () {
                            var spoiler = $('<div id="spoiler-tag"> ' +
                                    '<div class="text-center"><a class="btn btn-default" type="button" onclick="$(this).closest(\'div #spoiler-tag\').children(\'div .spoiler-body\').toggle(\'slow\'); $(this).blur(); return false;">Spoiler Button</a></div>' +
                                    '<div class="spoiler-body well">Hidden Content</div></div>');

                            context.invoke('editor.insertText', 'hello');
                            context.invoke('editor.insertNode', spoiler.get(0));
                        }
                    });

                    // create jQuery object from button instance.
                    return button.render();;
                });

                // This events will be attached when editor is initialized.
                this.events = {
                    // This will be called after modules are initialized.
                    'summernote.init': function (we, e) {
                    },
                    // This will be called when user releases a key on editable.
                    'summernote.keyup': function (we, e) {
                    }
                };

                // This method will be called when editor is initialized by $('..').summernote();
                // You can create elements for plugin
                this.initialize = function () {
                };

                // This methods will be called when editor is destroyed by $('..').summernote('destroy');
                // You should remove elements on `initialize`.
                this.destroy = function () {
                };
            }
        });
    }));

    $(function () {
        summerNoteForceLoad($('[data-control="summernote"]'));
    });

    function summernoteLoad() {
        var $summernoteTextAreas = $('[data-control="summernote"]');
        //cargar summernote
        summerNoteForceLoad($summernoteTextAreas);

        summernotes = $('.note-editor');
        summernotes.find('.note-toolbar').hide();
        summernotes.each(function (idx, element) {
            var $snote = $(element);
            $snote.find('.note-editable').focus(function () {
                toolbarOnDemand($snote, summernotes)
            });
            $snote.find('.note-codable').focus(function () {
                toolbarOnDemand($snote, summernotes)
            });
        });
    }

    function summerNoteForceLoad($element){
        $element.summernote({
            lang: '{{ app()->getLocale() }}',
            focus: false,
            toolbar : [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video', 'spoiler', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']],
                ['group', []]
            ]
        });
    }

    function toolbarOnDemand($snote) {
        if (!$snote.find('.note-toolbar').hasClass('active')) {
            summernotes.find('.note-toolbar')
                    .removeClass('active')
                    .slideUp();
            $snote.find('.note-toolbar')
                    .addClass('active')
                    .slideDown();
        }
    }
</script>
