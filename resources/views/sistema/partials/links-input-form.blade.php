<div class="col-md-4 col-sm-6 form-group">
    <label for="{{$inputname}}" class="label label-default">{{$placeholder}}</label>
    <button type="button" class="close pull-right" aria-label="Close"
            onclick="$(this).closest('.form-group').hide('slow', function() {$(this).remove(); }); return false;"
            ><span aria-hidden="true">&times;</span></button>
    <input class="form-control" placeholder="{{$placeholder or 'Mirror'}}" name="{{$inputname or 'mirror'}}" type="url" value="{{$valeuInput or ''}}">
</div>
