<script>
    function _loadView($elementID, $route) {
        if ($("#" + $elementID + ":hidden").length)
            $("#" + $elementID).load($route , function() { $(this).toggle('slow') } );
        else
            $("#" + $elementID).toggle('slow', function() {$(this).empty() });
    }

    function _loadViewAppend($element, $route) {
        $("<div>").load($route, function () {
            $element.append($(this).html());
        });
    }

    function _loadViewFormatosMidia($elementID, $route) {
        $("<div>").load($route, function () {
            $("#" + $elementID).append($(this).html());
        });
    }

    function _loadViewAppendByID($elementID, $route) {
        $("<div>").load($route, function () {
            $("#" + $elementID).append($(this).html());
        });
    }
</script>