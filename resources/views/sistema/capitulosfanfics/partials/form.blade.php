{!! Form::hidden('id', $capitulofanfic->id) !!}
{!! Form::hidden('fanfic_id', $fanfic->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">{{ trans('layout.sistema.capitulosfanfic.partials.form.input-title') }}</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.capitulosfanfic.partials.form.input-title') ]) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="numero" class="label label-default">{{ trans('layout.sistema.capitulosfanfic.partials.form.input-number') }}</label>
        {!! Form::number('numero', null, [ 'class' => 'form-control', 'placeholder' => '0' ]) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="link_original" class="label label-default">{{ trans('layout.sistema.capitulosfanfic.partials.form.label-original-link') }}
            <small>{{ trans('layout.generic.optional') }}</small>
        </label>
        {!! Form::text('link_original', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.capitulosfanfic.partials.form.label-original-link') ]) !!}
        <br/>
    </div>

    @include('sistema.partials.guardar_imagens_links-form')

    <div class="container" style="padding-right: 0; padding-left: 0;">
        @include('sistema.partials.recaptcha')

        @include('sistema.partials.imagem_hospedagem-form', ['label' => trans('layout.generic.label-image-host'), 'model' => $capitulofanfic])
    </div>

    <div class="col-sm-12" style="margin-top: 15px;">
        <label class="label label-default" for="strConteudo">{{ trans('layout.sistema.capitulosfanfic.partials.form.label-content') }}</label>

        {!! Form::textarea('strConteudo', null, ['data-control' => 'summernote', 'hidden' => '']) !!}
    </div>

</div>

@section('footer')
    @include('sistema.partials.script_summernote')
@stop