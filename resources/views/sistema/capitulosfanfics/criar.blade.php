@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.capitulosfanfic.criar.header') }} {{$fanfic->titulo}}
            <br/>
            <small>{{ trans('layout.sistema.capitulosfanfic.criar.sub-header') }}</small>
        </h1>
    </div>

    {!! Form::model( $capitulofanfic = new \App\Models\Capitulofanfic, ['method' => 'POST', 'route' => ['sistema::fanfics::capitulos::guardar', $fanfic->slug]]) !!}

    @include('sistema.capitulosfanfics.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::fanfics::index'])

    {!! Form::close() !!}

@stop
