@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.capitulosfanfic.editar.header') }} {{$fanfic->titulo}}
            <br/>
            <small>{{ trans('layout.sistema.capitulosfanfic.editar.sub-header') }}</small>
        </h1>
    </div>

    {!! Form::model( $capitulofanfic, [ 'method' => 'PATCH', 'route' => ['sistema::fanfics::capitulos::actualizar', $capitulofanfic->slug ]]) !!}

    @include('sistema.capitulosfanfics.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::fanfics::index',
        'deleteNameRoute' => 'sistema::fanfics::capitulos::apagar',
        'slug' => $capitulofanfic->slug
    ])

    {!! Form::close() !!}
@stop