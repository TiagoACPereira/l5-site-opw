@extends('sistema.app')

@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>{{ trans('layout.sistema.index.hello') }} <strong>{{ $user['login'] }}!</strong></h1>
            <p>{{ trans('layout.sistema.index.welcoming') }}</p>

            <br/>

            <h2 class="text-center">{{ trans('layout.sistema.index.birthday.heading') }}</h2>

            <div class="panel panel-default">
                <div class="panel-body">
                    <img src="/imagens/sistema/balao.png" width="35px" class="img-responsive pull-right">
                    <img src="/imagens/sistema/balao.png" width="35px" class="img-responsive pull-left">
                    @forelse($usersAniversarios as $userAniv)
                        <p class="text-center">
                            <b>{{ trans('layout.sistema.index.birthday.day') }} {{ $userAniv['diaAniversario'] }} </b> - {{$userAniv['nick']}} / {{ $userAniv['nome'] }}
                            @if ($userAniv['diaAniversario'] == date('d'))
                                <span style="color: #D63E71;"><b><small>{{ trans('layout.sistema.index.birthday.today') }}</small></b></span>
                            @endif
                        </p>
                    @empty
                        <p class="text-center">
                            {{ trans('layout.sistema.index.birthday.empty') }} <i class="fa fa-frown-o"></i>
                        </p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@stop