@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.fanfics.index.header') }}
            <span class="label label-{{ ($numero_fanfics == 0)? 'danger' : 'primary' }}">{{ $numero_fanfics }}</span>
            <br/>
            <small><a href="{{ route('sistema::fanfics::criar') }}">{{ trans('layout.sistema.fanfics.index.sub-header') }}</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($fanfics as $fanfic)

            <li class="list-group-item clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>{{ trans('layout.sistema.fanfics.index.label-title') }}</strong> {{ $fanfic->titulo }} |
                    <strong>{{ trans('layout.sistema.fanfics.index.label-registered') }}</strong> {{ $fanfic->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    <button onclick="_show('fanfic{{$fanfic->id}}'); return false;" type="button"
                            class="btn btn-default btn-sm dropdown-toggle">
                        <span class="label label-{{ ($fanfic->numeroCapitulos() == 0)? 'danger' : 'primary' }}">{{ $fanfic->numeroCapitulos() }}</span>
                        {{ trans('layout.sistema.fanfics.index.label-chapters') }}
                    </button>
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::fanfics::editar', 'deleteNameRoute' => 'sistema::fanfics::apagar', 'slug' => $fanfic->slug ])
                </span>
            </li>

            @if($fanfic->numeroCapitulos() != 0)
                <?php $msgTemp = $fanfic->numeroCapitulos() . " " . trans('layout.sistema.fanfics.index.label-number-chapters'); ?>
            @else
                <?php $msgTemp = trans('layout.sistema.fanfics.index.empty-empty'); ?>
            @endif

            <li class="list-group-item fanfic{{$fanfic->id}} clearfix" style="display: none; background-color: #f5f5f5">
                    <span style="display: inline-block; padding: 6px 12px;">
                        <strong>{{ $msgTemp }}</strong>
                    </span>
                    <span class="pull-right">
                        <a class="btn btn-default btn-sm" role="button"
                           href="{{ route('sistema::fanfics::capitulos::criar', $fanfic->slug) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                            {{ trans('layout.sistema.fanfics.index.input-chapter') }}
                        </a>
                    </span>
            </li>

            @foreach($fanfic->capitulos as $capitulofanfic)
                <li class="list-group-item fanfic{{$fanfic->id}} clearfix" style="display: none; background-color: #f5f5f5">
                    <span style="display: inline-block; padding: 6px 12px;">
                        <strong>{{ trans('layout.sistema.fanfics.index.chapter.label-chapter') }}</strong> {{ $capitulofanfic->numero }} |
                        <strong>{{ trans('layout.sistema.fanfics.index.chapter.label-title') }}</strong> {{ $capitulofanfic->titulo }} |
                        <strong>{{ trans('layout.sistema.fanfics.index.chapter.label-registered') }}</strong> {{ $capitulofanfic->created_at->diffForHumans() }}
                    </span>
                    <span class="pull-right">

                        <?php $random = uniqid(); ?>
                        <strong>
                            <a class="btn btn-default btn-sm" role="button" href="{{ route('sistema::fanfics::capitulos::editar', [$fanfic->slug, $capitulofanfic->slug]) }}">
                                <span class="glyphicon glyphicon-edit"></span>
                                {{ trans('layout.sistema.fanfics.index.chapter.button-edit') }}
                            </a>
                        </strong>
                        {!! Form::open( [ 'route' => ['sistema::fanfics::capitulos::apagar', $capitulofanfic->slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
                        <button class="btn btn-danger btn-sm" onclick="_delete(); return false;">
                            <span class='glyphicon glyphicon-trash'></span> {{ trans('layout.sistema.fanfics.index.chapter.button-delete') }}
                        </button>
                        {!! Form::close() !!}

                        <script>
                            function _delete() { swal({ title: "{!! trans('layout.sistema.fanfics.index.chapter.delete-warning-title') !!}", text: "{!! trans('layout.sistema.fanfics.index.chapter.delete-warning-message') !!}", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); }
                        </script>
                    </span>
                </li>
            @endforeach

        @empty
            <li class="list-group-item">
                <strong>{{ trans('layout.sistema.fanfics.index.empty') }}</strong>
            </li>
        @endforelse
    </ul>

@stop

@section('footer')
    <script>
        function _show($fanficID) {
            $("." + $fanficID).toggle('slow');
        }
    </script>
@stop