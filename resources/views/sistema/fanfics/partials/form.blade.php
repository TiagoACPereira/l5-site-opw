{!! Form::hidden('id', $fanfic->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">{{ trans('layout.sistema.fanfics.partials.form.input-title') }}</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.fanfics.partials.form.input-title') ]) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="autor" class="label label-default">{{ trans('layout.sistema.fanfics.partials.form.input-author') }}
            <small>{{ trans('layout.sistema.fanfics.partials.form.input-sub-author') }}</small>
        </label>
        {!! Form::text('autor', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.fanfics.partials.form.input-author') ]) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="link_original" class="label label-default">{{ trans('layout.sistema.fanfics.partials.form.input-original-link') }}
            <small>{{ trans('layout.generic.optional') }}</small>
        </label>
        {!! Form::text('link_original', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.fanfics.partials.form.input-original-link') ]) !!}
        <br/>
    </div>

    <div class="col-sm-6" style="height: 74px;">
        <label class="label label-default">{{ trans('layout.sistema.fanfics.partials.form.label-status-tag') }}</label>
        <br/>
        <div class="btn-group" data-toggle="buttons">
            <style scoped="scoped"> .btn span.glyphicon {opacity: 0; } .btn.active span.glyphicon { opacity: 1;} </style>
            <label class="btn btn-default {{ ($fanfic->isConcluida()) ? 'active' : '' }}" style="min-width: 140px;">
                {!! Form::radio('status', 'concluida', $fanfic->isConcluida(), ['id' => 'concluida']); !!}
                <span class="glyphicon glyphicon-ok"></span>
                <span class="">&nbsp;&nbsp;&nbsp;&nbsp;{{ trans('layout.sistema.fanfics.partials.form.tag-completed') }}</span>
            </label>
            <label class="btn btn-default {{ ($fanfic->isEmAndamento()) ? 'active' : '' }}" style="min-width: 140px;">
                {!! Form::radio('status', 'em_andamento', $fanfic->isEmAndamento(), ['id' => 'em_andamento']); !!}
                <span class="glyphicon glyphicon-ok"></span>
                <span class="">&nbsp;&nbsp;&nbsp;&nbsp;{{ trans('layout.sistema.fanfics.partials.form.tag-in-progress') }}</span>
            </label>
        </div>
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="tags" class="label label-default">{{ trans('layout.sistema.fanfics.partials.form.input-tag') }}</label>
        {!! Form::select('tags[]', $fanfic->allExistingTags(), null, [ 'id' => 'tags', 'class' => 'form-control',
        'multiple']) !!}
        <br/>
    </div>

    @include('sistema.partials.recaptcha')

</div>

@section('footer')
    <script type="text/javascript">
        $("#tags").select2({
            theme: 'bootstrap',
            placeholder: "{{ trans('layout.sistema.fanfics.partials.form.input-tag-placeholder') }}",
            tags: true,
            width: null,
            createTag: function(newTag) {
                return {
                    id: 'nova:' + newTag.term,
                    text: newTag.term + ' {{ trans('layout.generic.new') }}'
                };
            }
        });
    </script>
@stop