@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.fanfics.editar.header') }}
        </h1>
    </div>

    {!! Form::model( $fanfic, [ 'method' => 'PATCH', 'route' => ['sistema::fanfics::actualizar', $fanfic->slug ]]) !!}

    @include('sistema.fanfics.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::fanfics::index',
        'deleteNameRoute' => 'sistema::fanfics::apagar',
        'slug' => $fanfic->slug
    ])

    {!! Form::close() !!}
@stop