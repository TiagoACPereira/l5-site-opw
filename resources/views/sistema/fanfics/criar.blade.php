@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.fanfics.criar.header') }}
        </h1>
    </div>

    {!! Form::model( $fanfic = new \App\Models\Fanfic, ['method' => 'POST', 'route' => 'sistema::fanfics::guardar']) !!}

    @include('sistema.fanfics.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::fanfics::index'])

    {!! Form::close() !!}

@stop
