@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Volumes Cadastrados
            <span class="label label-{{ ($numero_volumes == 0)? 'danger' : 'primary' }}">{{ $numero_volumes }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::volumes::criar') }}">Adicionar Nova Volume</a></small>
        </h1>
    </div>

    <ul class="list-group">
        @forelse($volumes as $volume)
            <li class="list-group-item {{ ($volume->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block;">
                    <img src="{{ $volume->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Volume </strong> {{ $volume->numero }} |
                    <strong><small>{{ (!$volume->isPublished())? ('A publicar '. $volume->publish_at->diffForHumans()) : ('Publicado '. $volume->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    <button onclick="_loadView('li_volume_{{$volume->slug}}', '{{route('sistema::midia::volumes::mostrar', $volume->slug)}}'); return false;" type="button"
                            class="btn btn-default btn-sm dropdown-toggle">
                        <span class="label label-{{ ($volume->numeroCapitulos() == 0)? 'danger' : 'primary' }}">{{ $volume->numeroCapitulos() }}</span>
                        CAPÍTULOS
                    </button>
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::volumes::editar', 'deleteNameRoute' => 'sistema::midia::volumes::apagar', 'slug' => $volume->slug ])
                </span>
            </li>

            <div id="li_volume_{{$volume->slug}}" style="display: none;">

            </div>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhuma Volume!</strong>
            </li>
        @endforelse
    </ul>

    @include('sistema.partials.script_loadView')
@stop