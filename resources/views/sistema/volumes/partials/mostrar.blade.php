@if($volume->numeroCapitulos() != 0)
    <?php $msgTemp = $volume->numeroCapitulos() . " capítulo nesta volume!!"; ?>
@else
    <?php $msgTemp = "Não foi registado nenhum capítulo nesta volume!"; ?>
@endif

<li  class="list-group-item clearfix" style=" background-color: #f5f5f5">
    <span style="display: inline-block; padding: 6px 12px;">
        <strong>{{ $msgTemp }}</strong>
    </span>
    <span class="pull-right">
        <a class="btn btn-default btn-sm" role="button"
           href="{{ route('sistema::midia::volumes::capitulos::criar', $volume->slug) }}">
            <span class="glyphicon glyphicon-edit"></span>
            Inserir Capítulo
        </a>
    </span>
</li>

@foreach($volume->capitulos as $capitulo)
    <li class="list-group-item {{ ($capitulo->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix" style="background-color: #f5f5f5">
        <span style="display: inline-block; padding: 6px 12px;">
            <strong>Capitulo </strong> {{ $capitulo->numero }} |
            <strong>Título:</strong> {{ $capitulo->titulo }} |
            <strong>Cadastrado: </strong> {{ $capitulo->created_at->diffForHumans() }} |
            <strong><small>{{ (!$capitulo->isPublished())? ('A publicar '. $capitulo->publish_at->diffForHumans()) : ('Publicado '. $capitulo->publish_at->diffForHumans()) }}</small></strong>
        </span>
        <span class="pull-right">
            <?php $random = uniqid(); ?>
            <strong>
                <a class="btn btn-default btn-sm" role="button"
                   href="{{ route('sistema::midia::volumes::capitulos::editar', [$volume->slug, $capitulo->slug]) }}">
                    <span class="glyphicon glyphicon-edit"></span>
                    ALTERAR
                </a>
            </strong>
            {!! Form::open( [ 'route' => ['sistema::midia::volumes::capitulos::apagar', $capitulo->slug ], 'method' => 'DELETE', 'style' => 'display: inline;', 'id' => $random]) !!}
            <button class="btn btn-danger btn-sm" onclick="_delete(); return false;">
                <span class='glyphicon glyphicon-trash'></span> REMOVER
            </button>
            {!! Form::close() !!}

            <script> function _delete() { swal({ title: "Remover", text: "Essa ação é permanente deseja continuar?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Remover", cancelButtonText: "Cancelar" }, function (isConfirm) { if (isConfirm) { document.getElementById("{{$random}}").submit(); } }); } </script>
        </span>
    </li>
@endforeach
