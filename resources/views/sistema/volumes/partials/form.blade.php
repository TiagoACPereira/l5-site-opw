{!! Form::hidden('id', $volume->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-sm-6 col-md-4">
        <label for="numero" class="label label-default">Volume</label>
        {!! Form::number('numero', null, [ 'class' => 'form-control', 'placeholder' => '0' ]) !!}
        <br/>
    </div>

    <div class="col-sm-6 col-md-4">
        <label for="scanlator" class="label label-default">Scanlator</label>
        <select class="form-control select2-todo-tags" data-placeholder="Selecione ou adicione Scanlator" name="scanlator">
            @if(old('scanlator') != null)
                <?php $tempScanlator = old('scanlator'); ?>
            @elseif($volume->scanlator != null)
                <?php $tempScanlator = $volume->scanlator->id; ?>
            @else
                <?php $tempScanlator = null ?>
            @endif

            @forelse($scanlatorsList as $valeu => $nome)
                @if($tempScanlator == $valeu)
                    <option selected value="{{$valeu}}">{{$nome}}</option>
                @else
                    <option value="{{$valeu}}">{{$nome}}</option>
                @endif
            @empty
            @endforelse

            @if (substr(old('scanlator'), 0, 5) == 'nova:')
                <option selected value="{{old('scanlator')}}">{{substr(old('scanlator'), 5)}} (nova)</option>
            @endif
        </select>
        <br/>
    </div>

    <div class="col-sm-6 col-md-4">
        <label for="uploaders[]" class="label label-default">Uploaders</label>
        <select class="form-control select2-todo" data-placeholder="Selecione os Uploaders" name="uploaders[]" multiple>
            @forelse($uploadersList as $valeu => $nome)
                @if(old('uploaders') != null)
                    @forelse(old('uploaders') as $uploaderValeu)
                        @if($uploaderValeu == $valeu)
                            <option selected value="{{$valeu}}">{{$nome}}</option>
                        @else
                            <option value="{{$valeu}}">{{$nome}}</option>
                        @endif
                    @empty
                    @endforelse
                @elseif($volume != null)
                    @if( $volume->isUploader($valeu))
                        <option selected value="{{$valeu}}">{{$nome}}</option>
                    @else
                        <option value="{{$valeu}}">{{$nome}}</option>
                    @endif
                @else
                    <option value="{{$valeu}}">{{$nome}}</option>
                @endif
            @empty
            @endforelse
        </select>
        <br/>
    </div>

    <div class="col-sm-6 col-md-4">
        <label class="label label-default" for="data">Data da Lançamento</label>
        {!! Form::input('date', 'data', null, ['class' => 'form-control']) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form', ['dateClass' => 'col-sm-6 col-md-4', "timeClass" => 'col-sm-6 col-md-4'])

    @include('sistema.partials.imagem-form', ['label' => 'Imagem de Saga', 'model' => $volume, 'classInputImagem' => 'col-md-4 col-sm-6', 'classInputHost' => 'col-md-4 col-sm-6'])

    @include('sistema.partials.recaptcha', ['recaptchaClass' => 'col-sm-6 col-md-4 clearfix'])

    <div class="col-xs-12 container-fluid">

        <div class="panel panel-group">
            <div class="panel panel-info" id="mirrors-panel">
                <div class="panel-heading"
                     onclick="$(this).closest('div #mirrors-panel').children('div .panel-body').toggle('slow'); $(this).children('strong').children('.glyphicon').toggle(); $(this).blur(); return false;">
                    <strong style="display: inline-block; padding: 6px 12px;">
                        <i class="glyphicon glyphicon-eye-close" style="display: none;"></i>
                        <i class="glyphicon glyphicon-eye-open"></i>
                        Mirror's
                    </strong>
                </div>
                <div class="panel-body" style="display: none;" id="links">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-xs-12">
                            <button onclick="_loadViewAppend($(this).closest('#links').children('div #links-append'), '{{route('sistema::forms::mirror::criar',['inputname' => 'mirrors[]', 'placeholder' => 'Mirror' ] )}}'); $(this).blur(); return false;"
                                    type="button" class="btn btn-info btn-block">Adiciona Link <span
                                        class='glyphicon glyphicon-tasks'></span>
                            </button>
                        </div>
                    </div>
                    <div id="links-append" class="row">
                        @if(old('mirrors') != null)
                            @forelse(old('mirrors') as $key => $val)
                                @include('sistema.partials.links-input-form', ['valeuInput' => $val, 'inputname' => 'mirrors[]', 'placeholder' => 'Mirror'])
                            @empty
                            @endforelse
                        @elseif($volume != null)
                            @forelse( $volume->mirrors as $key => $val)
                                @include('sistema.partials.links-input-form', ['valeuInput' => $val->url, 'inputname' => 'mirrors[]', 'placeholder' => 'Mirror'])
                            @empty
                            @endforelse
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('sistema.partials.script_loadView')

    <script type="text/javascript">
        $(".select2-todo").select2({
            theme: 'bootstrap',
            tags: false,
            width: null,
            allowClear: true
        });

        $(".select2-todo-tags").select2({
            theme: 'bootstrap',
            tags: true,
            width: null,
            createTag: function(newTag) {
                return {
                    id: 'nova:' + newTag.term,
                    text: newTag.term + ' (nova)'
                };
            }
        });
    </script>

</div><!-- /.row -->