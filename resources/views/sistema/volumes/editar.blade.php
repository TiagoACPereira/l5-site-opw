@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Volume
        </h1>
    </div>

    {!! Form::model( $volume, ['method' => 'PATCH', 'route' => ['sistema::midia::volumes::actualizar', $volume->id ], 'files' => 'true'  ]) !!}
    <div class="row">
        <div class="col-md-12">
            <label class="label label-default" for="oldImage">Imagem Volume Antiga</label>
            <img style="padding-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                 src="{{ $volume->getImagem()->getLink() }}" id="oldImage">
        </div>
    </div><!-- /.row -->
    @include('sistema.volumes.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::midia::volumes::index',
        'deleteNameRoute' => 'sistema::midia::volumes::apagar',
        'slug' => $volume->slug
    ])

    {!! Form::close() !!}

@stop
