@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Volume
        </h1>
    </div>

    {!! Form::model( $volume = new \App\Models\Volume, [ 'method' => 'POST', 'route' => 'sistema::midia::volumes::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.volumes.partials.form')


    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::volumes::index'])

    {!! Form::close() !!}

@stop