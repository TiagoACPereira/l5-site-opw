@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.canaisvlog.editar.header') }}
        </h1>
    </div>

    {!! Form::model( $canalvlog, ['method' => 'PATCH', 'route' => ['sistema::canaisvlog::actualizar', $canalvlog->id ] ]) !!}

    @include('sistema.canaisvlog.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::canaisvlog::index',
        'deleteNameRoute' => 'sistema::canaisvlog::apagar',
        'slug' => $canalvlog->slug
    ])

    {!! Form::close() !!}

@stop
