@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.canaisvlog.criar.header') }}
        </h1>
    </div>

    {!! Form::model( $canalvlog = new \App\Models\CanalVlog, [ 'method' => 'POST', 'route' => 'sistema::canaisvlog::guardar' ]) !!}

    @include('sistema.canaisvlog.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::canaisvlog::index'])

    {!! Form::close() !!}

@stop