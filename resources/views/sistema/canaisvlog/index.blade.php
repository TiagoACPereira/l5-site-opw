@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>{{ trans('layout.sistema.canaisvlog.index.header') }}
            <span class="label label-{{ ($numero_canaisvlog == 0)? 'danger' : 'primary' }}">{{ $numero_canaisvlog }}</span>
            <br/>
            <small><a href="{{ route('sistema::canaisvlog::criar') }}">{{ trans('layout.sistema.canaisvlog.index.new') }}</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($canaisvlog as $canalvlog)
            <li class="list-group-item clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>{{ trans('layout.sistema.canaisvlog.index.name') }}</strong> {{ $canalvlog->nome }} |
                    <strong>{{ trans('layout.sistema.canaisvlog.index.registered') }} </strong> {{ $canalvlog->created_at->diffForHumans() }}
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::canaisvlog::editar', 'deleteNameRoute' => 'sistema::canaisvlog::apagar', 'slug' => $canalvlog->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>{{ trans('layout.sistema.canaisvlog.index.empty') }}</strong>
            </li>
        @endforelse
    </ul>
@stop