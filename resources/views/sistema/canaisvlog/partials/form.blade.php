{!! Form::hidden('id', $canalvlog->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row">
    <div class="col-md-12">
        <label for="informacao" class="label label-default">{{ trans('layout.sistema.canaisvlog.partials.form.input-name') }}</label>
        {!! Form::text('nome', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.canaisvlog.partials.form.input-name'), 'required' => '' ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="id_channel" class="label label-default">{{ trans('layout.sistema.canaisvlog.partials.form.input-channel-id') }} <small>{{ trans('layout.sistema.canaisvlog.partials.form.choose-one') }}</small></label>
        {!! Form::text('id_channel', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.canaisvlog.partials.form.input-channel-id')] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="nome_channel" class="label label-default">{{ trans('layout.sistema.canaisvlog.partials.form.input-channel-name') }} <small>{{ trans('layout.sistema.canaisvlog.partials.form.choose-one') }}</small></label>
        {!! Form::text('nome_channel', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.canaisvlog.partials.form.input-channel-name')] ) !!}
        <br/>
    </div>

    @include('sistema.partials.recaptcha')
</div>
@section('footer')
    <script>
        $("input[name*='id_channel']")
                .keyup(function() {
                    $("input[name*='nome_channel']").val('')
                });

        $("input[name*='nome_channel']")
                .keyup(function() {
                    $("input[name*='id_channel']").val('')
                });
    </script>
@stop