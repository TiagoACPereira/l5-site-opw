@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Noticia
        </h1>
    </div>

    {!! Form::model( $noticia, [ 'method' => 'PATCH', 'route' => ['sistema::noticias::actualizar', $noticia->slug ]]) !!}

    @include('sistema.noticias.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::noticias::index',
        'deleteNameRoute' => 'sistema::noticias::apagar',
        'slug' => $noticia->slug
    ])

    {!! Form::close() !!}
@stop