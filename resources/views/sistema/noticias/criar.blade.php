@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Noticia
        </h1>
    </div>

    {!! Form::model( $noticia = new \App\Models\Noticia, ['method' => 'POST', 'route' => 'sistema::noticias::guardar']) !!}

    @include('sistema.noticias.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::noticias::index'])

    {!! Form::close() !!}

@stop
