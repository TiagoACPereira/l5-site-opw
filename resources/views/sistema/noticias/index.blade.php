@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Noticias Cadastradas
            <span class="label label-{{ ($numero_noticias == 0)? 'danger' : 'primary' }}">{{ $numero_noticias }}</span>
            <br/>
            <small><a href="{{ route('sistema::noticias::criar') }}">Adicionar Nova Noticia</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($noticias as $noticia)
            <li class="list-group-item {{ ($noticia->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>Título:</strong> {{ $noticia->titulo }} |
                    <strong>Cadastrado: </strong> {{ $noticia->created_at->diffForHumans() }} |
                    <strong><small>{{ (!$noticia->isPublished())? ('A publicar '. $noticia->publish_at->diffForHumans()) : ('Publicado '. $noticia->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::noticias::editar', 'deleteNameRoute' => 'sistema::noticias::apagar', 'slug' => $noticia->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhuma Noticia!</strong>
            </li>
        @endforelse
    </ul>
    <div class="text-center">
        {!! $noticias->links() !!}
    </div>
@stop