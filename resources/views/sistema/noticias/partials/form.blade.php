{!! Form::hidden('id', $noticia->id) !!}
{!! Form::hidden('user_id', Auth::user()['id']) !!}

<div class="row" style="margin-bottom: 15px;">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">Título</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => 'Título' ]) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.guardar_imagens_links-form')

    <div class="container" style="padding-left: 0; padding-right: 0;">
        @include('sistema.partials.recaptcha')

        @include('sistema.partials.imagem_hospedagem-form', ['label' => 'Hospedagem de Imagens', 'model' => $noticia])
    </div>

    <div class="col-sm-6" style="padding-top: 15px; padding-bottom: 15px;">
        <button
                data-componentes="{{(old('noticiaComponente') != null && old('noticiaComponente.midia') != null)? min(array_keys(old('noticiaComponente.midia'))) : -1 }}"
                onclick="adicionarConteudo(this); return false;"
                type="button" class="btn btn-default btn-lg btn-block" >Adiciona Multimídia <span class='glyphicon glyphicon-tasks'></span>
        </button>
    </div>
    <div class="col-sm-6" style="padding-top: 15px; padding-bottom: 15px; align: middle">
        <button
                data-componentes="{{(old('noticiaComponente') != null && old('noticiaComponente.conteudo') != null)? min(array_keys(old('noticiaComponente.conteudo'))) : -1 }}"
                onclick="adicionarConteudo(this); return false;"
                type="button" class="btn btn-default btn-lg btn-block" >Adiciona Conteudo <span class='glyphicon glyphicon-tasks'></span>
        </button>
    </div>

    <style scoped="scoped"> .btn span.btn_checked {display: inline; } .btn span.btn_unchecked {display: none; } .btn.active span.btn_checked { display: none;} .btn.active span.btn_unchecked { display: inline;} </style>
    <div class="container">
        <div class="col-md-12 well">
            <ul id="conteudos" style="list-style-type: none; padding-right: 0; padding-left: 0;">
                @if(old('noticiaComponente') != null)
                    @foreach(old('noticiaComponente') as $oldComponente)
                        @if(isset($oldComponente['conteudo']))
                            @include('sistema.noticiascomponentes.partials.conteudo-form', [
                            'key' => key($oldComponente['conteudo']),
                            'noticia_completa' => ((isset(array_values($oldComponente['conteudo'])[0]['noticiaCompleta']))? true : false),
                            'strConteudo' => array_values($oldComponente['conteudo'])[0]['strConteudo']])
                        @endif
                    @endforeach
                @else
                    @if($noticia->numeroComponentes > 0)
                        @forelse($noticia->componentes as $componente)
                            {!! $componente->form !!}
                        @empty
                        @endforelse
                    @endif
                @endif
            </ul>
        </div>
    </div>
</div>

@section('footer')
    <style>
        .ui-state-highlight {
            border: 1px dashed lightslategray;
            box-sizing: border-box;
            border-spacing: 2px;
            padding: 10px;
            height: 100px;
        }
    </style>

    <script>
        $(function() {
            $( "#conteudos" ).sortable({
                connectWith: "#conteudos",
                handle: ".label-sortable",
                cancel: "div#noticia_componente",
                placeholder: "ui-state-highlight"
            });

            $('[data-selecionavel="false"]').disableSelection();

            summernoteLoad();
        });

        function adicionarConteudo($element) {
            $($element).data('componentes', ($($element).data('componentes') - 1));
            $("#conteudos").append(
                    $('<li></li>').attr('style', 'padding-top: 10px;')
                            .append('<label class="label label-default label-sortable" style="padding-bottom: 2px;">Arrastar Para Ordenar</label>')
                            .append('<a class="btn-group btn-group-xs  label-sortable" data-toggle="buttons" style="margin-left: 15px;"> ' +
                                    '<label class="btn btn-default btn-label" style="min-width: 70px; color: #FFFFFF; background-color: #337ab7; line-height: 1; font-size: 75%; font-weight: bold; padding: 0.2em 0.6em 0.3em;"> ' +
                                    '<input name="noticiaComponente[' + $($element).data('componentes') + '][conteudo][' + $($element).data('componentes') + '][noticiaCompleta]" type="checkbox"> ' +
                                    '<span class="btn_checked">Só Em Noticia Breve</span> <span class="btn_unchecked">Só Em Noticia Completa</span> ' +
                                    '</label> </a>')
                            .append('<a class="label label-default pull-right" style="background-color: #d9534f; line-height: 1; font-size: 75%; font-weight: bold; padding: 0.2em 0.6em 0.3em;" onclick="$(this).closest(\'li\').remove(); return false;"> <span class="glyphicon glyphicon-remove"></span> </a>')
                            .append(
                                    $("<textarea></textarea>").attr('hidden', '').attr('data-control', 'summernote').attr('name', 'noticiaComponente[' + $($element).data('componentes') + '][conteudo][' + $($element).data('componentes') + '][strConteudo]')
                            )
            );

            $($element).blur();

            summernoteLoad();
            summernotes.each(function (idx, element) {
                if ($(element).find('.note-toolbar').hasClass('active'))
                    $(element).find('.note-toolbar').show();
            });
        }
    </script>
    @include('sistema.partials.script_summernote')
@stop
