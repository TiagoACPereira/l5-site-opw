<li style="padding-top: 10px;">
    <label class="label label-default label-sortable" style="padding-bottom: 2px;">Arrastar Para Ordenar</label>
    <a class="btn-group btn-group-xs  label-sortable" data-toggle="buttons" style="margin-left: 15px;">
        <label class="btn btn-default btn-label {{ ($noticia_completa)? 'active' : '' }}" style="min-width: 70px; color: #FFFFFF; background-color: #337ab7; line-height: 1; font-size: 75%; font-weight: bold; padding: 0.2em 0.6em 0.3em;">
            <input name="noticiaComponente[{{$key}}][conteudo][{{$key}}][noticiaCompleta]" type="checkbox" {{ ($noticia_completa)? 'checked' : '' }}>
            <span class="btn_checked">Só Em Noticia Breve</span> <span class="btn_unchecked">Só Em Noticia Completa</span>
        </label>
    </a>
    <a class="label label-default pull-right" style="background-color: #d9534f; line-height: 1; font-size: 75%; font-weight: bold; padding: 0.2em 0.6em 0.3em;" onclick="$(this).closest('li').remove(); return false;"> <span class="glyphicon glyphicon-remove"></span> </a>
    <textarea data-control="summernote" name="noticiaComponente[{{$key}}][conteudo][{{$key}}][strConteudo]" hidden>{!! $strConteudo !!}</textarea>
</li>