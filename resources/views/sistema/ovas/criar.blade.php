@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Ovas
            <br/>
            <small>Novo Ova</small>
        </h1>
    </div>

    {!! Form::model( $ova = new \App\Models\Ova, ['method' => 'POST', 'route' => 'sistema::midia::ovas::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.ovas.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::ovas::index'])

    {!! Form::close() !!}

@stop
