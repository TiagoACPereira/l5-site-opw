<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">Titulo</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => 'Titulo' ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="numero" class="label label-default">Numero</label>
        {!! Form::number('numero', null, [ 'class' => 'form-control', 'placeholder' => '0' ] ) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.imagem-form', ['label' => 'Hospedagem Imagem de Ova', 'model' => $ova])

    @include('sistema.partials.recaptcha')
</div>

@include('sistema.formatosmidia.partials.container', ['model' => $ova])

@include('sistema.partials.script_loadView')