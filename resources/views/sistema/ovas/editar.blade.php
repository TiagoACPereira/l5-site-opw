@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Ovas
            <br/>
            <small>Editar Ova {{$ova->titulo}}</small>
        </h1>
    </div>

    {!! Form::model( $ova, [ 'method' => 'PATCH', 'route' => ['sistema::midia::ovas::actualizar', $ova->slug ], 'files' => 'true'  ]) !!}

    <div class="row">
        <div class="col-md-12">
            <label class="label label-default" for="oldImage">Imagem Antiga</label>
            <br/>
            <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                 src="{{ $ova->getImagem()->getLink() }}" id="oldImage">
        </div>
    </div>

        @include('sistema.ovas.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::midia::ovas::index',
            'deleteNameRoute' => 'sistema::midia::ovas::apagar',
            'slug' => $ova->slug
        ])

    {!! Form::close() !!}

@stop
