@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>Ovas Cadastrados
            <span class="label label-{{ ($numero_ovas == 0)? 'danger' : 'primary' }}">{{ $numero_ovas }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::ovas::criar') }}">Adicionar Novo Ova</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($ovas as $ova)
            <li class="list-group-item {{ ($ova->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <img src="{{ $ova->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>Titulo:</strong> {{ $ova->titulo }} |
                    <strong>Cadastrado: </strong> {{ $ova->created_at->diffForHumans() }}
                    <strong><small>{{ (!$ova->isPublished())? ('A publicar '. $ova->publish_at->diffForHumans()) : ('Publicado '. $ova->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::ovas::editar', 'deleteNameRoute' => 'sistema::midia::ovas::apagar', 'slug' => $ova->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhum ova!</strong>
            </li>
        @endforelse
    </ul>
@stop