@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>{{ trans('layout.sistema.especiais.index.header') }}
            <span class="label label-{{ ($numero_especiais == 0)? 'danger' : 'primary' }}">{{ $numero_especiais }}</span>
            <br/>
            <small><a href="{{ route('sistema::midia::especiais::criar') }}">{{ trans('layout.sistema.especiais.index.sub-header') }}</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($especiais as $especial)
            <li class="list-group-item {{ ($especial->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <img src="{{ $especial->getImagem()->getLink('lista') }}" id="oldImage">
                    <strong>{{ trans('layout.sistema.especiais.index.label-title') }}</strong> {{ $especial->titulo }} |
                    <strong>{{ trans('layout.sistema.especiais.index.label-registered') }}</strong> {{ $especial->created_at->diffForHumans() }}
                    <strong><small>({{ (!$especial->isPublished())? ( trans('layout.generic.label-publishing') .' '. $especial->publish_at->diffForHumans()) : (trans('layout.generic.label-published') .' '. $especial->publish_at->diffForHumans()) }})</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::midia::especiais::editar', 'deleteNameRoute' => 'sistema::midia::especiais::apagar', 'slug' => $especial->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>{{ trans('layout.sistema.especiais.index.empty') }}</strong>
            </li>
        @endforelse
    </ul>
@stop