@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.especiais.criar.header') }}
            <br/>
            <small>{{ trans('layout.sistema.especiais.criar.sub-header') }}</small>
        </h1>
    </div>

    {!! Form::model( $especial = new \App\Models\Especial, ['method' => 'POST', 'route' => 'sistema::midia::especiais::guardar', 'files' => 'true'  ]) !!}

    @include('sistema.especiais.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::midia::especiais::index'])

    {!! Form::close() !!}

@stop
