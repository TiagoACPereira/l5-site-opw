@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            {{ trans('layout.sistema.especiais.editar.header') }}
            <br/>
            <small>{{ trans('layout.sistema.especiais.editar.sub-header') }} {{$especial->titulo}}</small>
        </h1>
    </div>

    {!! Form::model( $especial, [ 'method' => 'PATCH', 'route' => ['sistema::midia::especiais::actualizar', $especial->slug ], 'files' => 'true'  ]) !!}

        <div class="row">
            <div class="col-md-12">
                <label class="label label-default" for="oldImage">{{ trans('layout.sistema.especiais.editar.label-image') }}</label>
                <br/>
                <img style="margin-top: 5px; margin-bottom:15px;" class="img-responsive center-block img-thumbnail"
                     src="{{ $especial->getImagem()->getLink() }}" id="oldImage">
            </div>
        </div>

        @include('sistema.especiais.partials.form')

        @include('sistema.partials.ok_cancel_delete_buttons', [
            'cancelNameRoute' => 'sistema::midia::especiais::index',
            'deleteNameRoute' => 'sistema::midia::especiais::apagar',
            'slug' => $especial->slug
        ])

    {!! Form::close() !!}

@stop
