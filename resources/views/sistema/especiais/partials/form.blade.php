<div class="row">
    <div class="col-sm-6">
        <label for="titulo" class="label label-default">{{ trans('layout.sistema.especiais.partials.form.input-title') }}</label>
        {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.especiais.partials.form.input-title') ] ) !!}
        <br/>
    </div>

    <div class="col-sm-6">
        <label for="sub_titulo" class="label label-default">{{ trans('layout.sistema.especiais.partials.form.input-sub-title') }}</label>
        {!! Form::text('sub_titulo', null, [ 'class' => 'form-control', 'placeholder' => trans('layout.sistema.especiais.partials.form.input-sub-title') ] ) !!}
        <br/>
    </div>

    @include('sistema.partials.publish_at-form')

    @include('sistema.partials.imagem-form', ['label' => trans('layout.generic.label-image-host'), 'model' => $especial])

    @include('sistema.partials.recaptcha')
</div>

@include('sistema.formatosmidia.partials.container', ['model' => $especial])

@include('sistema.partials.script_loadView')