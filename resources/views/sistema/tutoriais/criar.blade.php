@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Criar Tutorial
        </h1>
    </div>

    {!! Form::model( $tutorial = new \App\Models\Tutorial, ['method' => 'POST', 'route' => 'sistema::tutoriais::guardar']) !!}

    @include('sistema.tutoriais.partials.form')

    @include('sistema.partials.ok_cancel_buttons', ['cancelNameRoute' => 'sistema::tutoriais::index'])

    {!! Form::close() !!}

@stop
