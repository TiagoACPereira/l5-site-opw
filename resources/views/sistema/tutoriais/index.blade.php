@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Tutoriais Cadastrados
            <span class="label label-{{ ($numero_tutoriais == 0)? 'danger' : 'primary' }}">{{ $numero_tutoriais }}</span>
            <br/>
            <small><a href="{{ route('sistema::tutoriais::criar') }}">Adicionar Novo Tutorial</a></small>
        </h1>
    </div>
    <ul class="list-group">
        @forelse($tutoriais as $tutorial)
            <li class="list-group-item {{ ($tutorial->isPublished())? 'list-group-item-success' : 'list-group-item-info'}} clearfix">
                <span style="display: inline-block; padding: 6px 12px;">
                    <strong>Seção:</strong> {{ $tutorial->titulo }} |
                    <strong>Cadastrado: </strong> {{ $tutorial->created_at->diffForHumans() }} |
                    <strong><small>{{ (!$tutorial->isPublished())? ('A publicar '. $tutorial->publish_at->diffForHumans()) : ('Publicado '. $tutorial->publish_at->diffForHumans()) }}</small></strong>
                </span>
                <span class="pull-right">
                    @include('sistema.partials.ok_delete_buttons', [ 'alterarNameRoute' => 'sistema::tutoriais::editar', 'deleteNameRoute' => 'sistema::tutoriais::apagar', 'slug' => $tutorial->slug ])
                </span>
            </li>
        @empty
            <li class="list-group-item">
                <strong>Não foi registado nenhum Tutorial!</strong>
            </li>
        @endforelse
    </ul>

@stop