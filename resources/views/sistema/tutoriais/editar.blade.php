@extends('sistema.app')

@section('content')

    <div class="page-header">
        <h1>
            Editar Tutorial
        </h1>
    </div>

    {!! Form::model( $tutorial, [ 'method' => 'PATCH', 'route' => ['sistema::tutoriais::actualizar', $tutorial->slug ]]) !!}

    @include('sistema.tutoriais.partials.form')

    @include('sistema.partials.ok_cancel_delete_buttons', [
        'cancelNameRoute' => 'sistema::tutoriais::index',
        'deleteNameRoute' => 'sistema::tutoriais::apagar',
        'slug' => $tutorial->slug
    ])

    {!! Form::close() !!}
@stop