<div class="row">
    <div class="col-sm-12">
        <button
                data-formatos="{{(old('formato_midia') != null)? min(array_keys(old('formato_midia'))) : -1 }}"
                onclick="$(this).data('formatos', ($(this).data('formatos') - 1)); _loadViewFormatosMidia('formatosmidia', '{{route('sistema::forms::formatosmidia::criar', '')}}/'  + $(this).data('formatos'));  $(this).blur(); return false;"
                type="button" class="btn btn-default btn-lg btn-block" >{{ trans('layout.sistema.formatosmidia.partials.container.button-add-format') }} <span class='glyphicon glyphicon-tasks'></span>
        </button>
    </div>

    <div class="col-sm-12">
        <div id="conteudoAnteriorFormatosMidia">
            <style> .formatosmidia-placeholder { border: 2px dashed #ccc; height: 56px; } </style>
            <script> $(function() { $( "#formatosmidia" ).sortable({ connectWith: "#formatosmidia", handle: ".panel-heading", placeholder: "formatosmidia-placeholder col-xs-6 col-sm-4" }); });</script>
        </div>

        <div id="formatosmidia" style="margin-top: 15px;">
            @if(old('formato_midia') != null)
                @forelse(old('formato_midia') as $key => $val)
                    @include('sistema.formatosmidia.partials.form', ['id' => $key, 'formatoMidia' => null])
                @empty
                @endforelse
            @else
                @if($model->numeroFormatosMidia > 0)
                    @forelse($model->formatosMidia as $formatoMidia)
                        @include('sistema.formatosmidia.partials.form', ['id' => $formatoMidia->id, 'formatoMidia' => $formatoMidia])
                    @empty
                    @endforelse
                @endif
            @endif
        </div>
    </div>
</div>




@include('sistema.partials.script_loadView')

