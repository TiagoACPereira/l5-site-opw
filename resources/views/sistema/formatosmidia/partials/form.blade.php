<div class="formFormatosMidia col-xs-6 col-sm-4">
    <div class="panel panel-group">
        <div class="panel panel-default" id="formato_midia-panel">
            <div class="panel-heading clear-fix" data-toggle="modal" data-target="#{{ $modelName or "myModel" }}">
                <strong style="display: inline-block; padding: 6px 12px;">
                    {{ trans('layout.sistema.formatosmidia.partials.form.header') }}
                </strong>
                <?php $primeiraImagemFormato = true ?>
                <span id="imagens-formato">
                    @forelse($formatosImagemList as $formatoImagem)
                        @if(old('formato_midia.'.$id.'.formato_id') != null)
                            @if(old('formato_midia.'.$id.'.formato_id') == $formatoImagem['id'])
                                <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="height: 30px" >
                            @else
                                <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="display: none; height: 30px" >
                            @endif
                        @elseif($formatoMidia != null)
                            @if($formatoMidia->formato_id == $formatoImagem['id'])
                                <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="height: 30px" >
                            @else
                                <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="display: none; height: 30px" >
                            @endif
                        @else
                            @if($primeiraImagemFormato == true)
                                <?php $primeiraImagemFormato = false ?>
                                <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="height: 30px" >
                            @else
                                <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="display: none; height: 30px" >
                            @endif

                        @endif
                    @empty @endforelse
                </span>


            </div>
        </div>
    </div>

    <div class="modal fade" id="{{ $modelName or "myModel" }}" role="dialog" aria-labelledby="myModalLabel"> {{--MODEL--}}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"> {{--MODEL HEAD--}}
                    <span class="pull-right">
                        <a class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close"
                           onclick="$(this).closest('.formFormatosMidia').hide('slow', function() {$(this).remove(); }); return false;"
                           href=""><i class="glyphicon glyphicon-trash"></i> {{ trans('layout.sistema.formatosmidia.partials.form.button-delete') }}</a>
                    </span>
                    <h4 class="modal-title" id="myModalLabel">
                        <strong style="display: inline-block; padding: 6px 12px;">
                            {{ trans('layout.sistema.formatosmidia.partials.form.header') }}
                        </strong>
                        <?php $primeiraImagemFormato = true ?>
                        <span id="imagens-formato">
                            @forelse($formatosImagemList as $formatoImagem)
                                @if(old('formato_midia.'.$id.'.formato_id') != null)
                                    @if(old('formato_midia.'.$id.'.formato_id') == $formatoImagem['id'])
                                        <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="height: 30px" >
                                    @else
                                        <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="display: none; height: 30px" >
                                    @endif
                                @elseif($formatoMidia != null)
                                    @if($formatoMidia->formato_id == $formatoImagem['id'])
                                        <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="height: 30px" >
                                    @else
                                        <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="display: none; height: 30px" >
                                    @endif
                                @else
                                    @if($primeiraImagemFormato == true)
                                        <?php $primeiraImagemFormato = false ?>
                                        <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="height: 30px" >
                                    @else
                                        <img class="formato-imagem" id="{{$formatoImagem['id']}}" src="{{$formatoImagem['link']}}" style="display: none; height: 30px" >
                                    @endif

                                @endif
                            @empty @endforelse
                        </span>
                    </h4>
                </div> {{--MODEL HEAD--}}

                <div class="modal-body"> {{--MODEL BODY--}}
                    <div class="row">
                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][formato_id]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-format') }}</label>
                            <select class="form-control select2-todo select2-formato" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-format-placeholder') }}" name="formato_midia[{{$id}}][formato_id]">
                                @forelse($formatosList as $valeu => $nome)
                                    @if(old('formato_midia.'.$id.'.formato_id') == $valeu)
                                        <option selected value="{{$valeu}}">{{$nome}}</option>
                                    @elseif($formatoMidia != null)
                                        @if($formatoMidia->formato_id == $valeu)
                                            <option selected value="{{$valeu}}">{{$nome}}</option>
                                        @else
                                            <option value="{{$valeu}}">{{$nome}}</option>
                                        @endif
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][uploaders][]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-uploaders') }}</label>
                            <select class="form-control select2-todo" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-uploaders-placeholder') }}" name="formato_midia[{{$id}}][uploaders][]" multiple>
                                @forelse($uploadersList as $valeu => $nome)
                                    @if(old('formato_midia.'.$id.'.uploaders') != null)
                                        @forelse(old('formato_midia.'.$id.'.uploaders') as $uploaderValeu)
                                            @if($uploaderValeu == $valeu)
                                                <option selected value="{{$valeu}}">{{$nome}}</option>
                                            @else
                                                <option value="{{$valeu}}">{{$nome}}</option>
                                            @endif
                                        @empty
                                        @endforelse
                                    @elseif($formatoMidia != null)
                                        @if( $formatoMidia->isUploader($valeu))
                                            <option selected value="{{$valeu}}">{{$nome}}</option>
                                        @else
                                            <option value="{{$valeu}}">{{$nome}}</option>
                                        @endif
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][fansub]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-fansub') }}</label>
                            <select class="form-control select2-todo-tags" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-fansub-placeholder') }}" name="formato_midia[{{$id}}][fansub]">
                                @if(old('formato_midia.'.$id.'.fansub') != null)
                                    <?php $tempFansub = old('formato_midia.'.$id.'.fansub'); ?>
                                @elseif($formatoMidia != null)
                                    <?php $tempFansub = $formatoMidia->fansub->id; ?>
                                @else
                                    <?php $tempFansub = null ?>
                                @endif

                                @forelse($fansubsList as $valeu => $nome)
                                    @if($tempFansub == $valeu)
                                        <option selected value="{{$valeu}}">{{$nome}}</option>
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse

                                @if (substr(old('formato_midia.'.$id.'.fansub'), 0, 5) == 'nova:')
                                    <option selected value="{{old('formato_midia.'.$id.'.fansub')}}">{{substr(old('formato_midia.'.$id.'.fansub'), 5)}} {{ trans('layout.generic.new') }}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][audio]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-audio') }}</label>
                            <select class="form-control select2-todo-tags" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-audio-placeholder') }}" name="formato_midia[{{$id}}][audio]">
                                @if(old('formato_midia.'.$id.'.audio') != null)
                                    <?php $tempAudio = old('formato_midia.'.$id.'.audio'); ?>
                                @elseif($formatoMidia != null)
                                    <?php $tempAudio = $formatoMidia->audio->id; ?>
                                @else
                                    <?php $tempAudio = null ?>
                                @endif

                                @forelse($audiosList as $valeu => $nome)
                                    @if($tempAudio == $valeu)
                                        <option selected value="{{$valeu}}">{{$nome}}</option>
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse

                                @if (substr(old('formato_midia.'.$id.'.audio'), 0, 5) == 'nova:')
                                    <option selected value="{{old('formato_midia.'.$id.'.audio')}}">{{substr(old('formato_midia.'.$id.'.audio'), 5)}} {{ trans('layout.generic.new') }}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][legenda]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-legend') }}</label>
                            <select class="form-control select2-todo-tags" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-legend-placeholder') }}" name="formato_midia[{{$id}}][legenda]">
                                @if(old('formato_midia.'.$id.'.legenda') != null)
                                    <?php $tempLegenda = old('formato_midia.'.$id.'.legenda'); ?>
                                @elseif($formatoMidia != null)
                                    <?php $tempLegenda = $formatoMidia->legenda->id; ?>
                                @else
                                    <?php $tempLegenda = null ?>
                                @endif

                                @forelse($legendasList as $valeu => $nome)
                                    @if($tempLegenda == $valeu)
                                        <option selected value="{{$valeu}}">{{$nome}}</option>
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse

                                @if (substr(old('formato_midia.'.$id.'.legenda'), 0, 5) == 'nova:')
                                    <option selected value="{{old('formato_midia.'.$id.'.legenda')}}">{{substr(old('formato_midia.'.$id.'.legenda'), 5)}} {{ trans('layout.generic.new') }}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][tamanho]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-size') }}</label>
                            <select class="form-control select2-todo-tags" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-size-placeholder') }}" name="formato_midia[{{$id}}][tamanho]">
                                @if(old('formato_midia.'.$id.'.tamanho') != null)
                                    <?php $tempTamanho = old('formato_midia.'.$id.'.tamanho'); ?>
                                @elseif($formatoMidia != null)
                                    <?php $tempTamanho = $formatoMidia->tamanho->id; ?>
                                @else
                                    <?php $tempTamanho = null ?>
                                @endif

                                @forelse($tamanhosList as $valeu => $nome)
                                    @if($tempTamanho == $valeu)
                                        <option selected value="{{$valeu}}">{{$nome}}</option>
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse

                                @if (substr(old('formato_midia.'.$id.'.tamanho'), 0, 5) == 'nova:')
                                    <option selected value="{{old('formato_midia.'.$id.'.tamanho')}}">{{substr(old('formato_midia.'.$id.'.tamanho'), 5)}} {{ trans('layout.generic.new') }}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-6 form-group">
                            <label for="formato_midia[{{$id}}][extencao]" class="label label-default">{{ trans('layout.sistema.formatosmidia.partials.form.label-extension') }}</label>
                            <select class="form-control select2-todo-tags" data-placeholder="{{ trans('layout.sistema.formatosmidia.partials.form.label-extension-placeholder') }}" name="formato_midia[{{$id}}][extencao]">
                                @if(old('formato_midia.'.$id.'.extencao') != null)
                                    <?php $tempExtencao = old('formato_midia.'.$id.'.extencao'); ?>
                                @elseif($formatoMidia != null)
                                    <?php $tempExtencao = $formatoMidia->extencao->id; ?>
                                @else
                                    <?php $tempExtencao = null ?>
                                @endif

                                @forelse($extencoesList as $valeu => $nome)
                                    @if($tempExtencao == $valeu)
                                        <option selected value="{{$valeu}}">{{$nome}}</option>
                                    @else
                                        <option value="{{$valeu}}">{{$nome}}</option>
                                    @endif
                                @empty
                                @endforelse

                                @if (substr(old('formato_midia.'.$id.'.extencao'), 0, 5) == 'nova:')
                                    <option selected value="{{old('formato_midia.'.$id.'.extencao')}}">{{substr(old('formato_midia.'.$id.'.extencao'), 5)}} {{ trans('layout.generic.new') }}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="panel panel-group">
                            <div class="panel panel-info" id="mirrors-panel">
                                <div class="panel-heading"
                                     onclick="$(this).closest('div #mirrors-panel').children('div .panel-body').toggle('slow'); $(this).children('strong').children('.glyphicon').toggle(); $(this).blur(); return false;">
                                    <strong style="display: inline-block; padding: 6px 12px;">
                                        <i class="glyphicon glyphicon-eye-close" style="display: none;"></i>
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                        {{ trans('layout.sistema.formatosmidia.partials.form.mirror.header') }}
                                    </strong>
                                </div>
                                <div class="panel-body" style="display: none;" id="links">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-12">
                                            <button onclick="_loadViewAppend($(this).closest('#links').children('div #links-append'), '{{route('sistema::forms::mirror::criar',['inputname' => 'formato_midia[' . $id . '][mirrors][]', 'placeholder' => trans('layout.sistema.formatosmidia.partials.form.mirror.placeholder') ] )}}'); $(this).blur(); return false;"
                                                    type="button" class="btn btn-info btn-block">{{ trans('layout.sistema.formatosmidia.partials.form.mirror.add') }} <span
                                                        class='glyphicon glyphicon-tasks'></span>
                                            </button>
                                        </div>
                                    </div>
                                    <div id="links-append" class="row">
                                        @if(old('formato_midia.'.$id.'.mirrors') != null)
                                            @forelse(old('formato_midia.'.$id.'.mirrors') as $key => $val)
                                                @include('sistema.partials.links-input-form', ['valeuInput' => $val, 'inputname' => 'formato_midia[' . $id . '][mirrors][]', 'placeholder' => trans('layout.sistema.formatosmidia.partials.form.mirror.placeholder')])
                                            @empty
                                            @endforelse
                                        @elseif($formatoMidia != null)
                                            @forelse( $formatoMidia->mirrors as $key => $val)
                                                @include('sistema.partials.links-input-form', ['valeuInput' => $val->url, 'inputname' => 'formato_midia[' . $id . '][mirrors][]', 'placeholder' => trans('layout.sistema.formatosmidia.partials.form.mirror.placeholder')])
                                            @empty
                                            @endforelse
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> {{--END MODEL BODY--}}

                <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-block" data-dismiss="modal">Concluir</button>
                </div>

            </div>
        </div>
    </div> {{--END MODEL--}}

    @include('sistema.partials.script_loadView')

    <script type="text/javascript">
        $(".select2-formato")
                .on("select2:select", function (e) {
                    var img = 'img#' + $(this).find(":selected").attr('value');
                    var $imagens = $(this).closest('div .formFormatosMidia').children('div').children('div').children('div').children('span');
                    $imagens.children('img').not(img).hide();
                    $imagens.children(img).show();

                    $imagens = $(this).closest('div .modal-content').children('div').children('h4').children('span');
                    $imagens.children('img').not(img).hide();
                    $imagens.children(img).show();
                })
                .on("select2:unselect", function (e) {
                    var $imagens = $(this).closest('div .formFormatosMidia').children('div').children('div').children('div').children('span');
                    $imagens.children('img').hide();

                    $imagens = $(this).closest('div .modal-content').children('div').children('h4').children('span');
                    $imagens.children('img').hide();
                });

        $(".select2-todo").select2({
            theme: 'bootstrap',
            tags: false,
            width: null,
            allowClear: true
        });

        $(".select2-todo-tags").select2({
            theme: 'bootstrap',
            tags: true,
            width: null,
            createTag: function(newTag) {
                return {
                    id: 'nova:' + newTag.term,
                    text: newTag.term + ' {{ trans('layout.generic.new') }}'
                };
            }
        });
    </script>

    <script>
        /**
         * Vertically center Bootstrap 3 modals so they aren't always stuck at the top
         */
        $(function() {
            function reposition() {
                var modal = $(this),
                        dialog = modal.find('.modal-dialog');
                modal.css('display', 'block');

                // Dividing by two centers the modal exactly, but dividing by three
                // or four works better for larger screens.
                dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
            }
            // Reposition when a modal is shown
            $('.modal').on('show.bs.modal', reposition);
            // Reposition when the window is resized
            $(window).on('resize', function() {
                $('.modal:visible').each(reposition);
            });
        });
    </script>

</div>