<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during loading views with
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title.system' => 'Painel Administrativo',

    'description.system' => 'Painel Administrativo da OPW!',

    'copyright' => '&copy; Copyright 2007-2015, One Piece World',

    /*
    |--------------------------------------------------------------------------
    | Generic Layout Lines
    |--------------------------------------------------------------------------
    |
    */
    'generic.new' => '(nova)',
    'generic.optional' => '(opcional)',
    'generic.label-image-host' => 'Hospedagem Imagem',
    'generic.mirrors' => 'Mirror\'s',
    'generic.label-publishing' => 'A publicar',
    'generic.label-published' => 'Publicado',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.emails.password'
    |--------------------------------------------------------------------------
    |
    */
    'auth.emails.password.message' => 'Clique aqui para trocar a sua senha:',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.login'
    |--------------------------------------------------------------------------
    |
    */
    'auth.login.panel-heading' => 'Painel Administrativo',
    'auth.login.label-login-input' => 'Login',
    'auth.login.label-password-input' => 'Senha',
    'auth.login.label-remember-check' => ' Manter Sessão Iniciada',
    'auth.login.button-submit' => 'Iniciar Sessão',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.register'
    |--------------------------------------------------------------------------
    |
    */
    'auth.register.panel-heading' => 'Criar Utilizador',
    'auth.register.label-name-input' => 'Nome',
    'auth.register.label-email-input' => 'Email',
    'auth.register.label-login-input' => 'Login',
    'auth.register.label-password-input' => 'Senha',
    'auth.register.label-confirm-password-input' => 'Confirmar Senha',
    'auth.register.button-submit' => 'Registar',
    'auth.register.button-cancel' => 'Cancelar',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.passwords.reset'
    |--------------------------------------------------------------------------
    |
    */

    'auth.passwords.reset.panel-heading' => 'Trocar Senha',
    'auth.passwords.reset.label-email-input' => 'Endereço de Email',
    'auth.passwords.reset.label-password-input' => 'Senha',
    'auth.passwords.reset.label-confirm-password-input' => 'Comfirmar Senha',
    'auth.passwords.reset.button-submit' => 'Trocar Senha',


    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.passwords.email'
    |--------------------------------------------------------------------------
    |
    */
    'auth.passwords.email.panel-heading' => 'Trocar Senha',
    'auth.passwords.email.label-email-input' => 'Endereço de Email',
    'auth.passwords.email.button-submit' => ' Enviar Link para Trocar Senha',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.app'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.app.navbar.toggle' => 'Alterar Navegação',
    'sistema.app.navbar.brand' => 'OPW',
    'sistema.app.navbar.fan' => 'Área dos Fãs',
    'sistema.app.navbar.amv' => 'AMV\'s',
    'sistema.app.navbar.vlog' => 'Canais Vlog',
    'sistema.app.navbar.fanfic' => 'Fanfic\'s',
    'sistema.app.navbar.theories' => 'Teorias',
    'sistema.app.navbar.bio' => 'Biografias',
    'sistema.app.navbar.info' => 'Informações',
    'sistema.app.navbar.multimedia' => 'Multimídia',
    'sistema.app.navbar.episodes' => 'Episódios',
    'sistema.app.navbar.specials' => 'Especiais',
    'sistema.app.navbar.movies' => 'Filmes',
    'sistema.app.navbar.formats' => 'Formatos Mídia',
    'sistema.app.navbar.hentais' => 'Hentais',
    'sistema.app.navbar.games' => 'Jogos/Roms',
    'sistema.app.navbar.lyrics' => 'Letras',
    'sistema.app.navbar.manga' => 'Mangás',
    'sistema.app.navbar.ova' => 'Ovas',
    'sistema.app.navbar.ost' => 'Osts',
    'sistema.app.navbar.news' => 'Notícias',
    'sistema.app.navbar.partners' => 'Parceiros',
    'sistema.app.navbar.slider' => 'Slider',
    'sistema.app.navbar.tutorials' => 'Tutoriais',
    'sistema.app.navbar.users' => 'Usuários',
    'sistema.app.navbar.logout' => 'Sair',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.index.hello' => 'Olá',
    'sistema.index.welcoming' => 'Seja bem-vindo(a) ao sistema, selecione uma opção para dar andamento ao que deseja fazer no navegador em cima!',
    'sistema.index.birthday.heading' => 'Aniversariantes deste Mês',
    'sistema.index.birthday.day' => 'Dia',
    'sistema.index.birthday.today' => '(É HOJE! FELIZ ANIVERSÁRIO!)',
    'sistema.index.birthday.empty' => 'Ninguém faz anos neste Mês',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.amvs.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.amvs.index.header' => 'AMV\'s Cadastrados',
    'sistema.amvs.index.input-youtube-id' => 'ID Video Youtube',
    'sistema.amvs.index.button-submit' => 'Adicionar Novo AMV',
    'sistema.amvs.index.amv.title' => 'Titulo:',
    'sistema.amvs.index.amv.author' => 'Autor:',
    'sistema.amvs.index.amv.registered' => 'Cadastrado:',
    'sistema.amvs.index.amv.button-remove' => 'REMOVER',
    'sistema.amvs.index.amv.delete-warning.title' => 'Remover',
    'sistema.amvs.index.amv.delete-warning.message' => 'Essa ação é permanente deseja continuar?',
    'sistema.amvs.index.amv.empty' => 'Não foi registado nenhum AMV!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.partials.form.input-name' => 'Nome',
    'sistema.canaisvlog.partials.form.input-channel-id' => 'ID Canal Youtube',
    'sistema.canaisvlog.partials.form.input-channel-name' => 'Nome Canal Youtube',
    'sistema.canaisvlog.partials.form.choose-one' => '(Escolher um)',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.criar.header' => 'Adicionar Canal Vlog',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.editar.header' => 'Editar Canal Vlog',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.index.header' => 'Canais Vlog Cadastrados',
    'sistema.canaisvlog.index.new' => 'Adicionar Novo Canal Vlog',
    'sistema.canaisvlog.index.name' => 'Nome:',
    'sistema.canaisvlog.index.registered' => 'Cadastrado:',
    'sistema.canaisvlog.index.empty' => 'Não foi registado nenhum Canal Vlog!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulos.partial.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulos.partial.form.input-title' => 'Título',
    'sistema.capitulos.partial.form.input-number' => 'Numero',
    'sistema.capitulos.partial.form.input-scan' => 'Scanlator',
    'sistema.capitulos.partial.form.input-scan-placeholder' => 'Selecione ou adicione Scanlator',
    'sistema.capitulos.partial.form.label-uploader' => 'Uploaders',
    'sistema.capitulos.partial.form.input-uploader-placeholder' => 'Selecione os Uploaders',
    'sistema.capitulos.partial.form.input-mirror-placeholder' => 'Mirror',
    'sistema.capitulos.partial.form.label-link' => 'Adicionar Link',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulos.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulos.criar.header' => 'Volume',
    'sistema.capitulos.criar.sub-header' => 'Novo Capítulo',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulos.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulos.editar.header' => 'Volume',
    'sistema.capitulos.editar.sub-header' => 'Editar Capítulo',
    'sistema.capitulos.editar.label-image' => 'Imagem Atual',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulosfanfic.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulosfanfic.partials.form.input-title' => 'Título',
    'sistema.capitulosfanfic.partials.form.input-number' => 'Número',
    'sistema.capitulosfanfic.partials.form.label-original-link' => 'Link Original',
    'sistema.capitulosfanfic.partials.form.label-content' => 'Conteudo',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulosfanfic.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulosfanfic.criar.header' => 'Fanfic:',
    'sistema.capitulosfanfic.criar.sub-header' => 'Novo Capítulo',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulosfanfic.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulosfanfic.editar.header' => 'Fanfic:',
    'sistema.capitulosfanfic.editar.sub-header' => 'Editar Capítulo',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.episodios.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.episodios.partials.form.input-title' => 'Título',
    'sistema.episodios.partials.form.input-number' => 'Número',
    'sistema.episodios.partials.form.label-double-episode' => 'Episódio Duplo',
    'sistema.episodios.partials.form.label-filler' => 'Filler',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.episodios.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.episodios.criar.header' => 'Saga:',
    'sistema.episodios.criar.sub-header' => 'Novo Episódio',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.episodios.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.episodios.editar.header' => 'Saga:',
    'sistema.episodios.editar.sub-header' => 'Editar Episódio',
    'sistema.episodios.editar.label-image' => 'Imagem Atual',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.partials.form.input-title' => 'Título',
    'sistema.especiais.partials.form.input-sub-title' => 'Legenda',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.criar.header' => 'Especiais',
    'sistema.especiais.criar.sub-header' => 'Novo Especial',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.editar.header' => 'Especiais',
    'sistema.especiais.editar.sub-header' => 'Editar Especial',
    'sistema.especiais.editar.label-image' => 'Imagem Atual',

    /*
|--------------------------------------------------------------------------
| Layout Lines Of View 'sistema.especiais.index'
|--------------------------------------------------------------------------
|
*/
    'sistema.especiais.index.header' => 'Especiais Cadastrados',
    'sistema.especiais.index.sub-header' => 'Adicionar Novo Especial',
    'sistema.especiais.index.label-title' => 'Título:',
    'sistema.especiais.index.label-registered' => 'Cadastrado:',
    'sistema.especiais.index.empty' => 'Não foi registado nenhum especial!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.partials.form.input-title' => 'Título',
    'sistema.fanfics.partials.form.input-author' => 'Autor',
    'sistema.fanfics.partials.form.input-sub-author' => '(quem escreveu)',
    'sistema.fanfics.partials.form.input-original-link' => 'Link Original',
    'sistema.fanfics.partials.form.input-tag' => 'Tags',
    'sistema.fanfics.partials.form.input-tag-placeholder' => 'Selecione ou adicione Tags',
    'sistema.fanfics.partials.form.label-status-tag' => 'Estado de Fanfic',
    'sistema.fanfics.partials.form.tag-completed' => 'Concluída',
    'sistema.fanfics.partials.form.tag-in-progress' => 'Em Andamento',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.criar.header' => 'Criar Fanfic',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.editar.header' => 'Editar Fanfic',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.index.header' => 'Fanfic\'s Cadastradas',
    'sistema.fanfics.index.sub-header' => 'Adicionar Nova Fanfic',
    'sistema.fanfics.index.label-title' => 'Título:',
    'sistema.fanfics.index.label-registered' => 'Cadastrado:',
    'sistema.fanfics.index.label-chapters' => 'CAPÍTULOS',
    'sistema.fanfics.index.label-number-chapters' => 'capítulos nesta fanfic!!',
    'sistema.fanfics.index.input-chapter' => 'Inserir Capítulo',
    'sistema.fanfics.index.chapter.label-chapter' => 'Capítulo:',
    'sistema.fanfics.index.chapter.label-title' => 'Título:',
    'sistema.fanfics.index.chapter.label-registered' => 'Cadastrado:',
    'sistema.fanfics.index.chapter.button-edit' => 'ALTERAR',
    'sistema.fanfics.index.chapter.button-delete' => 'REMOVER',
    'sistema.fanfics.index.chapter.delete-warning-title' => 'Remover',
    'sistema.fanfics.index.chapter.delete-warning-message' => 'Essa ação é permanente deseja continuar?',
    'sistema.fanfics.index.empty-empty' => 'Não foi registado nenhuma capítulo nesta fanfic!',
    'sistema.fanfics.index.empty' => 'Não foi registado nenhuma Fanfic!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.partials.form.input-title' => 'Título',
    'sistema.filmes.partials.form.input-number' => 'Número',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.criar.header' => 'Filmes',
    'sistema.filmes.criar.sub-header' => 'Novo Filme',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.editar.header' => 'Filmes',
    'sistema.filmes.editar.sub-header' => 'Editar Filme',
    'sistema.filmes.editar.label-image' => 'Imagem Atual',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.index.header' => 'Filmes Cadastrados',
    'sistema.filmes.index.sub-header' => 'Adicionar Novo Filme',
    'sistema.filmes.index.label-title' => 'Título:',
    'sistema.filmes.index.label-registered' => 'Cadastrado:',
    'sistema.filmes.index.empty' => 'Não foi registado nenhum Filme!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.partials.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.partials.criar.label-create' => 'Criar',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.partials.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.partials.editar.label-save' => 'Guardar',
    'sistema.formatos.partials.editar.label-image' => 'Imagem Formato Mídia Atual',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.partials.form.input-name' => 'Nome',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.index.header' => 'Formatos Midia Cadastrados',
    'sistema.formatos.index.sub-header' => 'Adicionar Formato Midia',
    'sistema.formatos.index.label-title' => 'Nome:',
    'sistema.formatos.index.label-registered' => 'Cadastrado:',
    'sistema.formatos.index.button-edit' => 'ALTERAR',
    'sistema.formatos.index.button-delete' => 'REMOVER',
    'sistema.formatos.index.chapter.delete-warning-title' => 'Remover',
    'sistema.formatos.index.chapter.delete-warning-message' => 'Essa ação é permanente deseja continuar?',
    'sistema.formatos.index.empty' => 'Não foi registado nenhum Formato!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatosmidia.partials.container'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatosmidia.partials.container.button-add-format' => 'Adiciona Formato',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatosmidia.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatosmidia.partials.form.header' => 'Formato:',
    'sistema.formatosmidia.partials.form.button-delete' => 'Remover',
    'sistema.formatosmidia.partials.form.label-format' => 'Formato',
    'sistema.formatosmidia.partials.form.label-format-placeholder' => 'Selecione o Formato',
    'sistema.formatosmidia.partials.form.label-uploaders' => 'Uploaders',
    'sistema.formatosmidia.partials.form.label-uploaders-placeholder' => 'Selecione os Uploaders',
    'sistema.formatosmidia.partials.form.label-fansub' => 'Fansub',
    'sistema.formatosmidia.partials.form.label-fansub-placeholder' => 'Selecione ou adicione Fansub',
    'sistema.formatosmidia.partials.form.label-audio' => 'Audio',
    'sistema.formatosmidia.partials.form.label-audio-placeholder' => 'Selecione ou adicione Audio',
    'sistema.formatosmidia.partials.form.label-legend' => 'Legenda',
    'sistema.formatosmidia.partials.form.label-legend-placeholder' => 'Selecione ou adicione Legenda',
    'sistema.formatosmidia.partials.form.label-size' => 'Tamanho',
    'sistema.formatosmidia.partials.form.label-size-placeholder' => 'Selecione ou adicione Tamanho',
    'sistema.formatosmidia.partials.form.label-extension' => 'Extensão',
    'sistema.formatosmidia.partials.form.label-extension-placeholder' => 'Selecione ou adicione Extensão',
    'sistema.formatosmidia.partials.form.mirror.header' => 'Mirror\'s',
    'sistema.formatosmidia.partials.form.mirror.add' => 'Adiciona Link',
    'sistema.formatosmidia.partials.form.mirror.placeholder' => 'Mirror',

];
