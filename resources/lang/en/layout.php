<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during loading views with
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title.system' => 'Admin Panel',

    'description.system' => 'Admin Panel of OPW!',

    'copyright' => '&copy; Copyright 2007-2015, One Piece World',


    /*
    |--------------------------------------------------------------------------
    | Generic Layout Lines
    |--------------------------------------------------------------------------
    |
    */
    'generic.new' => '(new)',
    'generic.optional' => '(optional)',
    'generic.label-image-host' => 'Image Hosting',
    'generic.mirrors' => 'Mirror\'s',
    'generic.label-publishing' => 'Publishing',
    'generic.label-published' => 'Published',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.emails.password'
    |--------------------------------------------------------------------------
    |
    */
    'auth.emails.password.message' => 'Click here to reset your password:',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.login'
    |--------------------------------------------------------------------------
    |
    */
    'auth.login.panel-heading' => 'Login',
    'auth.login.label-login-input' => 'Login',
    'auth.login.label-password-input' => 'Password',
    'auth.login.label-remember-check' => 'Remember Me',
    'auth.login.button-submit' => 'Login',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.register'
    |--------------------------------------------------------------------------
    |
    */
    'auth.register.panel-heading' => 'Register User',
    'auth.register.label-name-input' => 'Name',
    'auth.register.label-email-input' => 'E-Mail',
    'auth.register.label-login-input' => 'Login',
    'auth.register.label-password-input' => 'Password',
    'auth.register.label-confirm-password-input' => 'Confirm Password',
    'auth.register.button-submit' => 'Register',
    'auth.register.button-cancel' => 'Cancel',


    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.passwords.reset'
    |--------------------------------------------------------------------------
    |
    */
    'auth.passwords.reset.panel-heading' => 'Password Reset',
    'auth.passwords.reset.label-email-input' => 'E-Mail Address',
    'auth.passwords.reset.label-password-input' => 'Password',
    'auth.passwords.reset.label-confirm-password-input' => 'Confirm Password',
    'auth.passwords.reset.button-submit' => 'Reset Password',


    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'auth.passwords.email'
    |--------------------------------------------------------------------------
    |
    */
    'auth.passwords.email.panel-heading' => 'Password Reset',
    'auth.passwords.email.label-email-input' => 'E-Mail Address',
    'auth.passwords.email.button-submit' => ' Send Password Reset Link',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.app'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.app.navbar.toggle' => 'Toggle navigation',
    'sistema.app.navbar.brand' => 'OPW',
    'sistema.app.navbar.fan' => 'Fans Area',
    'sistema.app.navbar.amv' => 'AMV\'s',
    'sistema.app.navbar.vlog' => 'Vlog Channels',
    'sistema.app.navbar.fanfic' => 'Fanfics',
    'sistema.app.navbar.theories' => 'Theories',
    'sistema.app.navbar.bio' => 'Biographies',
    'sistema.app.navbar.info' => 'Informations',
    'sistema.app.navbar.multimedia' => 'Multimedia',
    'sistema.app.navbar.episodes' => 'Episodes',
    'sistema.app.navbar.specials' => 'Specials',
    'sistema.app.navbar.movies' => 'Movies',
    'sistema.app.navbar.formats' => 'Media Formats',
    'sistema.app.navbar.hentais' => 'Hentais',
    'sistema.app.navbar.games' => 'Games/Roms',
    'sistema.app.navbar.lyrics' => 'Lyrics',
    'sistema.app.navbar.manga' => 'Mangás',
    'sistema.app.navbar.ova' => 'Ovas',
    'sistema.app.navbar.ost' => 'Osts',
    'sistema.app.navbar.news' => 'News',
    'sistema.app.navbar.partners' => 'Partners',
    'sistema.app.navbar.slider' => 'Slider',
    'sistema.app.navbar.tutorials' => 'Tutorials',
    'sistema.app.navbar.users' => 'Users',
    'sistema.app.navbar.logout' => 'Logout',


    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.index.hello' => 'Hello',
    'sistema.index.welcoming' => 'Welcome to the System!! You can select an option in the navbar above to start working on what you want!',
    'sistema.index.birthday.heading' => 'Birthdays of this Month',
    'sistema.index.birthday.day' => 'Day',
    'sistema.index.birthday.today' => 'IT\'S TODAY! HAPPY BIRTHDAY!',
    'sistema.index.birthday.empty' => 'Nobody has his birthday this month',


    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.amvs.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.amvs.index.header' => 'Registered AMV\'s',
    'sistema.amvs.index.input-youtube-id' => 'ID Youtube Video',
    'sistema.amvs.index.button-submit' => 'Add New AMV',
    'sistema.amvs.index.amv.title' => 'Title:',
    'sistema.amvs.index.amv.author' => 'Author:',
    'sistema.amvs.index.amv.registered' => 'Registered:',
    'sistema.amvs.index.amv.button-remove' => 'REMOVE',
    'sistema.amvs.index.amv.delete-warning.title' => 'Remove',
    'sistema.amvs.index.amv.delete-warning.message' => 'This action is permanent!! Do you wish to continue?',
    'sistema.amvs.index.amv.empty' => 'No AMV Registered!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.partials.form.input-name' => 'Name',
    'sistema.canaisvlog.partials.form.input-channel-id' => 'ID Youtube Channel',
    'sistema.canaisvlog.partials.form.input-channel-name' => 'Name Youtube Channel',
    'sistema.canaisvlog.partials.form.choose-one' => '(Choose One)',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.criar.header' => 'Add Vlog Channel',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.editar.header' => 'Edit Vlog Channel',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.canaisvlog.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.canaisvlog.index.header' => 'Registered Vlog Channels',
    'sistema.canaisvlog.index.new' => 'Add New Vlog Channel',
    'sistema.canaisvlog.index.name' => 'Name:',
    'sistema.canaisvlog.index.registered' => 'Registered:',
    'sistema.canaisvlog.index.empty' => 'No Vlog Channels Registered!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulos.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulos.partials.form.input-title' => 'Title',
    'sistema.capitulos.partials.form.input-number' => 'Number',
    'sistema.capitulos.partials.form.input-scan' => 'Scanlator',
    'sistema.capitulos.partials.form.input-scan-placeholder' => 'Select or Add an Scanlator',
    'sistema.capitulos.partials.form.label-uploader' => 'Uploaders',
    'sistema.capitulos.partials.form.input-uploader-placeholder' => 'Select the Uploaders',
    'sistema.capitulos.partials.form.input-mirror-placeholder' => 'Mirror',
    'sistema.capitulos.partials.form.label-link' => 'Add Link',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulos.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulos.criar.header' => 'Volume',
    'sistema.capitulos.criar.sub-header' => 'New Chapter',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulos.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulos.editar.header' => 'Volume',
    'sistema.capitulos.editar.sub-header' => 'Edit Chapter',
    'sistema.capitulos.editar.label-image' => 'Current Image',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulosfanfic.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulosfanfic.partials.form.input-title' => 'Title',
    'sistema.capitulosfanfic.partials.form.input-number' => 'Number',
    'sistema.capitulosfanfic.partials.form.label-original-link' => 'Original Link',
    'sistema.capitulosfanfic.partials.form.label-content' => 'Content',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulosfanfic.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulosfanfic.criar.header' => 'Fanfic:',
    'sistema.capitulosfanfic.criar.sub-header' => 'New Chapter',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.capitulosfanfic.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.capitulosfanfic.editar.header' => 'Fanfic:',
    'sistema.capitulosfanfic.editar.sub-header' => 'Edit Chapter',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.episodios.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.episodios.partials.form.input-title' => 'Title',
    'sistema.episodios.partials.form.input-number' => 'Number',
    'sistema.episodios.partials.form.label-double-episode' => 'Double Episode',
    'sistema.episodios.partials.form.label-filler' => 'Filler',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.episodios.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.episodios.criar.header' => 'Saga:',
    'sistema.episodios.criar.sub-header' => 'New Episode',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.episodios.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.episodios.editar.header' => 'Saga:',
    'sistema.episodios.editar.sub-header' => 'Edit Episode',
    'sistema.episodios.editar.label-image' => 'Current Image',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.partials.form.input-title' => 'Title',
    'sistema.especiais.partials.form.input-sub-title' => 'Sub Title',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.criar.header' => 'Specials',
    'sistema.especiais.criar.sub-header' => 'New Special',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.editar.header' => 'Specials',
    'sistema.especiais.editar.sub-header' => 'Edit Special',
    'sistema.especiais.editar.label-image' => 'Current Image',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.especiais.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.especiais.index.header' => 'Registered Special',
    'sistema.especiais.index.sub-header' => 'Add New Special',
    'sistema.especiais.index.label-title' => 'Title:',
    'sistema.especiais.index.label-registered' => 'Registered:',
    'sistema.especiais.index.empty' => 'No Special Registered!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.partials.form.input-title' => 'Title',
    'sistema.fanfics.partials.form.input-author' => 'Author',
    'sistema.fanfics.partials.form.input-sub-author' => '(who wrote)',
    'sistema.fanfics.partials.form.input-original-link' => 'Original Link',
    'sistema.fanfics.partials.form.input-tag' => 'Tags',
    'sistema.fanfics.partials.form.input-tag-placeholder' => 'Select or add Tags',
    'sistema.fanfics.partials.form.label-status-tag' => 'Fanfic Status',
    'sistema.fanfics.partials.form.tag-completed' => 'Completed',
    'sistema.fanfics.partials.form.tag-in-progress' => 'In Progress',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.criar.header' => 'Create Fanfic',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.editar.header' => 'Edit Fanfic',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.fanfics.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.fanfics.index.header' => 'Registered Fanfic\'s',
    'sistema.fanfics.index.sub-header' => 'Add New Fanfic',
    'sistema.fanfics.index.label-title' => 'Title:',
    'sistema.fanfics.index.label-registered' => 'Registered:',
    'sistema.fanfics.index.label-chapters' => 'CHAPTERS',
    'sistema.fanfics.index.label-number-chapters' => 'chapters in this fanfic!!',
    'sistema.fanfics.index.input-chapter' => 'Insert Chapter',
    'sistema.fanfics.index.chapter.label-chapter' => 'Chapter:',
    'sistema.fanfics.index.chapter.label-title' => 'Title:',
    'sistema.fanfics.index.chapter.label-registered' => 'Registered:',
    'sistema.fanfics.index.chapter.button-edit' => 'EDIT',
    'sistema.fanfics.index.chapter.button-delete' => 'REMOVE',
    'sistema.fanfics.index.chapter.delete-warning-title' => 'Remove',
    'sistema.fanfics.index.chapter.delete-warning-message' => 'This action is permanent!! Do you wish to continue?',
    'sistema.fanfics.index.empty-empty' => 'No chapters registered in this fanfic!',
    'sistema.fanfics.index.empty' => 'No Fanfic Registered!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.partials.form.input-title' => 'Title',
    'sistema.filmes.partials.form.input-number' => 'Number',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.criar.header' => 'Movies',
    'sistema.filmes.criar.sub-header' => 'New Movie',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.editar.header' => 'Movies',
    'sistema.filmes.editar.sub-header' => 'Edit Movie',
    'sistema.filmes.editar.label-image' => 'Current Image',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.filmes.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.filmes.index.header' => 'Registered Movie',
    'sistema.filmes.index.sub-header' => 'Add New Movie',
    'sistema.filmes.index.label-title' => 'Title:',
    'sistema.filmes.index.label-registered' => 'Registered:',
    'sistema.filmes.index.empty' => 'No Movie Registered!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.partials.criar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.partials.criar.label-create' => 'Create',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.partials.editar'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.partials.editar.label-save' => 'Save',
    'sistema.formatos.partials.editar.label-image' => 'Current Media Format Image',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.partials.form.input-name' => 'Name',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatos.index'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatos.index.header' => 'Registered Media Formats',
    'sistema.formatos.index.sub-header' => 'Add Media Format',
    'sistema.formatos.index.label-title' => 'Name:',
    'sistema.formatos.index.label-registered' => 'Registered:',
    'sistema.formatos.index.button-edit' => 'EDIT',
    'sistema.formatos.index.button-delete' => 'REMOVE',
    'sistema.formatos.index.chapter.delete-warning-title' => 'Remove',
    'sistema.formatos.index.chapter.delete-warning-message' => 'This action is permanent!! Do you wish to continue?',
    'sistema.formatos.index.empty' => 'No Media Format Registered!',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatosmidia.partials.container'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatosmidia.partials.container.button-add-format' => 'Add Format',

    /*
    |--------------------------------------------------------------------------
    | Layout Lines Of View 'sistema.formatosmidia.partials.form'
    |--------------------------------------------------------------------------
    |
    */
    'sistema.formatosmidia.partials.form.header' => 'Format:',
    'sistema.formatosmidia.partials.form.button-delete' => 'Remove',
    'sistema.formatosmidia.partials.form.label-format' => 'Format',
    'sistema.formatosmidia.partials.form.label-format-placeholder' => 'Select a Format',
    'sistema.formatosmidia.partials.form.label-uploaders' => 'Uploaders',
    'sistema.formatosmidia.partials.form.label-uploaders-placeholder' => 'Select the Uploaders',
    'sistema.formatosmidia.partials.form.label-fansub' => 'Fansub',
    'sistema.formatosmidia.partials.form.label-fansub-placeholder' => 'Select or add an Fansub',
    'sistema.formatosmidia.partials.form.label-audio' => 'Audio',
    'sistema.formatosmidia.partials.form.label-audio-placeholder' => 'Select or add an Audio',
    'sistema.formatosmidia.partials.form.label-legend' => 'Legend',
    'sistema.formatosmidia.partials.form.label-legend-placeholder' => 'Select or add an Legend',
    'sistema.formatosmidia.partials.form.label-size' => 'Size',
    'sistema.formatosmidia.partials.form.label-size-placeholder' => 'Select or add an Size',
    'sistema.formatosmidia.partials.form.label-extension' => 'Extension',
    'sistema.formatosmidia.partials.form.label-extension-placeholder' => 'Select or add an Extension',
    'sistema.formatosmidia.partials.form.mirror.header' => 'Mirror\'s',
    'sistema.formatosmidia.partials.form.mirror.add' => 'Add Link',
    'sistema.formatosmidia.partials.form.mirror.placeholder' => 'Mirror',

];
