var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {

    //▼▼▼▼▼▼▼▼▼▼ TODOS OS SASS's ▼▼▼▼▼▼▼▼▼▼

    //principal
    mix.copy('node_modules/bootstrap-sass/assets/fonts', 'public/build/fonts');

    mix.sass([
        'app.scss'
    ], 'public/css/all.css');

    //sistema
    mix.copy('node_modules/font-awesome/fonts', 'public/build/css/fonts');
    mix.copy('node_modules/summernote/dist/font', 'public/build/css/sistema/font');
    mix.copy('node_modules/select2/dist/css/select2.css', 'resources/assets/css/sistema/select2.css');
    mix.copy('node_modules/summernote/dist/summernote.css', 'resources/assets/css/sistema/summernote.css');

    mix.sass([
        'sistema/sistema.scss'
    ], 'resources/assets/css/sistema/main.css');

    mix.styles([
        'sistema/main.css',
        'sistema/select2.css',
        'sistema/summernote.css',
        'sistema/select2-bootstrap.css',
        'sistema/bootstrap-button-circle.css',
        'sistema/bootstrap-button-square.css',
        'sistema/bootstrap-checkbox-fancy.css',
        'sistema/bootstrap-material-switch.css'
    ], 'public/css/sistema/all.css');

    //▲▲▲▲▲▲▲▲▲▲ TODOS OS SASS's ▲▲▲▲▲▲▲▲▲▲

    //▼▼▼▼▼▼▼▼▼▼ TODOS OS JS's ▼▼▼▼▼▼▼▼▼▼

    //principal
    mix.copy('vendor/components/jquery/jquery.min.js', 'resources/assets/js/vendor/jquery.min.js');
    mix.copy('vendor/components/jqueryui/jquery-ui.min.js', 'resources/assets/js/vendor/jquery-ui.min.js');
    mix.copy('vendor/twbs/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js/vendor/bootstrap.min.js');

    mix.scripts([
        'vendor/jquery.min.js',
        'vendor/jquery-ui.min.js'
    ], 'public/js/topo.js');

    mix.scripts([
        'vendor/bootstrap.min.js'
    ], 'public/js/footer.js');

    //sistema
    mix.copy('node_modules/summernote/dist/summernote.min.js', 'resources/assets/js/vendor/summernote.min.js');
    mix.copy('node_modules/summernote/lang/summernote-pt-BR.js', 'resources/assets/js/vendor/summernote-pt-BR.js');
    mix.copy('node_modules/sweetalert/dist/sweetalert.min.js', 'resources/assets/js/vendor/sweetalert.min.js');
    mix.copy('node_modules/select2/dist/js/select2.js', 'resources/assets/js/vendor/select2.js');

    mix.scripts([
        'vendor/sweetalert.min.js',
        'vendor/select2.js'
    ], 'public/js/sistema/topo.js');

    mix.scripts([
        'navbarMaxWidth.js',
        'vendor/summernote.min.js',
        'vendor/summernote-pt-BR.js'
    ], 'public/js/sistema/footer.js');

    //▲▲▲▲▲▲▲▲▲▲ TODOS OS JS's ▲▲▲▲▲▲▲▲▲▲

    mix.version([
        'public/css/all.css',
        'public/css/sistema/all.css',

        'public/js/topo.js',
        'public/js/sistema/topo.js',

        'public/js/footer.js',
        'public/js/sistema/footer.js'
    ]);

});
