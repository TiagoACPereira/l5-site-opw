<?php


namespace App\Traits\Controllers;



trait UploadersTrait
{

    private function syncUploaders($uploaderable_model, $uploaders)
    {
        $uploaderable_model->Uploaders()->sync($uploaders);
    }

}
