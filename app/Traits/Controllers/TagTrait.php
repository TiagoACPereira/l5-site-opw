<?php


namespace App\Traits\Controllers;



use App\Models\Tag;

trait TagTrait
{

    private function syncTag($tagable_model, $requestArray, $tipo)
    {
        if (substr($requestArray[$tipo], 0, 5) == 'nova:')
        {
            $tag = Tag::DeTipo($tipo)->where('nome', substr($requestArray[$tipo], 5))->first();
            if($tag == null){
                $tag = new Tag(['tipo' => $tipo, 'nome' => substr($requestArray[$tipo], 5)]);
                $tag->save();
            }
            $tagable_model->UpdateTag($tipo, $tag->id);
        }else{
            $tagable_model->UpdateTag($tipo, $requestArray[$tipo]);
        }
    }

}
