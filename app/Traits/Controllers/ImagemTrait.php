<?php


namespace App\Traits\Controllers;

use App\Models\Conteudo;
use App\Models\Imagem;
use App\Services\ImgurImage;
use DOMDocument;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

trait ImagemTrait
{

    private function saveImagens(Conteudo $conteudo, $strConteudo, $hostImgur, $forceHost)
    {
        $dom = new DomDocument();

        //necessário para manter com a codificação UTF-8
        $dom->loadHTML(mb_convert_encoding($strConteudo, 'HTML-ENTITIES', 'UTF-8'));

        $images = $dom->getElementsByTagName('img');

        foreach ($images as $img) {
            $this->uploadImagemSummerNote($conteudo, $img, $hostImgur, $forceHost);
        }

        //devolve o html de conteudo mas com tag <body> e </body> extra.
        $strConteudo = $dom->saveHTML($dom->getElementsByTagName('body')->item(0));

        //remove as tag's body
        $strConteudo = substr($strConteudo, 6, strlen($strConteudo) - 13);

        $conteudo->update([
                'strConteudo' => $strConteudo,
                'forceHost' => $forceHost,
                'imgur' => $hostImgur
            ]
        );
    }

    private function uploadImage(Imagem $imagem, $nomeImagemForm = 'imagem', $imgur = false)
    {
        //se imagem já existe
        if ($imagem->nome != null) {
            return $this->updateUploadedImage($imagem, $nomeImagemForm, $imgur);
        }

        $ficheiroImagem = Input::file('imagem');

        if ($imgur) {
            if($ficheiroImagem == null){
                return $this->upload_image_link_imgur($imagem, $nomeImagemForm);
            }else{
                return $this->upload_image_inputFile_imgur($imagem, $ficheiroImagem);
            }
        } else {
            if($ficheiroImagem == null){
                return $this->upload_image_link_local($imagem, $nomeImagemForm);
            }else {
                return $this->upload_image_inputFile_local($imagem, $ficheiroImagem);
            }
        }
    }

    private function updateUploadedImage(Imagem $imagem, $nomeImagemForm = 'imagem', $imgur = false)
    {
        $ficheiro = Input::file($nomeImagemForm);

        if ($imagem->imgur) {
            // apagar imagem guardada do imgur
            $oldImgur = public_path($this->imgurFolder() . file_url_name($imagem->nome));
            if (File::exists($oldImgur)) {
                unlink($oldImgur);
            }
        } else {
            // apagar imagem guardada
            $oldImage = public_path($this->imageFolder() . $imagem->nome);
            if (File::exists($oldImage)) {
                unlink($oldImage);
            }
        }

        if ($imgur) {
            if($ficheiro == null){
                return $this->upload_image_link_imgur($imagem, $nomeImagemForm);
            }else{
                return $this->upload_image_inputFile_imgur($imagem, $ficheiro);
            }
        } else {
            if($ficheiro == null){
                return $this->upload_image_link_local($imagem, $nomeImagemForm);
            }else {
                return $this->upload_image_inputFile_local($imagem, $ficheiro);
            }
        }
    }

    private function changeHostUploadedImage(Imagem $imagem, $imgur)
    {
        if($imgur){
            $oldImage = path_imagem_cache_filename($imagem->nome);

            if ($oldImage == null || !File::exists($oldImage)) {
                return null;
            }

            $imagem = $this->upload_image_file_imgur($imagem, $oldImage);

            unlink($oldImage);
            return $imagem;
        }else{
            return $this->upload_image_file_local($imagem, public_path($this->imgurFolder() . file_url_name($imagem->nome)), true);
        }
    }

    private function uploadImagemSummerNote(Conteudo $conteudo, $imgHtml, $uploadToImgur = false, $forceHost)
    {
        $src = $imgHtml->getAttribute('src');

        //não é valido
        if (!preg_match('/data:image/', $src) && !isUrlImage($src)) {
            $imgHtml->removeAttribute('src');

            return null;
        }

        //se não é novo upload e não é para forçar upload de url, mantem.
        if (!$forceHost && !preg_match('/data:image/', $src)) {
            return null;
        }

        $isNovaImagem = true;

        //imagem é link. tem de se verificar se já tem na base de dados.
        if (!preg_match('/data:image/', $src)) {
            $isUrlImgur = isUrlImgur($src);
            $isUrlNosso = isUrlNosso($src);

            $nomeImagem = ($isUrlImgur) ? $src : file_url_name($src);

            $corretoHost = false;
            if ($isUrlNosso) {
                $corretoHost = $isUrlNosso != $uploadToImgur;
            } else {
                if ($isUrlImgur) {
                    $corretoHost = $isUrlImgur == $uploadToImgur;
                }
            }

            $isImagemExistente = $conteudo->isImagem($nomeImagem);

            //se o conteudo já tiver essa imagem e o correto host
            if ($corretoHost && $isImagemExistente) {
                return null;
            }

            //se for para alterar host
            $isNovaImagem = (!$isUrlNosso && !$isUrlImgur) ||
                ($isUrlImgur && !$isImagemExistente);
        }

        //se chegou aqui é porque é uma imagem nova ou host diferente.
        if ($isNovaImagem) {
            $imagem = $conteudo->RelacaoImagem()->create(Imagem::$defaults);
        } else {
            $imagem = $conteudo->getImagem(isUrlImgur($src) ? $src : file_url_name($src));
        }

        if($uploadToImgur)
        {
            if(preg_match('/data:image/', $src)){
                $this->upload_image_dataImage_imgur($imagem, $src);
            }else{
                $this->upload_image_link_imgur($imagem, $src);
            }
        } else {
            if(preg_match('/data:image/', $src)){
                $this->upload_image_dataImage_local($imagem, $src);
            }else{
                //se for nova imagem tem dados default e imgur é falso
                //se for uma imagem antiga e chegou aqui é pq é imgur
                //ou seja tem na pastaimgur a imagem.
                if($imagem->imgur){
                    $this->upload_image_file_local($imagem, public_path($this->imgurFolder() . file_url_name($imagem->nome)), true);
                }else{
                    $this->upload_image_link_local($imagem, $src);
                }
            }
        }

        $imgHtml->removeAttribute('src');
        $imgHtml->setAttribute('src', $imagem->getLink());

    }

    private function deleteUploadedImagem($nome, $unlink = false)
    {
        if ($nome == null || $nome == '') {
            return false;
        }

        if (isUrlImgur($nome)) {
            $path = public_path($this->imgurFolder() . file_url_name($nome));
            $newPath = public_path($this->deletedImageFolder() . file_url_name($nome));
        } else {
            $path = public_path($this->imageFolder() . $nome);
            $newPath = public_path($this->deletedImageFolder() . $nome);
        }

        if (File::exists($path)) {
            if ($unlink) {
                return unlink($path);
            } else {
                return rename($path, $newPath);
            }
        }

        return false;
    }

    private function upload_image_link_local(Imagem $imagem, $link)
    {
        if (!isUrlImage($link) || $imagem == null) {
            return null;
        }

        //nome imagem
        $extensao = file_url_extension($link);
        $filename = $this->imagePrefix() . uniqid() . '.' . $extensao;

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $filename;
        $imagem->update($newDados);

        //guardar imagem
        $finalPath = public_path($this->imageFolder() . $filename);
        $this->guardar_imagem($extensao, $link, $finalPath);

        return $imagem;
    }

    private function upload_image_link_imgur(Imagem $imagem, $link)
    {
        if (!isUrlImage($link) || $imagem == null) {
            return null;
        }

        //cria imagem temporaria
        if (isUrlNosso($link)) {
            $tempPath = path_imagem_cache_filename(file_url_name($link));
        } else {
            criarDirSeNaoExistir('upload/temp/', true);
            $tempPath = public_path('upload/temp/' . uniqid() . '.' . file_url_extension($link));
            $this->guardar_imagem(file_url_extension($link), $link, $tempPath);
        }

        if ($tempPath == null || !File::exists($tempPath)) {
            return null;
        }

        //faz upload da imagem temporaria
        $imgur = (new ImgurImage())->uploadFile($tempPath);

        if ($imgur == null) {
            return null;
        }

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $imgur['link'];
        $newDados['deletehash'] = $imgur['deletehash'];
        $newDados['imgur'] = true;
        $imagem->update($newDados);

        //Guardar imagem de imgur em hospedagem local
        $finalPath = public_path($this->imgurFolder() . file_url_name($imgur['link']));
        rename($tempPath, $finalPath);

        return $imagem;
    }

    private function upload_image_dataImage_local(Imagem $imagem, $dataImage)
    {
        if (!preg_match('/data:image/', $dataImage) || $imagem == null) {
            return null;
        }

        preg_match('/data:image\/(?<mime>.*?)\;/', $dataImage, $groups);
        $mimetype = $groups['mime'];

        // nome imagem
        $filename = $this->imagePrefix() . uniqid() . '.' . $mimetype;

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $filename;
        $imagem->update($newDados);

        //guardar imagem
        $finalPath = public_path($this->imageFolder() . $filename);
        $this->guardar_imagem($mimetype, $dataImage, $finalPath);

        return $imagem;
    }

    private function upload_image_dataImage_imgur(Imagem $imagem, $dataImage)
    {
        if (!preg_match('/data:image/', $dataImage) || $imagem == null) {
            return null;
        }

        //extensão dataImage
        preg_match('/data:image\/(?<mime>.*?)\;/', $dataImage, $groups);
        $extensao = $groups['mime'];

        //criar imagem temporaria
        criarDirSeNaoExistir('upload/temp/', true);
        $tempPath = public_path('upload/temp/' . uniqid() . '.' . $extensao);
        $this->guardar_imagem($extensao, $dataImage, $tempPath);

        if ($tempPath == null || !File::exists($tempPath)) {
            return null;
        }

        //faz upload da imagem temporaria
        $imgur = (new ImgurImage())->uploadFile($tempPath);

        if ($imgur == null) {
            return null;
        }

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $imgur['link'];
        $newDados['deletehash'] = $imgur['deletehash'];
        $newDados['imgur'] = true;
        $imagem->update($newDados);

        //Guardar imagem de imgur em hospedagem local
        $finalPath = public_path($this->imgurFolder() . file_url_name($imgur['link']));
        rename($tempPath, $finalPath);

        return $imagem;
    }

    private function upload_image_inputFile_local(Imagem $imagem, $ficheiro)
    {
        if (!File::exists($ficheiro->getRealPath()) || $imagem == null) {
            return null;
        }

        //novo nome imagem
        $extensao = $ficheiro->getClientOriginalExtension();
        $filename = $this->imagePrefix() . uniqid() . '.' . $extensao;

        //alterar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $filename;
        $imagem->update($newDados);

        //guardar imagem
        $caminho = public_path($this->imageFolder() . $filename);
        $this->guardar_imagem($extensao, $ficheiro->getRealPath(), $caminho);

        return $imagem;
    }

    private function upload_image_inputFile_imgur(Imagem $imagem, $ficheiro)
    {
        if (!File::exists($ficheiro->getRealPath()) || $imagem == null) {
            return null;
        }

        $imgur = (new ImgurImage())->uploadFile($ficheiro->getRealPath());

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $imgur['link'];
        $newDados['deletehash'] = $imgur['deletehash'];
        $newDados['imgur'] = true;
        $imagem->update($newDados);

        // Guardar Imagem de Imgur no nosso Host
        $finalPath = public_path($this->imgurFolder() . file_url_name($imgur['link']));
        $this->guardar_imagem($ficheiro->getClientOriginalExtension(), $ficheiro->getRealPath(), $finalPath);

        return $imagem;
    }

    private function upload_image_file_local(Imagem $imagem, $path, $moveExistingImage = false)
    {
        if (!File::exists($path) || $imagem == null) {
            return null;
        }

        //nome imagem
        $extensao = file_url_extension($path);
        $filename = $this->imagePrefix() . uniqid() . '.' . $extensao;

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $filename;
        $imagem->update($newDados);

        //guardar imagem
        $finalPath = public_path($this->imageFolder() . $filename);

        if($moveExistingImage) {
            rename($path, $finalPath);
        }else{
            $this->guardar_imagem($extensao, $path, $finalPath);
        }

        return $imagem;
    }

    private function upload_image_file_imgur(Imagem $imagem, $path)
    {
        if (!File::exists($path) || $imagem == null) {
            return null;
        }

        $imgur = (new ImgurImage())->uploadFile($path);

        //guardar dados
        $newDados = Imagem::$defaults;
        $newDados['nome'] = $imgur['link'];
        $newDados['deletehash'] = $imgur['deletehash'];
        $newDados['imgur'] = true;
        $imagem->update($newDados);

        // Guardar Imagem de Imgur no nosso Host
        $finalPath = public_path($this->imgurFolder() . file_url_name($imgur['link']));
        $this->guardar_imagem(file_url_extension($path), $path, $finalPath);

        return $imagem;
    }

    private function guardar_imagem($extenção, $img, $finalPath)
    {
        if (Str::equals('GIF', STR::upper($extenção))) {
            $contentOrFalseOnFailure = file_get_contents($img);
            if ($contentOrFalseOnFailure != false) {
                file_put_contents($finalPath, $contentOrFalseOnFailure);
            }
        } else {
            Image::make($img)->save($finalPath);
        }
    }

    private function imagemUploadedValida($nomeImagemForm = 'imagem')
    {
        return (Input::file($nomeImagemForm) != null && Input::file($nomeImagemForm)->isValid());
    }

    private function imageFolder()
    {
        return criarDirSeNaoExistir(property_exists($this,
            'imageFolder') ? $this->imageFolder : 'upload/', true);
    }

    private function tempFolder()
    {
        return criarDirSeNaoExistir(property_exists($this,
            'tempFolder') ? $this->tempFolder : 'upload/temp/', true);
    }

    private function imgurFolder()
    {
        return criarDirSeNaoExistir(property_exists($this,
            'imgurFolder') ? $this->imgurFolder : $this->imageFolder() . 'imgur/', true);
    }

    private function deletedImageFolder()
    {
        return criarDirSeNaoExistir(property_exists($this,
            'deletedImageFolder') ? $this->deletedImageFolder : $this->imageFolder() . 'deleted/', true);
    }

    private function imagePrefix()
    {
        return property_exists($this, 'imagePrefix') ? $this->imagePrefix : str_random(10);
    }

}
