<?php


namespace App\Traits\Controllers;



trait MirrorsTrait
{

    private function syncMirrors($linkable_model, $mirrors)
    {

        foreach ($linkable_model->mirrors as $mirror)
            $mirror->delete();
        $linkable_model->RelacaoLink()->detach();

        foreach ($mirrors as $idMirror => $mirror)
            $linkable_model->RelacaoLink()->create(['url' => $mirror, 'isDead' => false]);

    }

}
