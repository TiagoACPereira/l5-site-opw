<?php

namespace App\Traits\Controllers;

use App\Models\FormatoMidia;
use App\Models\Tag;

trait FormatoMidiaTrait
{
    use TagTrait, MirrorsTrait, UploadersTrait;

    private function syncFormatosMidia($midia, $request)
    {
        //mirrors atuais
        $formatosAtualizados = [];
        if($request->has('formato_midia'))
        {
            $posicao = 0;
            foreach($request->formato_midia as $id => $formatoRequest)
            {
                $formatoRequest['index'] = $posicao++;
                unset($uploaders);
                if(isset($formatoRequest['uploaders'])){
                    $uploaders = $formatoRequest['uploaders'];
                    unset($formatoRequest['uploaders']);
                }

                unset($mirrors);
                if(isset($formatoRequest['mirrors'])){
                    $mirrors = $formatoRequest['mirrors'];
                    unset($formatoRequest['mirrors']);
                }

                if($midia->isFormato($id))
                {
                    $formato = FormatoMidia::where('id', $id)->first();
                    $formato->update($formatoRequest);

                }else{
                    $formato = $midia->RelacaoFormatoMidia()->create($formatoRequest);
                }

                //fansubs
                $this->syncTag($formato,$formatoRequest, Tag::$TIPO_FANSUB);

                //Audios
                $this->syncTag($formato,$formatoRequest, Tag::$TIPO_AUDIO);

                //Legendas
                $this->syncTag($formato,$formatoRequest, Tag::$TIPO_LEGENDA);

                //Tamanhos
                $this->syncTag($formato,$formatoRequest, Tag::$TIPO_TAMANHO);

                //Extencoes
                $this->syncTag($formato,$formatoRequest, Tag::$TIPO_EXTENCAO);

                //mirrors
                if(isset($mirrors))
                    $this->syncMirrors($formato, $mirrors);
                else
                    foreach ($formato->mirrors as $mirror)
                        $mirror->delete();

                //uploaders
                if(isset($uploaders))
                    $this->syncUploaders($formato, $uploaders);

                $formato->save();

                $formatosAtualizados[] = $formato->id;
            }
        }

        $todosFormatos = $midia->RelacaoFormatoMidia()->lists('id')->toArray();
        // array_diff percorre o primeiro array, e se algum valor nesse array
        // não estiver presente nos formatos actualizados é pq foi removido

        foreach (array_diff($todosFormatos, $formatosAtualizados) as $formatoMidiaId)
        {
            $formatoDelete = FormatoMidia::where('id', $formatoMidiaId)->first();

            foreach ($formatoDelete->mirrors as $mirror)
                $mirror->delete();

            $formatoDelete->delete();
        }
    }

    private function destroyFormatoMidia($midia)
    {
        foreach ($midia->formatosMidia as $formatoMidia)
            if($formatoMidia != null)
            {
                foreach ($formatoMidia->mirrors as $mirror)
                    if($mirror != null)
                        $mirror->delete();

                $formatoMidia->delete();
            }
    }
}