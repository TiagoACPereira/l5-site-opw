<?php

namespace App\Traits\Models;

use App\Models\Tag;

trait TagTrait
{
    public function Tags($tipo)
    {
        return $this->morphToMany(Tag::class, 'tagable')->DeTipo($tipo);
    }

    public function UpdateTag($tipo, $new_tag_id)
    {
        if($this->Tags($tipo)->count() != 0)
            foreach ($this->Tags($tipo)->get() as $tag)
                $this->Tags($tipo)->detach($tag->id); //desnecessario informar o tipo

        $this->Tags($tipo)->attach($new_tag_id);
    }

    public function getAudioAttribute()
    {
        return $this->Tags(Tag::$TIPO_AUDIO)->first();
    }

    public function getScanAttribute()
    {
        return $this->Tags(Tag::$TIPO_SCAN)->first();
    }

    public function getScanlatorAttribute()
    {
        return $this->Tags(Tag::$TIPO_SCANLATOR)->first();
    }

    public function getExtencaoAttribute()
    {
        return $this->Tags(Tag::$TIPO_EXTENCAO)->first();
    }

    public function getFansubAttribute()
    {
        return $this->Tags(Tag::$TIPO_FANSUB)->first();
    }

    public function getFormatoAttribute()
    {
        return $this->Tags(Tag::$TIPO_FORMATO)->first();
    }

    public function getIdiomaAttribute()
    {
        return $this->Tags(Tag::$TIPO_IDIOMA)->first();
    }

    public function getLegendaAttribute()
    {
        return $this->Tags(Tag::$TIPO_LEGENDA)->first();
    }

    public function getTamanhoAttribute()
    {
        return $this->Tags(Tag::$TIPO_TAMANHO)->first();
    }

}