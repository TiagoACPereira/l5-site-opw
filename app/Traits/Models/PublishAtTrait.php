<?php

namespace App\Traits\Models;

use Carbon\Carbon;

trait PublishAtTrait
{

    public function scopePublished($query)
    {
        $query->where('publish_at', '<=', Carbon::now());
    }

    public function scopeUnpublished($query)
    {
        $query->where('publish_at', '>', Carbon::now());
    }

    public function setPublishAtAttribute($data)
    {
        $parte = explode(" ", $data);

        if ($parte[0] == '' && $parte[1] == '') {
            $dataFormatada = Carbon::now();
        } else {
            if ($parte[0] == '') {
                $dataFormatada = Carbon::createFromFormat('H:i', $parte[1]);
            } else {
                if ($parte[1] == '') {
                    $dataFormatada = Carbon::createFromFormat('Y-m-d', $parte[0]);
                } else {
                    $dataFormatada = Carbon::createFromFormat('Y-m-d H:i', $data);
                }
            }
        }

        $this->attributes['publish_at'] = $dataFormatada;
    }

    public function getDatePublishAtAttribute()
    {
        if ($this->publish_at != null) {
            return $this->publish_at->format('Y-m-d');
        }
        return Carbon::now()->format('Y-m-d');
    }

    public function getTimePublishAtAttribute()
    {
        if ($this->publish_at != null) {
            return $this->publish_at->format('H:i');
        }
        return Carbon::now()->format('H:i');
    }

    public function isPublished()
    {
        if($this == null)
            return false;

        $publish_at = $this->attributes['publish_at'];
        if($publish_at == null)
            return false;

        if($publish_at <= Carbon::now())
            return true;

        return false;
    }

}