<?php

namespace App\Traits\Models;


use App\Models\Link;

trait MirrorsTrait
{
    public function RelacaoLink()
    {
        //linkable corresponde ao inicio do nome dado na coluna 'linkable_id' e 'linkable_type'
        return $this->morphToMany(Link::class, 'linkable');
    }

    public function getLinks()
    {
        return $this->RelacaoLink()->getResults();
    }

    public function isMirror($url)
    {
        return $this->RelacaoLink()->where('url', $url)->count() == 1;
    }

    public function getMirrorsAttribute()
    {
        return $this->RelacaoLink()->get();
    }

    public function getNumeroMirrorsAttribute()
    {
        return $this->RelacaoLink()->count();
    }
}