<?php

namespace App\Traits\Models;

use App\Models\Conteudo;

trait ConteudoTrait
{

    public function RelacaoConteudos()
    {
        /* conteudable corresponde ao inicio do nome dado na coluna 'conteudable_id' e 'conteudable_type' */
        return $this->morphToMany(Conteudo::class, 'conteudable');
    }

    public function getConteudo()
    {
        return $this->RelacaoConteudos()->firstOrFail();
    }

    public function getStrConteudo()
    {
        return $this->getConteudo()->attributes['strConteudo'];
    }

    public function getImgurAttribute()
    {
        $conteudo = $this->RelacaoConteudos()->first();
        if($conteudo == null)
            return 0;
        else
            return $conteudo->attributes['imgur'];
    }

    public function setImgurAttribute($imgur)
    {
        $conteudo = $this->RelacaoConteudos()->first();
        if($conteudo != null)
            $this->getConteudo()->attributes['imgur'] = $imgur;
    }

    public function setForceHostAttribute($forceHost)
    {
        $conteudo = $this->RelacaoConteudos()->first();
        if($conteudo != null)
            $this->getConteudo()->attributes['forceHost'] = $forceHost;
    }

    public function getForceHostAttribute()
    {
        $conteudo = $this->RelacaoConteudos()->first();
        if($conteudo == null)
            return 0;
        else
            return $conteudo->attributes['forceHost'];
    }

    public function isImgur()
    {
        return $this->imgur;
    }

    public function isMyHost()
    {
        return !$this->imgur;
    }
}