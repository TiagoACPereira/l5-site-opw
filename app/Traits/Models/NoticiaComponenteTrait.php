<?php

namespace App\Traits\Models;

use App\Models\NoticiaComponente;

trait NoticiaComponenteTrait
{

    public function RelacaoNoticiaComponente()
    {
        return $this->morphToMany(NoticiaComponente::class, 'noticia_componenteable');
    }

    public function getNoticiaComponente()
    {
        return $this->RelacaoNoticiaComponente()->firstOrFail();
    }
}
