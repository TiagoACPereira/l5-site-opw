<?php

namespace App\Traits\Models;


use App\Models\Utilizador;

trait UploadersTrait
{
    public function Uploaders()
    {
        return $this->morphToMany(Utilizador::class, 'uploaderable');
    }

    public function isUploader($id)
    {
        return $this->Uploaders()->where('id', $id)->count() == 1;
    }

    public function getUploadersAttribute()
    {
        return $this->Uploaders()->lists('id')->toArray();
    }

    public function getNumeroUploadersAttribute()
    {
        return $this->Uploaders()->count();
    }

    public function allExistingUploaders()
    {
        return Utilizador::lists('nome', 'id');
    }

}