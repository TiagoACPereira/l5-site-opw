<?php

namespace App\Traits\Models;

use App\Models\Imagem;

trait ImagemTrait
{
    public function RelacaoImagem()
    {
        //imageable corresponde ao inicio do nome dado na coluna 'imageable_id' e 'imageable_type'
        return $this->morphToMany(Imagem::class, 'imageable');
    }

    public function getImagem()
    {
        return $this->RelacaoImagem()->firstOrFail();
    }

    public function isImgur()
    {
        $imagem = $this->RelacaoImagem()->first();
        if($imagem == null)
            return 0;
        else
            return $imagem->attributes['imgur'];
    }

    public function isMyHost()
    {
        $imagem = $this->RelacaoImagem()->first();
        if($imagem == null)
            return 1;
        else
            return !$imagem->attributes['imgur'];
    }
}