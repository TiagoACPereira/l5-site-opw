<?php

namespace App\Traits\Models;

use App\Models\FormatoMidia;

trait FormatoMidiaTrait
{
    public function RelacaoFormatoMidia()
    {
        //formatomidiable corresponde ao inicio do nome dado na coluna 'formatomidiable_id' e 'formatomidiable_type'
        return $this->morphToMany(FormatoMidia::class, 'formatomidiable');
    }

    public function getFormatosMidiaAttribute()
    {
        return $this->RelacaoFormatoMidia()->orderBy('index')->get();
    }

    public function getNumeroFormatosMidiaAttribute()
    {
        return $this->RelacaoFormatoMidia()->count();
    }

    public function isFormato($id)
    {
        return $this->RelacaoFormatoMidia()->where('id', $id)->count() == 1;
    }

    public function getFormatos()
    {
        return $this->RelacaoFormatoMidia()->getResults();
    }
}