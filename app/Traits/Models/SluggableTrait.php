<?php

namespace App\Traits\Models;

trait SluggableTrait
{
    use \Cviebrock\EloquentSluggable\SluggableTrait;

    /**
     * Simple find by Slug. If it's null try by id. Fail if not found.
     *
     * @param $slug
     * @return Model|Collection
     */
    public static function findBySlugOrIdOrFail($slug)
    {
        $model = self::findBySlug($slug);

        if($model != null)
            return $model;

        return self::findOrFail($slug);
    }

    /**
     * Simple find by Slug first. If not find by Id.
     *
     * @param $slug
     * @return Model|Collection|null
     */
    public static function findBySlugOrId($slug)
    {

        $model = self::findBySlug($slug);

        if($model != null)
            return $model;

        return self::find($slug);
    }

}