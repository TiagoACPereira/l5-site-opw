<?php

namespace App\Traits\Requests;


trait FormatoMidiaTrait
{
     private function regrasFormatoMidia($regras)
    {
        if(request()->get('formato_midia') != null)
            foreach (request()->get('formato_midia') as $key => $val) {
                $regras['formato_midia.' . $key . '.formato_id'] = 'required';
                $regras['formato_midia.' . $key . '.uploaders'] = 'required';
                $regras['formato_midia.' . $key . '.fansub'] = 'required';
                $regras['formato_midia.' . $key . '.audio'] = 'required';
                $regras['formato_midia.' . $key . '.legenda'] = 'required';
                $regras['formato_midia.' . $key . '.tamanho'] = 'required';
                $regras['formato_midia.' . $key . '.extencao'] = 'required';
                if(isset($val['mirrors']))
                    foreach ($val['mirrors'] as $keyMirror => $valMirror) {
                        $regras['formato_midia.' . $key . '.mirrors.'. $keyMirror] = 'required';
                    }
            }
        return $regras;
    }

    private function mensagensFM($mensagens)
    {
        if(request()->get('formato_midia') != null)
            foreach (request()->get('formato_midia') as $key => $val) {
                $mensagens['formato_midia.' . $key . '.formato_id.required'] = 'Falta escolher o Formato num dos Formatos Midia.';
                $mensagens['formato_midia.' . $key . '.uploaders.required'] = 'Falta escolher um Uploader num dos Formatos Midia.';
                $mensagens['formato_midia.' . $key . '.fansub.required'] = 'Falta introduzir/escolher uma Fansub num dos Formatos Midia.';
                $mensagens['formato_midia.' . $key . '.audio.required'] = 'Falta introduzir/escolher um audio num dos Formatos Midia.';
                $mensagens['formato_midia.' . $key . '.legenda.required'] = 'Falta introduzir/escolher uma legenda num dos Formatos Midia.';
                $mensagens['formato_midia.' . $key . '.tamanho.required'] = 'Falta introduzir/escolher um tamanho num dos Formatos Midia.';
                $mensagens['formato_midia.' . $key . '.extencao.required'] = 'Falta introduzir/escolher uma extenção num dos Formatos Midia.';
                if(isset($val['mirrors']))
                    foreach ($val['mirrors'] as $keyMirror => $valMirror) {
                        $mensagens['formato_midia.' . $key . '.mirrors.'. $keyMirror . '.required'] = 'Tem um mirror vazio num dos Formatos.';
                    }
            }

        return $mensagens;

    }
}