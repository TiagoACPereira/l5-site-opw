<?php

namespace App\Traits\Requests;


trait RecaptchaTrait
{
     private function regrasRecaptcha($regras)
    {
        $regras['g-recaptcha-response'] = 'required|recaptcha';

        return $regras;
    }

}