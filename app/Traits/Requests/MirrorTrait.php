<?php

namespace App\Traits\Requests;


trait MirrorTrait
{
    private function regrasMirror($regras)
    {
        if(request()->has('mirrors'))
            foreach (request()->get('mirrors') as $keyMirror => $valMirror) {
                $regras['mirrors.'. $keyMirror] = 'required';
            }
        return $regras;
    }

    private function mensagensMirror($mensagens)
    {
        if(request()->has('mirrors'))
            foreach (request()->get('mirrors') as $keyMirror => $valMirror) {
                $mensagens['mirrors.'. $keyMirror . '.required'] = 'Tem um mirror vazio.';
            }
        return $mensagens;
    }
}