<?php

namespace App\Traits\Requests;


use Illuminate\Support\Facades\Route;

trait ImagemTrait
{
    private function regrasImagem($regras, $routeCriar, $routeAtualizar)
    {
        $routeName = Route::getCurrentRoute()->getName();

        if ($routeName == $routeCriar) {
            $regras['imagem'] = 'required_if:inputImagemUrl,|image';
            $regras['inputImagemUrl'] = 'required_if:imagem,null|url';
        }

        if ($routeName == $routeAtualizar) {
            $regras['imagem'] = 'image';
            $regras['inputImagemUrl'] = 'url';
        }

        return $regras;
    }

    private function mensagensImagem($mensagens)
    {
        $mensagens['imagem.image']  = 'A imagem não é uma imagem válida.';
        $mensagens['imagem.required_if']  = 'Se não introduzir um url de imagem, tem de fazer upload de uma local.';
        $mensagens['inputImagemUrl.url']  = 'A url da imagem introduzido é inválida.';
        $mensagens['inputImagemUrl.required_if']  = 'Se não fizer upload de uma imagem local, tem de introduzir um url com a imagem.';

        return $mensagens;
    }
}