<?php

namespace App\Image\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class FiltroLista implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        // resize the image to a height of 30 and constrain aspect ratio (auto width)
        // prevent possible upsizing
        $image->resize(null, 30, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        return $image;
    }
}
