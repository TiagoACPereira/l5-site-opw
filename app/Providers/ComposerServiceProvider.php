<?php

namespace App\Providers;

use App\Http\Controllers\Sistema\OstsController;
use App\Http\ViewComposers\CapituloComposer;
use App\Http\ViewComposers\FormatoMidiaComposer;
use App\Http\ViewComposers\HentaiComposer;
use App\Http\ViewComposers\JogoComposer;
use App\Http\ViewComposers\OstComposer;
use App\Http\ViewComposers\VolumeComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('sistema.formatosmidia.partials.form', FormatoMidiaComposer::class);
        view()->composer('sistema.jogos.partials.form', JogoComposer::class);
        view()->composer('sistema.hentais.partials.form', HentaiComposer::class);
        view()->composer('sistema.osts.partials.form', OstComposer::class);
        view()->composer('sistema.volumes.partials.form', VolumeComposer::class);
        view()->composer('sistema.capitulos.partials.form', CapituloComposer::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
