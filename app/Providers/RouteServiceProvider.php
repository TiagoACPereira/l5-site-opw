<?php

namespace App\Providers;

use App\Models\Amv;
use App\Models\CanalVlog;
use App\Models\Capitulo;
use App\Models\Capitulofanfic;
use App\Models\Episodio;
use App\Models\Especial;
use App\Models\Fanfic;
use App\Models\Filme;
use App\Models\Formato;
use App\Models\Hentai;
use App\Models\Info;
use App\Models\Jogo;
use App\Models\Letra;
use App\Models\Noticia;
use App\Models\Ost;
use App\Models\Ova;
use App\Models\Parceiro;
use App\Models\Plataforma;
use App\Models\Saga;
use App\Models\Slide;
use App\Models\Teoria;
use App\Models\Tutorial;
use App\Models\Utilizador;
use App\Models\Volume;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
//    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $parametrosNumeros = ['id'];

        foreach ($parametrosNumeros as $parametro) {
            $router->pattern($parametro, '[0-9]+');
        }

        parent::boot($router);

        $router->model('usuarios', Utilizador::class);
        $router->model('amvs', Amv::class);


        $router->bind('slider', function($value) { return Slide::findBySlugOrIdOrFail($value); });
        $router->bind('parceiros', function($value) { return Parceiro::findBySlugOrIdOrFail($value); });
        $router->bind('informacoes', function($value) { return Info::findBySlugOrIdOrFail($value); });
        $router->bind('tutoriais', function($value) { return Tutorial::findBySlugOrIdOrFail($value); });
        $router->bind('teorias', function($value) { return Teoria::findBySlugOrIdOrFail($value); });
        $router->bind('letras', function($value) { return Letra::findBySlugOrIdOrFail($value); });
        $router->bind('canaisvlog', function($value) { return CanalVlog::findBySlugOrIdOrFail($value); });
        $router->bind('fanfics', function($value) { return Fanfic::findBySlugOrIdOrFail($value); });
        $router->bind('capitulosfanfics', function($value) { return Capitulofanfic::findBySlugOrIdOrFail($value); });
        $router->bind('sagas', function($value) { return Saga::findBySlugOrIdOrFail($value); });
        $router->bind('formatos', function($value) { return Formato::findBySlugOrIdOrFail($value); });
        $router->bind('episodios', function($value) { return Episodio::findBySlugOrIdOrFail($value); });
        $router->bind('especiais', function($value) { return Especial::findBySlugOrIdOrFail($value); });
        $router->bind('filmes', function($value) { return Filme::findBySlugOrIdOrFail($value); });
        $router->bind('ovas', function($value) { return Ova::findBySlugOrIdOrFail($value); });
        $router->bind('plataformas', function($value) { return Plataforma::findBySlugOrIdOrFail($value); });
        $router->bind('jogos', function($value) { return Jogo::findBySlugOrIdOrFail($value); });
        $router->bind('hentais', function($value) { return Hentai::findBySlugOrIdOrFail($value); });
        $router->bind('osts', function($value) { return Ost::findBySlugOrIdOrFail($value); });
        $router->bind('volumes', function($value) { return Volume::findBySlugOrIdOrFail($value); });
        $router->bind('capitulos', function($value) { return Capitulo::findBySlugOrIdOrFail($value); });
        $router->bind('noticias', function($value) { return Noticia::findBySlugOrIdOrFail($value); });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
