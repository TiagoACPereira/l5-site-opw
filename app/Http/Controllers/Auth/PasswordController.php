<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use SEO;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        SEO::setTitle(trans('layout.title.system'));
        SEO::setDescription(trans('layout.description.system'));

        SEO::metatags()->addMeta('Content-Language', app()->getLocale(), 'name');
        SEO::metatags()->addMeta('Copyright', trans('layout.copyright'), 'name');


        SEO::metatags()->addMeta('content-language', app()->getLocale(), 'http-equiv');
    }
}
