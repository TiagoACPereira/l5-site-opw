<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\FilmeRequest;
use App\Models\Filme;
use App\Models\Imagem;
use App\Traits\Controllers\FormatoMidiaTrait;
use App\Traits\Controllers\ImagemTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class FilmesController extends Controller
{

    protected $imagePrefix = 'filme_';
    protected $imageFolder = 'upload/filmes/';
    protected $deletedImageFolder = 'upload/filmes/deleted/';

    use ImagemTrait;

    use FormatoMidiaTrait;

    public function __construct()
    {
        $this->middleware('accessFilmes');
    }

    public function index()
    {
        $filmes = Filme::ordenarFilmes()->get();

        $numero_filmes = count($filmes->toArray());

        return view('sistema.filmes.index', compact('filmes', 'numero_filmes'));
    }

    public function create()
    {
        return view('sistema.filmes.criar');
    }

    public function store(FilmeRequest $request)
    {
        $filme = new Filme(Filme::$defaults);

        $filme->user_id = Auth::user()->id;

        $filme->titulo = $request->titulo;
        $filme->numero = $request->numero;

        $filme->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $filme->save();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $this->uploadImage($filme->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $this->syncFormatosMidia($filme, $request);

        flash()->success('Sucesso', 'Criado Filme com Sucesso!!');

        return redirect()->route('sistema::midia::filmes::index');
    }

    public function edit(Filme $filme)
    {
        return view('sistema.filmes.editar', compact('filme'));
    }

    public function update(Filme $filme, FilmeRequest $request)
    {
        $filme->titulo = $request->titulo;
        $filme->numero = $request->numero;

        $filme->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($filme->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($filme->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $filme->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($filme->getImagem(), $imgur);
        }

        $filme->save();

        $this->syncFormatosMidia($filme, $request);

        flash()->success('Sucesso', 'Alterado Filme com Sucesso!!');

        return redirect()->route('sistema::midia::filmes::index');
    }

    public function destroy(Filme $filme)
    {
        if($filme != null)
        {
            $imagem = $filme->getImagem();

            $this->destroyFormatoMidia($filme);

            $filme->delete();
            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);
        }

        flash()->success('Sucesso', 'Removido Filme com Sucesso!!');

        return redirect()->route('sistema::midia::filmes::index');
    }
}
