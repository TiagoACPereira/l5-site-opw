<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\OvaRequest;
use App\Models\Imagem;
use App\Models\Ova;
use App\Traits\Controllers\FormatoMidiaTrait;
use App\Traits\Controllers\ImagemTrait;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class OvasController extends Controller
{

    protected $imagePrefix = 'ova_';
    protected $imageFolder = 'upload/ovas/';
    protected $deletedImageFolder = 'upload/ovas/deleted/';

    use ImagemTrait;

    use FormatoMidiaTrait;

    public function __construct()
    {
        $this->middleware('accessOvas');
    }

    public function index()
    {
        $ovas = Ova::ordenarOvas()->get();

        $numero_ovas = count($ovas->toArray());

        return view('sistema.ovas.index', compact('ovas', 'numero_ovas'));
    }

    public function create()
    {
        return view('sistema.ovas.criar');
    }

    public function store(OvaRequest $request)
    {
        $ova = new Ova(Ova::$defaults);

        $ova->user_id = Auth::user()->id;

        $ova->titulo = $request->titulo;
        $ova->numero = $request->numero;

        $ova->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $ova->save();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $this->uploadImage($ova->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $this->syncFormatosMidia($ova, $request);

        flash()->success('Sucesso', 'Criado Ova com Sucesso!!');

        return redirect()->route('sistema::midia::ovas::index');
    }

    public function edit(Ova $ova)
    {
        return view('sistema.ovas.editar', compact('ova'));
    }

    public function update(Ova $ova, OvaRequest $request)
    {
        $ova->titulo = $request->titulo;
        $ova->numero = $request->numero;

        $ova->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($ova->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($ova->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $ova->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($ova->getImagem(), $imgur);
        }

        $ova->save();

        $this->syncFormatosMidia($ova, $request);

        flash()->success('Sucesso', 'Alterado Ova com Sucesso!!');

        return redirect()->route('sistema::midia::ovas::index');
    }

    public function destroy(Ova $ova)
    {
        if($ova != null)
        {
            $imagem = $ova->getImagem();

            $this->destroyFormatoMidia($ova);

            $ova->delete();
            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);
        }

        flash()->success('Sucesso', 'Removido Ova com Sucesso!!');

        return redirect()->route('sistema::midia::ovas::index');
    }
}
