<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\CapitulofanficRequest;
use App\Models\Capitulofanfic;
use App\Models\Conteudo;
use App\Models\Fanfic;
use App\Traits\Controllers\ImagemTrait as ImageUploadTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CapitulosfanficController extends Controller
{
    protected $imagePrefix = 'fanfic_cap_';
    protected $imageFolder = 'upload/fanfics/capitulos/';
    protected $deletedImageFolder = 'upload/fanfics/capitulos/deleted/';

    use ImageUploadTrait;

    public function __construct()
    {
        $this->middleware('accessCapitulosfanfics');
    }

    public function create(Fanfic $fanfic)
    {
        return view('sistema.capitulosfanfics.criar', compact("fanfic"));
    }

    public function store(Fanfic $fanfic, CapitulofanficRequest $request)
    {
        $dados = $request->all();

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $dados['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        //utilizador é adicionado atravez de um campo escondido no form
        $capituloFanfic = $fanfic->Capitulos()->create($dados);

        $conteudo = $capituloFanfic->RelacaoConteudos()->create(Conteudo::$defaults);

        $this->saveImagens($conteudo, $dados['strConteudo'], $dados['imgur'], $forceHost);

        flash()->success('Sucesso', 'Capítulo de \''.$fanfic->titulo.'\' criada com Sucesso!!');

        return redirect()->route('sistema::fanfics::index');
    }

    public function edit(Fanfic $fanfic, Capitulofanfic $capitulofanfic)
    {
        $capitulofanfic->strConteudo = $capitulofanfic->getStrConteudo();

        return view('sistema.capitulosfanfics.editar', compact('capitulofanfic', 'fanfic'));
    }

    public function update(Capitulofanfic $capitulofanfic, CapitulofanficRequest $request)
    {
        $newCapitulo = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newCapitulo['user_id']);

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $newCapitulo['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $capitulofanfic->update($newCapitulo);

        $conteudo = $capitulofanfic->getConteudo();

        $conteudo->update($newCapitulo);

        $imagens = $conteudo->getImagens();

        //procura por imagens removidas e apaga-as
        foreach ($imagens as $imagem) {
            if ($imagem != null && !str_contains($conteudo->strConteudo, $imagem->nome)) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        $this->saveImagens($capitulofanfic->getConteudo(), $newCapitulo['strConteudo'], $newCapitulo['imgur'], $forceHost);

        flash()->success('Sucesso', 'Capítulo de \''.$capitulofanfic->Fanfic()->first()->titulo.'\' alterada com Sucesso!!');

        return redirect()->route('sistema::fanfics::index');
    }

    public function destroy(Capitulofanfic $capitulofanfic)
    {
        $titulo = $capitulofanfic->fanfic->titulo;
        $numero = $capitulofanfic->numero;

        $conteudo = $capitulofanfic->getConteudo();

        $imagens = $conteudo->getImagens();

        foreach ($imagens as $imagem) {
            if ($imagem != null) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        if ($conteudo != null) {
            $conteudo->delete();
        }

        if ($capitulofanfic != null) {
            $capitulofanfic->delete();
        }

        flash()->success('Sucesso', 'Capítulo '. $numero .' de \''.$titulo.'\' Removido com Sucesso!!');

        return redirect()->route('sistema::fanfics::index');
    }
}
