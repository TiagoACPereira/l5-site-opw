<?php

namespace App\Http\Controllers\Sistema;

use App\Models\Formato;
use App\Models\FansubTag;
use App\Models\InformacoesUtilizador;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoadFormsController extends Controller
{

    public function __construct()
    {
        $this->middleware('addPlainTextToHeader');
    }

    public function create_mirror($inputname, $placeholder)
    {
        return view('sistema.partials.links-input-form', compact('inputname', 'placeholder'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function create_formMidia($id)
    {
        $formatoMidia = null;
        return view('sistema.formatosmidia.partials.form', compact('id', 'formatoMidia'));
    }

    public function create_conteudo($id)
    {
        $conteudo = null;
        return view('sistema.noticiascomponentes.partials.conteudo-form', compact('id', 'conteudo'));
    }
}
