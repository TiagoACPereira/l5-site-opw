<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\EpisodioRequest;
use App\Models\Episodio;
use App\Models\Imagem;
use App\Models\Saga;
use App\Traits\Controllers\FormatoMidiaTrait;
use App\Traits\Controllers\ImagemTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EpisodiosController extends Controller
{

    protected $imagePrefix = 'epi_';
    protected $imageFolder = 'upload/episodios/';
    protected $deletedImageFolder = 'upload/episodios/deleted/';

    use ImagemTrait;

    use FormatoMidiaTrait;

    public function __construct()
    {
        $this->middleware('accessEpisodios');
    }

    public function create(Saga $saga)
    {
        return view('sistema.episodios.criar', compact('saga'));
    }

    public function store(Saga $saga, EpisodioRequest $request)
    {

        $episodio = new Episodio(Episodio::$defaults);

        $episodio->user_id = Auth::user()->id;
        $episodio->saga_id = $saga->id;

        $episodio->titulo = $request->titulo;
        $episodio->numero = $request->numero;
        $episodio->is_Duplo = (Input::get('isDuplo')) ? 1 : 0;
        $episodio->filler_tag = (Input::get('isFiller')) ? 1 : 0;

        $episodio->str_numero = (Input::get('isDuplo'))? $episodio->numero . '-' . ($episodio->numero + 1) : $episodio->numero;
        $episodio->str_numero = (Input::get('isFiller'))? $episodio->str_numero . ' [FILLER]' : $episodio->str_numero;

        $episodio->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $episodio->save();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $this->uploadImage($episodio->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $this->syncFormatosMidia($episodio, $request);

        flash()->success('Sucesso', 'Criado Episódio com Sucesso!!');

        return redirect()->route('sistema::midia::sagas::index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Saga $saga, Episodio $episodio)
    {
        //caso tente editar um episodio que não seja daquela saga
        if($episodio->saga != $saga)
            return redirect()->route('sistema::midia::sagas::episodios::editar', [$episodio->saga->slug, $episodio->slug]);

        return view('sistema.episodios.editar', compact('episodio', 'saga'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Episodio $episodio, EpisodioRequest $request)
    {
        $episodio->titulo = $request->titulo;
        $episodio->numero = $request->numero;
        $episodio->is_Duplo = (Input::get('isDuplo')) ? 1 : 0;
        $episodio->filler_tag = (Input::get('isFiller')) ? 1 : 0;

        $episodio->str_numero = (Input::get('isDuplo'))? $episodio->numero . '-' . ($episodio->numero + 1) : $episodio->numero;
        $episodio->str_numero = (Input::get('isFiller'))? $episodio->str_numero . ' [FILLER]' : $episodio->str_numero;

        $episodio->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($episodio->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($episodio->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $episodio->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($episodio->getImagem(), $imgur);
        }

        $episodio->save();

        $this->syncFormatosMidia($episodio, $request);

        flash()->success('Sucesso', 'Alterado Episódio com Sucesso!!');

        return redirect()->route('sistema::midia::sagas::index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Episodio $episodio)
    {
        if($episodio != null)
        {
            $imagem = $episodio->getImagem();

            $this->destroyFormatoMidia($episodio);

            $episodio->delete();
            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);
        }

        flash()->success('Sucesso', 'Removido Episódio com Sucesso!!');

        return redirect()->route('sistema::midia::sagas::index');
    }

}
