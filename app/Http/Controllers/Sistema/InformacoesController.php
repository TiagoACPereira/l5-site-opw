<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\InfoRequest;
use App\Models\Conteudo;
use App\Models\Imagem;
use App\Models\Info;
use App\Traits\Controllers\ImagemTrait as ImageUploadTrait;
use DOMDocument;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class InformacoesController extends Controller
{
    protected $imagePrefix = 'info_';
    protected $imageFolder = 'upload/infos/';
    protected $deletedImageFolder = 'upload/infos/deleted/';

    use ImageUploadTrait;

    public function __construct()
    {
        $this->middleware('accessInformacoes');
    }

    public function index()
    {
        if (Auth::user()->isAdmin() || Auth::user()->isRevisor()) {
            $infos = Info::latest('publish_at')->get();
        } else {
            $infos = Auth::user()->Infos()->latest('publish_at')->get();
        }

        $numero_infos = count($infos->toArray());

        return view('sistema.informacoes.index', compact('infos', 'numero_infos'));
    }

    public function create()
    {
        return view('sistema.informacoes.criar');
    }

    public function store(InfoRequest $request)
    {
        $dados = $request->all();

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $dados['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $info = Auth::user()->Infos()->create($dados);

        $conteudo = $info->RelacaoConteudos()->create(Conteudo::$defaults);

        $this->saveImagens($conteudo, $dados['strConteudo'], $dados['imgur'], $forceHost);

        flash()->success('Sucesso', 'Informação criada com Sucesso!!');

        return redirect()->route('sistema::informacoes::index');
    }

    public function edit(Info $info)
    {
        $info->strConteudo = $info->getStrConteudo();

        return view('sistema.informacoes.editar', compact('info'));
    }

    public function update(Info $info, InfoRequest $request)
    {
        $newInfo = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newInfo['user_id']);

        $newInfo['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $newInfo['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $info->update($newInfo);

        $conteudo = $info->getConteudo();

        $conteudo->update($newInfo);

        $imagens = $conteudo->getImagens();

        //procura por imagens removidas e apaga-as
        foreach ($imagens as $imagem) {
            if ($imagem != null && !str_contains($conteudo->strConteudo, $imagem->nome)) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        $this->saveImagens($info->getConteudo(), $newInfo['strConteudo'], $newInfo['imgur'], $forceHost);

        flash()->success('Sucesso', 'Informação alterada com Sucesso!!');

        return redirect()->route('sistema::informacoes::index');
    }

    public function destroy(Info $info)
    {
        $conteudo = $info->getConteudo();

        $imagens = $conteudo->getImagens();

        foreach ($imagens as $imagem) {
            if ($imagem != null) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        if ($conteudo != null) {
            $conteudo->delete();
        }

        if ($info != null) {
            $info->delete();
        }

        flash()->success('Sucesso', 'Informação removida com Sucesso!!');

        return redirect()->route('sistema::informacoes::index');
    }
}
