<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\SlideRequest;
use App\Models\Imagem;
use App\Models\Slide;
use App\Traits\Controllers\ImagemTrait;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class SliderController extends Controller
{

    protected $imagePrefix = 'slider_';
    protected $imageFolder = 'upload/slider/';
    protected $deletedImageFolder = 'upload/slider/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessSlide');
    }

    public function index()
    {
        $slides = Slide::latest('publish_at')->get();

        $numero_slides = count($slides->toArray());

        return view('sistema.slides.index', compact('slides', 'numero_slides'));
    }

    public function create()
    {
        return view('sistema.slides.criar');
    }

    public function store(SlideRequest $request)
    {
        $dados = $request->all();

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $slide = Auth::user()->Slides()->create($dados);

        $this->uploadImage($slide->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $slide->save();

        flash()->success('Sucesso', 'Criado Slide com Sucesso!!');

        return redirect()->route('sistema::slider::index');
    }

    public function edit(Slide $slide)
    {
        return view('sistema.slides.editar', compact('slide'));
    }

    public function update(Slide $slide, SlideRequest $request)
    {
        $newSlide = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newSlide['user_id']);

        $newSlide['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($slide->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($slide->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $slide->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($slide->getImagem(), $imgur);
        }

        $slide->update($newSlide);

        flash()->success('Sucesso', 'Alterado Slide com Sucesso!!');

        return redirect()->route('sistema::slider::index');
    }

    public function destroy(Slide $slide)
    {
        if ($slide != null)
        {
            $imagem = $slide->getImagem();

            $slide->delete();

            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);
        }

        flash()->success('Sucesso', 'Eliminado Slide com Sucesso!!');
        return redirect()->route('sistema::slider::index');
    }
}
