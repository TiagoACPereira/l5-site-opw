<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\OstRequest;
use App\Models\Imagem;
use App\Models\Ost;
use App\Models\Tag;
use App\Traits\Controllers\ImagemTrait;
use App\Traits\Controllers\MirrorsTrait;
use App\Traits\Controllers\TagTrait;
use App\Traits\Controllers\UploadersTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class OstsController extends Controller
{

    protected $imagePrefix = 'ost_';
    protected $imageFolder = 'upload/osts/';
    protected $deletedImageFolder = 'upload/osts/deleted/';

    use ImagemTrait, TagTrait, MirrorsTrait, UploadersTrait;

    public function __construct()
    {
        $this->middleware('accessOsts');
    }

    public function index()
    {
        $osts = Ost::all();

        $numero_osts = count($osts->toArray());

        return view('sistema.osts.index', compact('osts', 'numero_osts'));
    }

    public function create()
    {
        return view('sistema.osts.criar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(OstRequest $request)
    {
        $dados = $request->all();

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $ost = Auth::user()->Osts()->create($dados);

        $this->uploadImage($ost->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        //Tamanhos
        $this->syncTag($ost, $dados, Tag::$TIPO_TAMANHO);

        //Formato
        $this->syncTag($ost, $dados, Tag::$TIPO_FORMATO);

        //Idioma
        $this->syncTag($ost, $dados, Tag::$TIPO_IDIOMA);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($ost, $mirrors);
        else
            foreach ($ost->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($ost, $uploaders);

        $ost->save();

        flash()->success('Sucesso', 'Criado Ost com Sucesso!!');

        return redirect()->route('sistema::midia::osts::index');
    }

    public function edit(Ost $ost)
    {
        return view('sistema.osts.editar', compact('ost'));
    }

    public function update(Ost $ost, OstRequest $request)
    {
        $dados = $request->all();

        unset($dados['user_id']);

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($ost->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($ost->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $ost->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($ost->getImagem(), $imgur);
        }

        $ost->update($dados);

        //Tamanhos
        $this->syncTag($ost, $dados, Tag::$TIPO_TAMANHO);

        //Formato
        $this->syncTag($ost, $dados, Tag::$TIPO_FORMATO);

        //Idioma
        $this->syncTag($ost, $dados, Tag::$TIPO_IDIOMA);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($ost, $mirrors);
        else
            foreach ($ost->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($ost, $uploaders);

        $ost->save();

        flash()->success('Sucesso', 'Alterado Ost com Sucesso!!');

        return redirect()->route('sistema::midia::osts::index');
    }

    public function destroy(Ost $ost)
    {
        if($ost != null)
        {
            $imagem = $ost->getImagem();

            foreach ($ost->mirrors as $mirror)
                if($mirror != null)
                    $mirror->delete();

            $ost->delete();

            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);

            flash()->success('Sucesso', 'Removido Ost com Sucesso!!');
        }

        return redirect()->route('sistema::midia::osts::index');
    }
}
