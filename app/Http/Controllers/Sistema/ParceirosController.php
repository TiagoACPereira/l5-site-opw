<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\ParceiroRequest;
use App\Models\Imagem;
use App\Models\Parceiro;
use App\Traits\Controllers\ImagemTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ParceirosController extends Controller
{

    protected $imagePrefix = 'parceiro_';
    protected $imageFolder = 'upload/parceiros/';
    protected $deletedImageFolder = 'upload/parceiros/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessParceiros');
    }

    public function index()
    {
        $parceiros = Parceiro::latest()->get();

        $numero_parceiros = count($parceiros->toArray());

        return view('sistema.parceiros.index', compact('parceiros', 'numero_parceiros'));
    }

    public function create()
    {
        return view('sistema.parceiros.criar');
    }

    public function store(ParceiroRequest $request)
    {
        $dados = $request->all();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $parceiros = Auth::user()->Parceiros()->create($dados);

        $this->uploadImage($parceiros->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $parceiros->save();

        flash()->success('Sucesso', 'Criado Parceiro com Sucesso!!');

        return redirect()->route('sistema::parceiros::index');
    }

    public function edit(Parceiro $parceiro)
    {
        return view('sistema.parceiros.editar', compact('parceiro'));
    }

    public function update(Parceiro $parceiro, ParceiroRequest $request)
    {
        $newParceiro = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newParceiro['user_id']);

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload por ficheiro
            $this->uploadImage($parceiro->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($parceiro->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $parceiro->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($parceiro->getImagem(), $imgur);
        }

        $parceiro->update($newParceiro);

        flash()->success('Sucesso', 'Editado Parceiro com Sucesso!!');

        return redirect()->route('sistema::parceiros::index');
    }

    public function destroy(Parceiro $parceiro)
    {
        $imagem = $parceiro->getImagem();

        if ($parceiro != null) {
            $parceiro->delete();
        }

        if ($imagem != null) {
            $imagem->delete();
        }

        $this->deleteUploadedImagem($imagem->nome);

        flash()->success('Sucesso', 'Eliminado Parceiro com Sucesso!!');
        return redirect()->route('sistema::parceiros::index');
    }
}
