<?php

namespace App\Http\Controllers\Sistema;

use App\Models\Utilizador;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $mes_extenso = [
            'Jan' => 'Janeiro',
            'Feb' => 'Fevereiro',
            'Mar' => 'Marco',
            'Apr' => 'Abril',
            'May' => 'Maio',
            'Jun' => 'Junho',
            'Jul' => 'Julho',
            'Aug' => 'Agosto',
            'Nov' => 'Novembro',
            'Oct' => 'Outubro',
            'Sep' => 'Setembro',
            'Dec' => 'Dezembro'
        ];
        $strMesAtual = $mes_extenso[date('M')];

        $usersAniversarios = [];

        foreach (Utilizador::all()->sortBy('nick') as $usuario) {
            $data = $usuario->dataNascimento;
            $partesData = explode("-", $data);
            if ((sizeof($partesData) == 3) && $partesData[1] == date('m')) {
                $usuario['diaAniversario'] = $partesData[2];
                $usersAniversarios[] = $usuario;
            }
        }
        $user = Auth::user();

        $total = 10;
        return view('sistema.index', compact('user', 'strMesAtual', 'usersAniversarios', 'total'));
    }
}
