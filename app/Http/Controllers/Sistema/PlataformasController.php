<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\PlataformaRequest;
use App\Models\Imagem;
use App\Models\Plataforma;
use App\Traits\Controllers\ImagemTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PlataformasController extends Controller
{

    protected $imagePrefix = 'plat_';
    protected $imageFolder = 'upload/plataformas/';
    protected $deletedImageFolder = 'upload/plataformas/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessPlataformas');
    }

    public function index()
    {
        $plataformas[Plataforma::$TIPO_JOGO] = Plataforma::latest()->jogos()->get();
        $plataformas[Plataforma::$TIPO_ROM] = Plataforma::latest()->roms()->get();

        $numero_plataformas = Plataforma::count();

        $plataformasTipos = Plataforma::$TIPO_JOGO . '/' . Plataforma::$TIPO_ROM;

        return view('sistema.plataformas.index', compact('plataformas', 'numero_plataformas', 'plataformasTipos'));
    }

    public function create($tipo_plataforma)
    {
        return view('sistema.plataformas.partials.criar', compact('tipo_plataforma'));
    }

    public function store(PlataformaRequest $request)
    {
        $dados = $request->all();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $plataforma = Auth::user()->Plataformas()->create($dados);

        $this->uploadImage($plataforma->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $plataforma->save();

        flash()->success('Sucesso', 'Criado Plataforma com Sucesso!!');

        return redirect()->route('sistema::midia::plataformas::index');
    }

    public function edit(Plataforma $plataforma)
    {
        return view('sistema.plataformas.partials.editar', compact('plataforma'));
    }

    public function update(Plataforma $plataforma, PlataformaRequest $request)
    {
        $newPlataforma = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newPlataforma['user_id']);

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($plataforma->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($plataforma->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $plataforma->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($plataforma->getImagem(), $imgur);
        }

        $plataforma->update($newPlataforma);

        flash()->success('Sucesso', 'Alterado Plataforma com Sucesso!!');

        return redirect()->route('sistema::midia::plataformas::index');
    }

    public function destroy(Plataforma $plataforma)
    {
        $imagem = $plataforma->getImagem();

        if ($plataforma != null) {
            $plataforma->delete();
        }

        if ($imagem != null) {
            $imagem->delete();
        }

        $this->deleteUploadedImagem($imagem->nome);

        flash()->success('Sucesso', 'Eliminado Plataforma com Sucesso!!');
        return redirect()->route('sistema::midia::plataformas::index');
    }
}
