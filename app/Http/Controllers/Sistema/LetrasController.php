<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LetraRequest;
use App\Models\Conteudo;
use App\Models\Letra;
use App\Traits\Controllers\ImagemTrait as ImageUploadTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LetrasController extends Controller
{

    protected $imagePrefix = 'letra_';
    protected $imageFolder = 'upload/letras/';
    protected $deletedImageFolder = 'upload/letras/deleted/';

    use ImageUploadTrait;

    public function __construct()
    {
        $this->middleware('accessLetras');
    }

    public function index()
    {
        if (Auth::user()->isAdmin() || Auth::user()->isRevisor()) {
            $letras = Letra::latest('publish_at')->get();
        } else {
            $letras = Auth::user()->Letras()->latest('publish_at')->get();
        }

        $numero_letras = count($letras->toArray());

        return view('sistema.letras.index', compact('letras', 'numero_letras'));
    }

    public function create()
    {
        return view('sistema.letras.criar');
    }

    public function store(LetraRequest $request)
    {
        $dados = $request->all();

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $dados['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $letras = Auth::user()->Letras()->create($dados);

        $conteudo = $letras->RelacaoConteudos()->create(Conteudo::$defaults);

        $this->saveImagens($conteudo, $dados['strConteudo'], $dados['imgur'], $forceHost);

        flash()->success('Sucesso', 'Criado Letra com Sucesso!!');

        return redirect()->route('sistema::letras::index');
    }

    public function edit(Letra $letra)
    {
        $letra->strConteudo = $letra->getStrConteudo();

        return view('sistema.letras.editar', compact('letra'));
    }

    public function update(Letra $letra, LetraRequest $request)
    {
        $newLetra = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newLetra['user_id']);

        $newLetra['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $newLetra['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $letra->update($newLetra);

        $conteudo = $letra->getConteudo();

        $conteudo->update($newLetra);

        $imagens = $conteudo->getImagens();

        //procura por imagens removidas e apaga-as
        foreach ($imagens as $imagem) {
            if ($imagem != null && !str_contains($conteudo->strConteudo, $imagem->nome)) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        $this->saveImagens($letra->getConteudo(), $newLetra['strConteudo'], $newLetra['imgur'], $forceHost);

        flash()->success('Sucesso', 'Letra alterada com Sucesso!!');

        return redirect()->route('sistema::letras::index');
    }

    public function destroy(Letra $letra)
    {
        $conteudo = $letra->getConteudo();

        $imagens = $conteudo->getImagens();

        foreach ($imagens as $imagem) {
            if ($imagem != null) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        if ($conteudo != null) {
            $conteudo->delete();
        }

        if ($letra != null) {
            $letra->delete();
        }

        flash()->success('Sucesso', 'Letra Removida com Sucesso!!');

        return redirect()->route('sistema::letras::index');
    }

}
