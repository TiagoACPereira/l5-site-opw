<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\TeoriaRequest;
use App\Models\Conteudo;
use App\Models\Teoria;
use Illuminate\Http\Request;
use App\Traits\Controllers\ImagemTrait as ImageUploadTrait;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class TeoriasController extends Controller
{
    protected $imagePrefix = 'teoria_';
    protected $imageFolder = 'upload/teorias/';
    protected $deletedImageFolder = 'upload/teorias/deleted/';

    use ImageUploadTrait;

    public function __construct()
    {
        $this->middleware('accessTeorias');
    }

    public function index()
    {
        if (Auth::user()->isAdmin() || Auth::user()->isRevisor()) {
            $teorias = Teoria::latest('publish_at')->get();
        } else {
            $teorias = Auth::user()->Teorias()->latest('publish_at')->get();
        }

        $numero_teorias = count($teorias->toArray());

        return view('sistema.teorias.index', compact('teorias', 'numero_teorias'));
    }

    public function create()
    {
        return view('sistema.teorias.criar');
    }

    public function store(TeoriaRequest $request)
    {
        $dados = $request->all();

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $dados['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $teorias = Auth::user()->Teorias()->create($dados);

        $conteudo = $teorias->RelacaoConteudos()->create(Conteudo::$defaults);

        $this->saveImagens($conteudo, $dados['strConteudo'], $dados['imgur'], $forceHost);

        flash()->success('Sucesso', 'Criado Teoria com Sucesso!!');

        return redirect()->route('sistema::teorias::index');
    }

    public function edit(Teoria $teoria)
    {
        $teoria->strConteudo = $teoria->getStrConteudo();

        return view('sistema.teorias.editar', compact('teoria'));
    }

    public function update(Teoria $teoria, TeoriaRequest $request)
    {
        $newTeoria = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newTeoria['user_id']);

        $newTeoria['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $newTeoria['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $teoria->update($newTeoria);

        $conteudo = $teoria->getConteudo();

        $conteudo->update($newTeoria);

        $imagens = $conteudo->getImagens();

        //procura por imagens removidas e apaga-as
        foreach ($imagens as $imagem) {
            if ($imagem != null && !str_contains($conteudo->strConteudo, $imagem->nome)) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        $this->saveImagens($teoria->getConteudo(), $newTeoria['strConteudo'], $newTeoria['imgur'], $forceHost);

        flash()->success('Sucesso', 'Teoria alterada com Sucesso!!');

        return redirect()->route('sistema::teorias::index');
    }

    public function destroy(Teoria $teoria)
    {
        $conteudo = $teoria->getConteudo();

        $imagens = $conteudo->getImagens();

        foreach ($imagens as $imagem) {
            if ($imagem != null) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        if ($conteudo != null) {
            $conteudo->delete();
        }

        if ($teoria != null) {
            $teoria->delete();
        }

        flash()->success('Sucesso', 'Teoria Removida com Sucesso!!');

        return redirect()->route('sistema::teorias::index');
    }

}
