<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\CapituloRequest;
use App\Models\Capitulo;
use App\Models\Imagem;
use App\Models\Tag;
use App\Models\Volume;
use App\Traits\Controllers\ImagemTrait;
use App\Traits\Controllers\MirrorsTrait;
use App\Traits\Controllers\TagTrait;
use App\Traits\Controllers\UploadersTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CapitulosController extends Controller
{
    protected $imagePrefix = 'epi_';
    protected $imageFolder = 'upload/capitulos/';
    protected $deletedImageFolder = 'upload/capitulos/deleted/';

    use ImagemTrait, MirrorsTrait, UploadersTrait, TagTrait;

    public function __construct()
    {
        $this->middleware('accessCapitulos');
    }

    public function create(Volume $volume)
    {
        return view('sistema.capitulos.criar', compact('volume'));
    }

    public function store(Volume $volume, CapituloRequest $request)
    {

        $capitulo = new Capitulo(Capitulo::$defaults);

        $capitulo->user_id = Auth::user()->id;
        $capitulo->volume_id = $volume->id;

        $capitulo->titulo = $request->titulo;
        $capitulo->numero = $request->numero;

        $capitulo->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $capitulo->save();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $this->uploadImage($capitulo->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        //Scanlator
        $this->syncTag($capitulo, $request->all(), Tag::$TIPO_SCANLATOR);

        //mirrors
        if($request->has('mirrors'))
            $this->syncMirrors($capitulo, $request->mirrors);
        else
            foreach ($capitulo->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if($request->has('uploaders'))
            $this->syncUploaders($capitulo, $request->uploaders);

        flash()->success('Sucesso', 'Criado Capítulo com Sucesso!!');

        return redirect()->route('sistema::midia::volumes::index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Volume $volume, Capitulo $capitulo)
    {
        //caso tente editar um capitulo que não seja daquela volume
        if($capitulo->volume != $volume)
            return redirect()->route('sistema::midia::volumes::capitulos::editar', [$capitulo->volume->slug, $capitulo->slug]);

        return view('sistema.capitulos.editar', compact('capitulo', 'volume'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Capitulo $capitulo, CapituloRequest $request)
    {
        $capitulo->titulo = $request->titulo;
        $capitulo->numero = $request->numero;

        $capitulo->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($capitulo->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($capitulo->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $capitulo->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($capitulo->getImagem(), $imgur);
        }

        $capitulo->save();

        //Scanlator
        $this->syncTag($capitulo, $request->all(), Tag::$TIPO_SCANLATOR);

        //mirrors
        if($request->has('mirrors'))
            $this->syncMirrors($capitulo, $request->mirrors);
        else
            foreach ($capitulo->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if($request->has('uploaders'))
            $this->syncUploaders($capitulo, $request->uploaders);

        flash()->success('Sucesso', 'Alterado Capítulo com Sucesso!!');

        return redirect()->route('sistema::midia::volumes::index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Capitulo $capitulo)
    {
        if($capitulo != null)
        {
            $imagem = $capitulo->getImagem();

            foreach ($capitulo->mirrors as $mirror)
                if($mirror != null)
                    $mirror->delete();

            $capitulo->delete();
            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);
        }

        flash()->success('Sucesso', 'Removido Capítulo com Sucesso!!');

        return redirect()->route('sistema::midia::volumes::index');
    }
}
