<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\EspecialRequest;
use App\Models\Especial;
use App\Models\Imagem;
use App\Traits\Controllers\FormatoMidiaTrait;
use App\Traits\Controllers\ImagemTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EspeciaisController extends Controller
{

    protected $imagePrefix = 'especial_';
    protected $imageFolder = 'upload/especiais/';
    protected $deletedImageFolder = 'upload/especiais/deleted/';

    use ImagemTrait;

    use FormatoMidiaTrait;

    public function __construct()
    {
        $this->middleware('accessEspeciais');
    }

    public function index()
    {
        $especiais = Especial::ordenarEspeciais()->get();

        $numero_especiais = count($especiais->toArray());

        return view('sistema.especiais.index', compact('especiais', 'numero_especiais'));
    }

    public function create()
    {
        return view('sistema.especiais.criar');
    }

    public function store(EspecialRequest $request)
    {
        $especial = new Especial(Especial::$defaults);

        $especial->user_id = Auth::user()->id;

        $especial->titulo = $request->titulo;
        $especial->sub_titulo = $request->sub_titulo;

        $especial->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $especial->save();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $this->uploadImage($especial->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $this->syncFormatosMidia($especial, $request);

        flash()->success('Sucesso', 'Criado Especial com Sucesso!!');

        return redirect()->route('sistema::midia::especiais::index');
    }

    public function edit(Especial $especial)
    {
        return view('sistema.especiais.editar', compact('especial'));
    }

    public function update(Especial $especial, EspecialRequest $request)
    {
        $especial->titulo = $request->titulo;
        $especial->sub_titulo = $request->sub_titulo;

        $especial->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($especial->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($especial->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $especial->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($especial->getImagem(), $imgur);
        }

        $especial->save();

        $this->syncFormatosMidia($especial, $request);

        flash()->success('Sucesso', 'Alterado Especial com Sucesso!!');

        return redirect()->route('sistema::midia::especiais::index');
    }

    public function destroy(Especial $especial)
    {
        if($especial != null)
        {
            $imagem = $especial->getImagem();

            $this->destroyFormatoMidia($especial);

            $especial->delete();
            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);
        }

        flash()->success('Sucesso', 'Removido Especial com Sucesso!!');

        return redirect()->route('sistema::midia::especiais::index');
    }
}
