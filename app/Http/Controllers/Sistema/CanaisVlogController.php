<?php

namespace App\Http\Controllers\Sistema;

use Alaouy\Youtube\Facades\Youtube;
use App\Http\Requests\CanalVlogRequest;
use App\Models\CanalVlog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CanaisVlogController extends Controller
{

    public function __construct()
    {
        $this->middleware('accessCanaisVlog');
    }

    public function index()
    {
        $canaisvlog = CanalVlog::latest()->get();

        $numero_canaisvlog = count($canaisvlog->toArray());

        return view('sistema.canaisvlog.index', compact('canaisvlog', 'numero_canaisvlog'));
    }

    public function create()
    {
        return view('sistema.canaisvlog.criar');
    }

    public function store(CanalVlogRequest $request)
    {
        $dados = $request->all();

        if($request->id_channel != ''){
            $dados['channel'] = $request->id_channel;
            $dados['channel_id'] = true;
            $dados['channel_info'] = Youtube::getChannelById($dados['channel']);;
        } else{
            $dados['channel'] = $request->nome_channel;
            $dados['channel_id'] = false;
            $dados['channel_info'] = Youtube::getChannelByName($dados['channel']);;
        }

        if(!$dados['channel_info']){
            flash()->overlay('Erro', 'Id/Nome do Canal de Youtube está Inválido.', 'error');
            return redirect()->back()->withInput();
        }

        $canalvlog = Auth::user()->CanaisVlog()->create($dados);

        flash()->success('Sucesso', 'Canal Vlog adicionado com Sucesso!!');

        return redirect()->route('sistema::canaisvlog::index');
    }

    public function edit(CanalVlog $canalvlog)
    {
        return view('sistema.canaisvlog.editar', compact('canalvlog'));
    }

    public function update(CanalVlog $canalvlog, CanalVlogRequest $request)
    {
        $newCanalVlog = $request->all();

        if($request->id_channel != ''){
            $dados['channel'] = $request->id_channel;
            $dados['channel_id'] = true;
            $dados['channel_info'] = Youtube::getChannelById($dados['channel']);;
        } else{
            $dados['channel'] = $request->nome_channel;
            $dados['channel_id'] = false;
            $dados['channel_info'] = Youtube::getChannelByName($dados['channel']);;
        }

        if(!$dados['channel_info']){
            flash()->overlay('Erro', 'Id/Nome do Canal de Youtube está Inválido.', 'error');
            return redirect()->back()->withInput();
        }

        //se não remover o id vai acabar substituir por quem criou
        unset($newCanalVlog['user_id']);

        $canalvlog->update($newCanalVlog);

        flash()->success('Sucesso', 'Canal Vlog actualizado com Sucesso!!');

        return redirect()->route('sistema::canaisvlog::index');
    }

    public function destroy(CanalVlog $canalvlog)
    {
        if ($canalvlog != null) {
            $canalvlog->delete();
        }

        flash()->success('Sucesso', 'Canal Vlog removido com Sucesso!!');

        return redirect()->route('sistema::canaisvlog::index');
    }
}
