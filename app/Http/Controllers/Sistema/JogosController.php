<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\JogoRequest;
use App\Models\Imagem;
use App\Models\Jogo;
use App\Models\Plataforma;
use App\Models\Tag;
use App\Traits\Controllers\ImagemTrait;
use App\Traits\Controllers\MirrorsTrait;
use App\Traits\Controllers\UploadersTrait;
use App\Traits\Controllers\TagTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class JogosController extends Controller
{

    protected $imagePrefix = 'jogo_';
    protected $imageFolder = 'upload/jogos/';
    protected $deletedImageFolder = 'upload/jogos/deleted/';

    use ImagemTrait, TagTrait, MirrorsTrait, UploadersTrait;

    public function __construct()
    {
        $this->middleware('accessJogos');
    }

    public function create(Plataforma $plataforma)
    {
        return view('sistema.jogos.criar', compact('plataforma'));
    }

    public function store(Plataforma $plataforma, JogoRequest $request)
    {
        $dados = $request->all();

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $jogo = $plataforma->Jogos()->create($dados);

        $this->uploadImage($jogo->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        //Tamanhos
        $this->syncTag($jogo, $dados, Tag::$TIPO_TAMANHO);

        //Formato
        $this->syncTag($jogo, $dados, Tag::$TIPO_FORMATO);

        //Idioma
        $this->syncTag($jogo, $dados, Tag::$TIPO_IDIOMA);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($jogo, $mirrors);
        else
            foreach ($jogo->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($jogo, $uploaders);

        $jogo->save();

        flash()->success('Sucesso', 'Criado ' . $jogo->plataforma->tipo .' com Sucesso!!');

        return redirect()->route('sistema::midia::plataformas::index');
    }


    public function edit(Plataforma $plataforma, Jogo $jogo)
    {
        //caso tente editar um episodio que não seja daquela saga
        if($jogo->plataforma != $plataforma)
            return redirect()->route('sistema::midia::plataformas::jogos::editar', [$jogo->plataforma->slug, $jogo->slug]);

        return view('sistema.jogos.editar', compact('plataforma', 'jogo'));
    }

    public function update(Jogo $jogo, JogoRequest $request)
    {
        $dados = $request->all();
        unset($dados['user_id']);

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($jogo->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($jogo->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $jogo->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($jogo->getImagem(), $imgur);
        }

        $jogo->update($dados);

        //Tamanhos
        $this->syncTag($jogo, $dados, Tag::$TIPO_TAMANHO);

        //Formato
        $this->syncTag($jogo, $dados, Tag::$TIPO_FORMATO);

        //Idioma
        $this->syncTag($jogo, $dados, Tag::$TIPO_IDIOMA);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($jogo, $mirrors);
        else
            foreach ($jogo->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($jogo, $uploaders);

        $jogo->save();

        flash()->success('Sucesso', 'Alterado ' . $jogo->plataforma->tipo .' com Sucesso!!');

        return redirect()->route('sistema::midia::plataformas::index');
    }

    public function destroy(Jogo $jogo)
    {
        if($jogo != null)
        {
            $tipo = $jogo->plataforma->tipo;

            $imagem = $jogo->getImagem();

            foreach ($jogo->mirrors as $mirror)
                if($mirror != null)
                    $mirror->delete();

            $jogo->delete();

            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);

            flash()->success('Sucesso', 'Removido '.$tipo . ' com Sucesso!!');
        }

        return redirect()->route('sistema::midia::plataformas::index');
    }
}
