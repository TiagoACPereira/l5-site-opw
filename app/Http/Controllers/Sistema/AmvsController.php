<?php

namespace App\Http\Controllers\Sistema;

use Alaouy\Youtube\Facades\Youtube;
use App\Http\Requests\AmvRequest;
use App\Models\Amv;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AmvsController extends Controller
{

    public function __construct()
    {
        $this->middleware('accessCanaisVlog');
    }

    public function index()
    {
        $amvs = Amv::latest()->get();

        $numero_amvs = count($amvs->toArray());

        foreach($amvs as $amv){
            if($amv != null && !Youtube::getVideoInfo($amv->video_id))
            {
                $amv->delete();
            }
        }

        return view('sistema.amvs.index', compact('amvs', 'numero_amvs'));
    }

    public function store(AmvRequest $request)
    {
        $dados = $request->all();

        try {
            $dados['video_id'] = Youtube::parseVIdFromURL($request->video_id);
        } catch (Exception $e) {
            $dados['video_id'] = $request->video_id;
        }

        if(Amv::where('video_id', $dados['video_id'])->first() != null){
            flash()->overlay('Erro', 'O video que tentou adicionar já se encontra adicionado.', 'warning');
            return redirect()->route('sistema::amvs::index');
        }

        $dados['video_info'] = Youtube::getVideoInfo($dados['video_id']);

        if(!$dados['video_info']){
            flash()->overlay('Erro', 'Id do AMV do Youtube está Inválido.', 'error');
            return redirect()->back()->withInput();
        }

        $amv = Auth::user()->Amvs()->create($dados);

        flash()->success('Sucesso', 'AMV adicionado com Sucesso!!');

        return redirect()->route('sistema::amvs::index');
    }

    public function destroy(Amv $amv)
    {
        if ($amv != null) {
            $amv->delete();
        }

        flash()->success('Sucesso', 'AMV removido com Sucesso!!');

        return redirect()->route('sistema::amvs::index');
    }
}
