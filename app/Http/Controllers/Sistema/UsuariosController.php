<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\UsuarioRequest;
use App\Models\Imagem;
use App\Models\InformacoesUtilizador;
use App\Models\PermissoesUtilizador;
use App\Models\Utilizador;
use App\Traits\Controllers\ImagemTrait;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UsuariosController extends Controller
{

    protected $imagePrefix = 'avatar';
    protected $imageFolder = 'upload/avatars/';
    protected $deletedImageFolder = 'upload/avatars/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessUsuarios');
    }

    public function index()
    {
        $usuarios = Utilizador::all();

        $numero_usuarios = count($usuarios->toArray());

        return view('sistema.usuarios.index', compact('usuarios', 'numero_usuarios'));
    }

    public function create()
    {
        return view('auth.register');
    }

    public function store(UsuarioRequest $request)
    {
        $dados = $request->all();

        $user = Utilizador::create([
            'nome'     => $dados['nome'],
            'login'    => $dados['login'],
            'email'    => $dados['email'],
            'password' => $dados['password'],
        ]);;

        //cria linha das permissoes
        $user->Permissoes()->create(PermissoesUtilizador::$defaults);

        //cria linha das informacoes
        $user->Informacoes()->create(InformacoesUtilizador::$defaults);

        //cria o avatar em informacoes
        $user->Informacoes->RelacaoImagem()->create(Imagem::$defaults);

        flash()->success('Sucesso', 'Criado Usuário com Sucesso!!');

        if (!Auth::check()) {
            Auth::login($user);

            return redirect()->route('sistema::index');
        } else {
            return redirect()->route('sistema::usuarios::index');
        }

    }

    public function edit(Utilizador $usuario)
    {
        $link_imagem = ($usuario->informacoes->getAvatar()->nome != null) ? route('imagecache',
            ['template' => 'original', 'filename' => $usuario->Informacoes->getAvatar()->nome]) : null;

        return view('sistema.usuarios.editar', compact('usuario', 'link_imagem'));
    }

    public function update(Utilizador $usuario, UsuarioRequest $request)
    {
        $novoUsuario = $request->all();

        //se password vazia, não altera
        if ($novoUsuario['password'] == "") {
            unset($novoUsuario['password']);
            unset($novoUsuario['password_confirmation']);
        }

        //só admins pode alterar o avatar
        if (Auth::user()->isAdmin() && $this->imagemUploadedValida('avatar')) {
            $this->uploadImage($usuario->Informacoes->getAvatar(), 'avatar');
        }

        $usuario->update($novoUsuario);

        //PERMISSOES
        if (Auth::user()->isAdmin()) {
            $usuario->Permissoes->is_Admin = (Input::get('admin')) ? 1 : 0;
            $usuario->Permissoes->is_Revisor = (Input::get('revisor')) ? 1 : 0;
            $usuario->Permissoes->is_Redator = (Input::get('redator')) ? 1 : 0;
            $usuario->Permissoes->is_Editor_Noticias = (Input::get('editorNoticias')) ? 1 : 0;
            $usuario->Permissoes->is_Midia_Uploader = (Input::get('midiaUploader')) ? 1 : 0;
            $usuario->Permissoes->is_Fan_Area = (Input::get('fanArea')) ? 1 : 0;
            $usuario->Permissoes->save();
        }

        flash()->success('Sucesso', 'Actualizado Usuário com Sucesso!!');

        return redirect()->route('sistema::usuarios::index');

    }

    public function desactivar(Utilizador $usuario)
    {
        if($usuario->active){
            $usuario->update(['active' => false]);
            flash()->success('Sucesso', 'Usuário desactivado com Sucesso!!');
        } else {
            flash()->info('Atenção', 'O Usuário já se encontra desactivado!!');
        }

        return redirect()->route('sistema::usuarios::index');
    }

    public function activar(Utilizador $usuario)
    {
        if(!$usuario->active){
            $usuario->update(['active' => true]);
            flash()->success('Sucesso', 'Usuário activado com Sucesso!!');
        } else {
            flash()->info('Atenção', 'O Usuário já se encontra activado!!');
        }

        return redirect()->route('sistema::usuarios::index');
    }

    public function destroy(Utilizador $usuario)
    {
//        $usuario->update(['active' => false]);
//
//        flash()->success('Sucesso', 'Removido Usuário com Sucesso!!');
//
//        return redirect()->route('sistema::usuarios::index');

        $permissoes = $usuario->Permissoes;
        $informacoes = $usuario->Informacoes;
        $avatar = $informacoes->RelacaoImagem()->first();

        if ($usuario != null) {
            $usuario->delete();
        }
        if ($permissoes != null) {
            $permissoes->delete();
        }
        if ($informacoes != null) {
            $informacoes->delete();
        }
        if ($avatar != null) {
            $avatar->delete();
            $this->deleteUploadedImagem($avatar->nome);
        }

        flash()->success('Sucesso', 'Removido Usuário com Sucesso!!');

        return redirect()->route('sistema::usuarios::index');
    }
}
