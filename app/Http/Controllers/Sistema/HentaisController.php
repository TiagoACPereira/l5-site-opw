<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\HentaiRequest;
use App\Models\Hentai;
use App\Models\Imagem;
use App\Models\Tag;
use App\Traits\Controllers\ImagemTrait;
use App\Traits\Controllers\MirrorsTrait;
use App\Traits\Controllers\TagTrait;
use App\Traits\Controllers\UploadersTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HentaisController extends Controller
{

    protected $imagePrefix = 'hentai_';
    protected $imageFolder = 'upload/hentais/';
    protected $deletedImageFolder = 'upload/hentais/deleted/';

    use ImagemTrait, TagTrait, MirrorsTrait, UploadersTrait;

    public function __construct()
    {
        $this->middleware('accessHentais');
    }

    public function index()
    {
        $hentais = Hentai::all();

        $numero_hentais = count($hentais->toArray());

        return view('sistema.hentais.index', compact('hentais', 'numero_hentais'));
    }

    public function create()
    {
        return view('sistema.hentais.criar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(HentaiRequest $request)
    {
        $dados = $request->all();

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $hentai = Auth::user()->Hentais()->create($dados);

        $this->uploadImage($hentai->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        //Tamanhos
        $this->syncTag($hentai, $dados, Tag::$TIPO_TAMANHO);

        //Formato
        $this->syncTag($hentai, $dados, Tag::$TIPO_FORMATO);

        //Idioma
        $this->syncTag($hentai, $dados, Tag::$TIPO_IDIOMA);

        //Scan
        $this->syncTag($hentai, $dados, Tag::$TIPO_SCAN);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($hentai, $mirrors);
        else
            foreach ($hentai->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($hentai, $uploaders);

        $hentai->save();

        flash()->success('Sucesso', 'Criado Hentai com Sucesso!!');

        return redirect()->route('sistema::midia::hentais::index');
    }

    public function edit(Hentai $hentai)
    {
        return view('sistema.hentais.editar', compact('hentai'));
    }

    public function update(Hentai $hentai, HentaiRequest $request)
    {
        $dados = $request->all();

        unset($dados['user_id']);

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($hentai->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($hentai->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $hentai->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($hentai->getImagem(), $imgur);
        }

        $hentai->update($dados);

        //Tamanhos
        $this->syncTag($hentai, $dados, Tag::$TIPO_TAMANHO);

        //Formato
        $this->syncTag($hentai, $dados, Tag::$TIPO_FORMATO);

        //Idioma
        $this->syncTag($hentai, $dados, Tag::$TIPO_IDIOMA);

        //Scan
        $this->syncTag($hentai, $dados, Tag::$TIPO_SCAN);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($hentai, $mirrors);
        else
            foreach ($hentai->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($hentai, $uploaders);

        $hentai->save();

        flash()->success('Sucesso', 'Alterado Hentai com Sucesso!!');

        return redirect()->route('sistema::midia::hentais::index');
    }

    public function destroy(Hentai $hentai)
    {
        if($hentai != null)
        {
            $imagem = $hentai->getImagem();

            foreach ($hentai->mirrors as $mirror)
                if($mirror != null)
                    $mirror->delete();

            $hentai->delete();

            if ($imagem != null)
                $imagem->delete();

            $this->deleteUploadedImagem($imagem->nome);

            flash()->success('Sucesso', 'Removido Hentai com Sucesso!!');
        }

        return redirect()->route('sistema::midia::hentais::index');
    }
}
