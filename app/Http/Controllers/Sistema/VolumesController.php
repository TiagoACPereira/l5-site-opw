<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\VolumeRequest;
use App\Models\Imagem;
use App\Models\Tag;
use App\Models\Volume;
use App\Traits\Controllers\ImagemTrait;
use App\Traits\Controllers\MirrorsTrait;
use App\Traits\Controllers\TagTrait;
use App\Traits\Controllers\UploadersTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class VolumesController extends Controller
{
    use TagTrait, MirrorsTrait, UploadersTrait;

    protected $imagePrefix = 'volume_';
    protected $imageFolder = 'upload/volumes/';
    protected $deletedImageFolder = 'upload/volumes/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessVolumes');
    }

    public function index()
    {
        $volumes = Volume::ordenarVolumes()->get();

        $numero_volumes = count($volumes->toArray());

        return view('sistema.volumes.index', compact('volumes', 'numero_volumes'));
    }

    public function create()
    {
        return view('sistema.volumes.criar');
    }

    public function store(VolumeRequest $request)
    {
        $dados = $request->all();

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $volume = Auth::user()->Volumes()->create($dados);

        $this->uploadImage($volume->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        //Scanlator
        $this->syncTag($volume, $dados, Tag::$TIPO_SCANLATOR);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($volume, $mirrors);
        else
            foreach ($volume->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($volume, $uploaders);

        $volume->save();

        flash()->success('Sucesso', 'Criada Volume com Sucesso!!');

        return redirect()->route('sistema::midia::volumes::index');
    }

    public function show(Volume $volume)
    {
        return view('sistema.volumes.partials.mostrar', compact('volume'));
    }

    public function edit(Volume $volume)
    {
        return view('sistema.volumes.editar', compact('volume'));
    }

    public function update(Volume $volume, VolumeRequest $request)
    {
        $newVolume = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newVolume['user_id']);

        unset($uploaders);
        if(isset($dados['uploaders'])){
            $uploaders = $dados['uploaders'];
            unset($dados['uploaders']);
        }

        unset($mirrors);
        if(isset($dados['mirrors'])){
            $mirrors = $dados['mirrors'];
            unset($dados['mirrors']);
        }

        $newVolume['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($volume->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($volume->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $volume->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($volume->getImagem(), $imgur);
        }

        $volume->update($newVolume);

        //Scanlator
        $this->syncTag($volume, $newVolume, Tag::$TIPO_SCANLATOR);

        //mirrors
        if(isset($mirrors))
            $this->syncMirrors($volume, $mirrors);
        else
            foreach ($volume->mirrors as $mirror)
                $mirror->delete();

        //uploaders
        if(isset($uploaders))
            $this->syncUploaders($volume, $uploaders);

        flash()->success('Sucesso', 'Alterada Volume com Sucesso!!');

        return redirect()->route('sistema::midia::volumes::index');
    }

    public function destroy(Volume $volume)
    {
        if ($volume != null) {
            $imagem = $volume->getImagem();

            foreach ($volume->mirrors as $mirror)
                if($mirror != null)
                    $mirror->delete();

            if ($imagem != null)
                $imagem->delete();

            $volume->delete();

            $this->deleteUploadedImagem($imagem->nome);

            flash()->success('Sucesso', 'Eliminada Volume com Sucesso!!');
        }

        return redirect()->route('sistema::midia::volumes::index');
    }
}
