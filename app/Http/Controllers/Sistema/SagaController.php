<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\SagaRequest;
use App\Models\Imagem;
use App\Models\Saga;
use App\Traits\Controllers\ImagemTrait;
use App\Traits\Controllers\TagTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class SagaController extends Controller
{

    protected $imagePrefix = 'saga_';
    protected $imageFolder = 'upload/saga/';
    protected $deletedImageFolder = 'upload/saga/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessSagas');
    }

    public function index()
    {
        $sagas = Saga::ordenarSagas()->get();

        $numero_sagas = count($sagas->toArray());

        return view('sistema.sagas.index', compact('sagas', 'numero_sagas'));
    }

    public function create()
    {
        return view('sistema.sagas.criar');
    }

    public function store(SagaRequest $request)
    {
        $dados = $request->all();

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $saga = Auth::user()->Sagas()->create($dados);

        $this->uploadImage($saga->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $saga->save();

        flash()->success('Sucesso', 'Criada Saga com Sucesso!!');

        return redirect()->route('sistema::midia::sagas::index');
    }

    public function show(Saga $saga)
    {
        return view('sistema.sagas.partials.mostrar', compact('saga'));
    }

    public function edit(Saga $saga)
    {
        return view('sistema.sagas.editar', compact('saga'));
    }

    public function update(Saga $saga, SagaRequest $request)
    {
        $newSaga = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newSaga['user_id']);

        $newSaga['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($saga->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($saga->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $saga->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($saga->getImagem(), $imgur);
        }

        $saga->update($newSaga);

        flash()->success('Sucesso', 'Alterada Saga com Sucesso!!');

        return redirect()->route('sistema::midia::sagas::index');
    }

    public function destroy(Saga $saga)
    {
        $imagem = $saga->getImagem();

        if ($saga != null) {
            $saga->delete();
        }

        if ($imagem != null) {
            $imagem->delete();
        }

        $this->deleteUploadedImagem($imagem->nome);

        flash()->success('Sucesso', 'Eliminada Saga com Sucesso!!');
        return redirect()->route('sistema::midia::sagas::index');
    }
}
