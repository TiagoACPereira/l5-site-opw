<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\TutorialRequest;
use App\Models\Conteudo;
use App\Models\Imagem;
use App\Models\Info;
use App\Models\Tutorial;
use App\Traits\Controllers\ImagemTrait as ImageUploadTrait;
use DOMDocument;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class TutoriaisController extends Controller
{
    protected $imagePrefix = 'tuto_';
    protected $imageFolder = 'upload/tutoriais/';
    protected $deletedImageFolder = 'upload/tutoriais/deleted/';

    use ImageUploadTrait;

    public function __construct()
    {
        $this->middleware('accessTutoriais');
    }

    public function index()
    {
        if (Auth::user()->isAdmin() || Auth::user()->isRevisor()) {
            $tutoriais = Tutorial::latest('publish_at')->get();
        } else {
            $tutoriais = Auth::user()->Tutoriais()->latest('publish_at')->get();
        }

        $numero_tutoriais = count($tutoriais->toArray());

        return view('sistema.tutoriais.index', compact('tutoriais', 'numero_tutoriais'));
    }

    public function create()
    {
        return view('sistema.tutoriais.criar');
    }

    public function store(TutorialRequest $request)
    {
        $dados = $request->all();

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $dados['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $dados['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $tutorial = Auth::user()->Tutoriais()->create($dados);

        $conteudo = $tutorial->RelacaoConteudos()->create(Conteudo::$defaults);

        $this->saveImagens($conteudo, $dados['strConteudo'], $dados['imgur'], $forceHost);

        flash()->success('Sucesso', 'Criado Tutorial com Sucesso!!');

        return redirect()->route('sistema::tutoriais::index');
    }

    public function edit(Tutorial $tutorial)
    {
        $tutorial->strConteudo = $tutorial->getStrConteudo();

        return view('sistema.tutoriais.editar', compact('tutorial'));
    }

    public function update(Tutorial $tutorial, TutorialRequest $request)
    {
        $newTutorial = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newTutorial['user_id']);

        $newTutorial['publish_at'] = $request->date_publish_at . ' ' . $request->time_publish_at;

        $forceHost = (Input::get('forceHost'))? 1 : 0 ;

        $newTutorial['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;

        $tutorial->update($newTutorial);

        $conteudo = $tutorial->getConteudo();

        $conteudo->update($newTutorial);

        $imagens = $conteudo->getImagens();

        //procura por imagens removidas e apaga-as
        foreach ($imagens as $imagem) {
            if ($imagem != null && !str_contains($conteudo->strConteudo, $imagem->nome)) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        $this->saveImagens($tutorial->getConteudo(), $newTutorial['strConteudo'], $newTutorial['imgur'], $forceHost);

        flash()->success('Sucesso', 'Alterado Tutorial com Sucesso!!');

        return redirect()->route('sistema::tutoriais::index');
    }

    public function destroy(Tutorial $tutorial)
    {
        $conteudo = $tutorial->getConteudo();

        $imagens = $conteudo->getImagens();

        foreach ($imagens as $imagem) {
            if ($imagem != null) {
                $imagem->delete();
                $this->deleteUploadedImagem($imagem->nome);
            }
        }

        if ($conteudo != null) {
            $conteudo->delete();
        }

        if ($tutorial != null) {
            $tutorial->delete();
        }

        flash()->success('Sucesso', 'Removido Tutorial com Sucesso!!');

        return redirect()->route('sistema::tutoriais::index');
    }

}
