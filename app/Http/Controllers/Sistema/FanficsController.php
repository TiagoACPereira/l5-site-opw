<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\FanficRequest;
use App\Models\Fanfic;
use App\Models\Fanfictag;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class FanficsController extends Controller
{

    public function __construct()
    {
        $this->middleware('accessFanfics');
    }

    public function index()
    {
        $fanfics = Fanfic::all();

        $numero_fanfics = count($fanfics->toArray());

        return view('sistema.fanfics.index', compact('fanfics', 'numero_fanfics'));
    }

    public function create()
    {
        return view('sistema.fanfics.criar');
    }

    public function store(FanficRequest $request)
    {
        $dados = $request->all();

        $fanfic = Auth::user()->Fanfics()->create($dados);

        $this->syncTags($fanfic, $request);

        $fanficstatus = [Input::get('status') => true];

        $fanfic->FanficStatus()->create($fanficstatus);

        flash()->success('Sucesso', 'Fanfic criada com Sucesso!!');

        return redirect()->route('sistema::fanfics::index');
    }

    public function edit(Fanfic $fanfic)
    {
        return view('sistema.fanfics.editar', compact('fanfic'));
    }

    public function update(Fanfic $fanfic, FanficRequest $request)
    {
        $newFanfic = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newFanfic['user_id']);

        $fanfic->update($newFanfic);

        $this->syncTags($fanfic, $request);

        flash()->success('Sucesso', 'Fanfic alterada com Sucesso!!');

        return redirect()->route('sistema::fanfics::index');
    }

    public function destroy(Fanfic $fanfic)
    {
        foreach($fanfic->capitulos as $capitulo)
            if($capitulo != null)
                $capitulo->delete();

        $fanficstatus = $fanfic->FanficStatus();

        if ($fanficstatus != null) {
            $fanficstatus->delete();
        }

        if ($fanfic != null) {
            $fanfic->delete();
        }

        flash()->success('Sucesso', 'Fanfic removida com Sucesso!!');

        return redirect()->route('sistema::fanfics::index');
    }

    private function syncTags(Fanfic $fanfic, FanficRequest $request)
    {
        if ( ! $request->has('tags'))
        {
            $fanfic->Fanfictags()->detach();
            return;
        }

        $allTagIds = array();

        foreach ($request->tags as $tagId)
        {
            if (substr($tagId, 0, 5) == 'nova:')
            {
                $newTag = Fanfictag::create(['nome' => substr($tagId, 5)]);
                $allTagIds[] = $newTag->id;
                continue;
            }
            $allTagIds[] = $tagId;
        }

        $fanfic->Fanfictags()->sync($allTagIds);
    }

}
