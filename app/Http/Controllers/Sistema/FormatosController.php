<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\FormatoRequest;
use App\Models\Formato;
use App\Models\Imagem;
use App\Traits\Controllers\ImagemTrait;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class FormatosController extends Controller
{

    protected $imagePrefix = 'formato_';
    protected $imageFolder = 'upload/formato/';
    protected $deletedImageFolder = 'upload/formato/deleted/';

    use ImagemTrait;

    public function __construct()
    {
        $this->middleware('accessFormatos');
    }

    public function index()
    {
        $formatos = Formato::latest()->get();

        $numero_formatos = count($formatos->toArray());

        return view('sistema.formatos.index', compact('formatos', 'numero_formatos'));
    }

    public function create()
    {
        return view('sistema.formatos.partials.criar');
    }

    public function store(FormatoRequest $request)
    {

        $dados = $request->all();

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){
            $img = $request->inputImagemUrl;
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else{
            $img = 'imagem';
        }

        $formato = Auth::user()->Formatos()->create($dados);

        $this->uploadImage($formato->RelacaoImagem()->create(Imagem::$defaults), $img, $imgur);

        $formato->save();

        flash()->success('Sucesso', 'Criado Formato com Sucesso!!');

        return redirect()->route('sistema::midia::formatos::index');
    }

    public function edit(Formato $formato)
    {
        return view('sistema.formatos.partials.editar', compact('formato'));
    }

    public function update(Formato $formato, FormatoRequest $request)
    {
        $newFormato = $request->all();

        //se não remover o id vai acabar substituir por quem criou
        unset($newFormato['user_id']);

        $imgur = (Input::get('imageUploadHost') == 'imgur') ? true : false;

        if ($this->imagemUploadedValida('imagem')) { //se está a fazer upload de nova imagem
            $this->uploadImage($formato->getImagem(), 'imagem', $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl)){ //se está a fazer upload por link
            $this->uploadImage($formato->getImagem(), $request->inputImagemUrl, $imgur);
        }else if($request->inputImagemUrl != '' && isUrlImage($request->inputImagemUrl) == false){ //se chegou aqui é pq o link não é valido
            flash()->error('Erro', 'URL de Imagem introduzida não é valida!');
            return redirect()->back()->withInput();
        }else if($imgur != $formato->isImgur()){ //se está a mudar a hospedagem da imagem atual
            $this->changeHostUploadedImage($formato->getImagem(), $imgur);
        }

        $formato->update($newFormato);

        flash()->success('Sucesso', 'Alterado Formato com Sucesso!!');

        return redirect()->route('sistema::midia::formatos::index');
    }

    public function destroy(Formato $formato)
    {
        $imagem = $formato->getImagem();

        if ($formato != null) {
            $formato->delete();
        }

        if ($imagem != null) {
            $imagem->delete();
        }

        $this->deleteUploadedImagem($imagem->nome);

        flash()->success('Sucesso', 'Eliminado Formato com Sucesso!!');
        return redirect()->route('sistema::midia::formatos::index');
    }
}
