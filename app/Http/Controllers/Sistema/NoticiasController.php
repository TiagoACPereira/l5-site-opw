<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\NoticiaRequest;
use App\Models\Conteudo;
use App\Models\Episodio;
use App\Models\Noticia;
use App\Models\NoticiaComponente;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class NoticiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('accessNoticias');
    }

    public function index()
    {
        $noticiasBD = Noticia::latest('publish_at');

        $noticias = $noticiasBD->paginate(10);

        $numero_noticias = $noticiasBD->count();

        return view('sistema.noticias.index', compact('noticias', 'numero_noticias'));
    }

    public function create()
    {
        return view('sistema.noticias.criar');
    }

    public function store(NoticiaRequest $request)
    {
        $noticia = new Noticia(Noticia::$defaults);

        $noticia->user_id = Auth::user()->id;
        $noticia->titulo = $request->titulo;
        $noticia->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $noticia->save();

        $this->syncNoticiaComponentes($noticia, $request);

        flash()->success('Sucesso', 'Criado Noticia com Sucesso!!');

        return redirect()->route('sistema::noticias::index');
    }

    public function edit(Noticia $noticia)
    {
        return view('sistema.noticias.editar', compact('noticia'));
    }

    public function update(Noticia $noticia, NoticiaRequest $request)
    {
        $noticia->titulo = $request->titulo;
        $noticia->publish_at = $request->date_publish_at . ' ' . $request->time_publish_at;

        $noticia->save();

        $this->syncNoticiaComponentes($noticia, $request);

        flash()->success('Sucesso', 'Alterado Noticia com Sucesso!!');

        return redirect()->route('sistema::noticias::index');
    }

    public function destroy(Noticia $noticia)
    {
        foreach ($noticia->componentes as $componente) {
            $componente->deleteComponent();

            $componente->delete();
        }

        $noticia->delete();

        flash()->success('Sucesso', 'Removido Noticia com Sucesso!!');

        return redirect()->route('sistema::noticias::index');
    }

    public function syncNoticiaComponentes(Noticia $noticia, $request)
    {


        $componentesAtualizados = [];
        if($request->has('noticiaComponente'))
        {
            $posicao = 0;
            foreach($request->noticiaComponente as $noticiaComponenteRequest)
            {

                $tipo = key($noticiaComponenteRequest);
                $noticiaComponenteRequest = array_values($noticiaComponenteRequest)[0];
                $idNoticiaComponente = key($noticiaComponenteRequest);
                $componenteRequest = array_values($noticiaComponenteRequest)[0];

                $dadosNoticiaComponente['posicao'] = $posicao++;
                $dadosNoticiaComponente['noticia_completa'] = isset($componenteRequest['noticiaCompleta']) ? true : false;
                unset($componenteRequest['noticiaCompleta']);

                switch ($tipo)
                {
                    case 'conteudo':
                        $dadosNoticiaComponente['tipo_componente'] = Conteudo::class;
                        $componenteRequest['forceHost'] = (Input::get('forceHost'))? 1 : 0 ;
                        $componenteRequest['imgur'] = (Input::get('imageUploadHost') == 'imgur')? true : false;
                        break;

                    case 'episodio':
                        $dadosNoticiaComponente['tipo_componente'] = Episodio::class;
                        break;
                    default:
                        break;
                }

                if($noticia->isComponente($idNoticiaComponente))
                {
                    $noticiaComponente = NoticiaComponente::where('id', $idNoticiaComponente)->first();
                    $noticiaComponente->update($dadosNoticiaComponente);
                } else {
                    $noticiaComponente = $noticia->RelacaoComponentes()->create($dadosNoticiaComponente);
                    $noticiaComponente->save();
                }

                $noticiaComponente->update(['componente' => $componenteRequest]);

                $componentesAtualizados[] = $noticiaComponente->id;
            }
        }

        $todosComponentes = $noticia->RelacaoComponentes()->lists('id')->toArray();
        // array_diff percorre o primeiro array, e se algum valor nesse array
        // não estiver presente nos formatos actualizados é pq foi removido

        foreach(array_diff($todosComponentes, $componentesAtualizados) as $componentesId)
        {
            $noticiaComponente = NoticiaComponente::where('id', $componentesId)->first();

            $noticiaComponente->deleteComponent();

            $noticiaComponente->delete();
        }
    }

}
