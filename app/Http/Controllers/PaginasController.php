<?php

namespace App\Http\Controllers;

use App\Models\Utilizador;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaginasController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function noticias($id = 1)
    {
//        $total_pag = Noticia::all()->count();
//        $quantas = $total_pag / 5;
//        $contagem = ceil($quantas);
//        $quantas = ceil($quantas);
//        $ultima = $quantas;
//
//        $qtd = 5;
//
//        $pagina_e = "";
//        $arraymenos = "";
//
//        if(isset($_GET["page"])){
//            $pagina_e = $_GET["page"];
//        }
//// ctype_digit só dseixa passar se for numero inteiro positivo
////só funciona em numeros em string como no caso da leitura com $_GET
//        if($pagina_e != "" && ctype_digit($pagina_e)){
//            if($pagina_e==0)$pagina_e = 1;
//            $pagina = $pagina_e;
//            $limite = $qtd * $pagina_e;
//            $limite = $limite - 5;
//        }else{
//            $pagina = 1;
//            $limite = 0;
//        }
        $noticias = Noticia::all()->sortByDesc('conta')->forPage($id, 5);

        foreach ($noticias as $noticia) {
            $partes = explode('-', $noticia['criado']);
            $noticia['criado'] = $partes[2] . '/' . $partes[1];
            $autor = Utilizador::all()->where('id', $noticia['autor'])->take(1)->get(0);
            if ($noticia['autor'] !== $autor['id'])
            {
                $noticia['autor'] = 'Ex-membro';
                $noticia['avatar'] = 'Ex-membro-avatar.png';
            }else{
                $noticia['avatar'] = $autor['avatar'];
                $noticia['autor'] = $autor['nick'];
            }

//            $idfinal = $noticia['id'];
//            $titulofinal = $noticia['titulo'];
//            $noticia = $noticia['conteudo'];
//
//            $noticia = costum_tags_html($noticia, 'noticias', $idfinal);
//            echo $noticia;

        }
        return view('noticias', compact('noticias', 'paginator'));
    }
}
