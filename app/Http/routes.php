<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Sistema routes...
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Sistema\CapitulosController;
use App\Http\Controllers\Sistema\FanficsController;
use App\Http\Controllers\PaginasController;
use App\Http\Controllers\Sistema\AmvsController;
use App\Http\Controllers\Sistema\BiografiasController;
use App\Http\Controllers\Sistema\CanaisVlogController;
use App\Http\Controllers\Sistema\CapitulosfanficController;
use App\Http\Controllers\Sistema\EpisodiosController;
use App\Http\Controllers\Sistema\EspeciaisController;
use App\Http\Controllers\Sistema\FilmesController;
use App\Http\Controllers\Sistema\FormatosController;
use App\Http\Controllers\Sistema\HentaisController;
use App\Http\Controllers\Sistema\IndexController;
use App\Http\Controllers\Sistema\InformacoesController;
use App\Http\Controllers\Sistema\JogosController;
use App\Http\Controllers\Sistema\LetrasController;
use App\Http\Controllers\Sistema\LoadFormsController;
use App\Http\Controllers\Sistema\NoticiasController;
use App\Http\Controllers\Sistema\OstsController;
use App\Http\Controllers\Sistema\OvasController;
use App\Http\Controllers\Sistema\ParceirosController;
use App\Http\Controllers\Sistema\PlataformasController;
use App\Http\Controllers\Sistema\SagaController;
use App\Http\Controllers\Sistema\SliderController;
use App\Http\Controllers\Sistema\TutoriaisController;
use App\Http\Controllers\Sistema\UsuariosController;
use App\Http\Controllers\Sistema\TeoriasController;
use App\Http\Controllers\Sistema\VolumesController;
use \App\Http\Controllers\Auth\PasswordController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web' ], function () {

    Route::group(['prefix' => 'sistema', 'as' => 'sistema::'], function () {

        // Authentication routes...
        Route::post('login', ['middleware' => 'guest', 'uses' => AuthController::class . '@postLogin']);
        Route::get('login', ['middleware' => 'guest', 'uses' => AuthController::class . '@getLogin', 'as' => 'login']);
        Route::get('logout', ['uses' => AuthController::class . '@getLogout', 'as' => 'logout']);

        // Password Reset Routes...
        Route::get('password/reset/{token?}', ['uses' => PasswordController::class . '@showResetForm', 'as' => 'password::reset::token']);
        Route::post('password/email', ['uses' => PasswordController::class . '@sendResetLinkEmail', 'as' => 'password::email']);
        Route::post('password/reset', ['uses' => PasswordController::class . '@reset', 'as' => 'password::reset']);

        //Sistema Autentificado routes...
        Route::group(['middleware' => ['auth', 'activeUser']],  function () {

            // Index routes...
            Route::get('/', ['uses' => IndexController::class . '@index', 'as' => 'index']);
            Route::get('home', ['uses' => IndexController::class . '@index']);
            Route::get('index', ['uses' => IndexController::class . '@index']);

            Route::get('biografias', ['uses' => BiografiasController::class . '@index', 'as' => 'biografias']);

            //Form's routes...
            Route::group(['prefix' => 'forms', 'as' => 'forms::'], function(){
                Route::get('mirror/{inputname}/{placeholder}', ['uses' => LoadFormsController::class . '@create_mirror', 'as' => 'mirror::criar']);
                Route::get('formatosmidia/{rd_id}', ['uses' => LoadFormsController::class . '@create_formMidia', 'as' => 'formatosmidia::criar']);

            });

            // Noticias routes...
            Route::resource('noticias', NoticiasController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'noticias::index',
                    'create'  => 'noticias::criar',
                    'store'   => 'noticias::guardar',
                    'edit'    => 'noticias::editar',
                    'update'  => 'noticias::actualizar',
                    'destroy' => 'noticias::apagar',
                ],
            ]);

            //Multimidia routes...
            Route::group(['prefix' => 'midia', 'as' => 'midia::'], function(){

                //Formato routes...
                Route::resource('formatos', FormatosController::class, [
                    'except' => ['show'],
                    'names'  => [
                        'index'   => 'formatos::index',
                        'create'  => 'formatos::criar',
                        'store'   => 'formatos::guardar',
                        'edit'    => 'formatos::editar',
                        'update'  => 'formatos::actualizar',
                        'destroy' => 'formatos::apagar',
                    ],
                ]);

                //Sagas routes...
                Route::resource('sagas', SagaController::class, [
                    'names'  => [
                        'index'   => 'sagas::index',
                        'show'    => 'sagas::mostrar',
                        'create'  => 'sagas::criar',
                        'store'   => 'sagas::guardar',
                        'edit'    => 'sagas::editar',
                        'update'  => 'sagas::actualizar',
                        'destroy' => 'sagas::apagar',
                    ],
                ]);

                // Sagas Episodios routes...
                Route::patch('sagas/episodios/{episodios}', ['uses' => EpisodiosController::class . '@update', 'as' => 'sagas::episodios::actualizar']);
                Route::delete('sagas/episodios/{episodios}', ['uses' => EpisodiosController::class . '@destroy', 'as' => 'sagas::episodios::apagar']);

                Route::resource('sagas/{sagas}/episodios', EpisodiosController::class, [
                    'except' => ['show', 'index', 'update', 'destroy'],
                    'names'  => [
                        'create'  => 'sagas::episodios::criar',
                        'store'   => 'sagas::episodios::guardar',
                        'edit'    => 'sagas::episodios::editar',
                    ],
                ]);

                //Especiais routes...
                Route::resource('especiais', EspeciaisController::class, [
                    'except' => ['show'],
                    'names'  => [
                        'index'   => 'especiais::index',
                        'create'  => 'especiais::criar',
                        'store'   => 'especiais::guardar',
                        'edit'    => 'especiais::editar',
                        'update'  => 'especiais::actualizar',
                        'destroy' => 'especiais::apagar',
                    ],
                ]);

                //Filmes routes...
                Route::resource('filmes', FilmesController::class, [
                    'except' => ['show'],
                    'names'  => [
                        'index'   => 'filmes::index',
                        'create'  => 'filmes::criar',
                        'store'   => 'filmes::guardar',
                        'edit'    => 'filmes::editar',
                        'update'  => 'filmes::actualizar',
                        'destroy' => 'filmes::apagar',
                    ],
                ]);

                //Ovas routes...
                Route::resource('ovas', OvasController::class, [
                    'except' => ['show'],
                    'names'  => [
                        'index'   => 'ovas::index',
                        'create'  => 'ovas::criar',
                        'store'   => 'ovas::guardar',
                        'edit'    => 'ovas::editar',
                        'update'  => 'ovas::actualizar',
                        'destroy' => 'ovas::apagar',
                    ],
                ]);

                // Plataformas routes...
                Route::get('plataformas/{tipo_pataforma}', ['uses' => PlataformasController::class . '@create', 'as' => 'plataformas::criar']);
                Route::resource('plataformas', PlataformasController::class, [
                    'except' => ['show', 'create'],
                    'names'  => [
                        'index'   => 'plataformas::index',
                        'store'   => 'plataformas::guardar',
                        'edit'    => 'plataformas::editar',
                        'update'  => 'plataformas::actualizar',
                        'destroy' => 'plataformas::apagar',
                    ],
                ]);

                // Jogos routes...
                Route::patch('plataformas/jogos/{jogos}', ['uses' => JogosController::class . '@update', 'as' => 'plataformas::jogos::actualizar']);
                Route::delete('plataformas/jogos/{jogos}', ['uses' => JogosController::class . '@destroy', 'as' => 'plataformas::jogos::apagar']);
                Route::resource('plataformas/{plataformas}/jogos', JogosController::class, [
                    'except' => ['show', 'index', 'update', 'destroy'],
                    'names'  => [
                        'create'  => 'plataformas::jogos::criar',
                        'store'   => 'plataformas::jogos::guardar',
                        'edit'    => 'plataformas::jogos::editar',
                    ],
                ]);

                //Hentais routes...
                Route::resource('hentais', HentaisController::class, [
                    'except' => ['show'],
                    'names'  => [
                        'index'   => 'hentais::index',
                        'create'  => 'hentais::criar',
                        'store'   => 'hentais::guardar',
                        'edit'    => 'hentais::editar',
                        'update'  => 'hentais::actualizar',
                        'destroy' => 'hentais::apagar',
                    ],
                ]);

                //Hentais routes...
                Route::resource('osts', OstsController::class, [
                    'except' => ['show'],
                    'names'  => [
                        'index'   => 'osts::index',
                        'create'  => 'osts::criar',
                        'store'   => 'osts::guardar',
                        'edit'    => 'osts::editar',
                        'update'  => 'osts::actualizar',
                        'destroy' => 'osts::apagar',
                    ],
                ]);

                //Volumes routes...
                Route::resource('volumes', VolumesController::class, [
                    'names'  => [
                        'index'   => 'volumes::index',
                        'show'    => 'volumes::mostrar',
                        'create'  => 'volumes::criar',
                        'store'   => 'volumes::guardar',
                        'edit'    => 'volumes::editar',
                        'update'  => 'volumes::actualizar',
                        'destroy' => 'volumes::apagar',
                    ],
                ]);

                // Volumes Capitulos routes...
                Route::patch('volumes/capitulos/{capitulos}', ['uses' => CapitulosController::class . '@update', 'as' => 'volumes::capitulos::actualizar']);
                Route::delete('volumes/capitulos/{capitulos}', ['uses' => CapitulosController::class . '@destroy', 'as' => 'volumes::capitulos::apagar']);
                Route::resource('volumes/{volumes}/capitulos', CapitulosController::class, [
                    'except' => ['show', 'index', 'update', 'destroy'],
                    'names'  => [
                        'create'  => 'volumes::capitulos::criar',
                        'store'   => 'volumes::capitulos::guardar',
                        'edit'    => 'volumes::capitulos::editar',
                    ],
                ]);

            });

            // Usuarios routes...
            Route::patch('usuarios/{usuarios}/desactivar', ['uses' => UsuariosController::class . '@desactivar', 'as' => 'usuarios::desactivar']);
            Route::patch('usuarios/{usuarios}/activar', ['uses' => UsuariosController::class . '@activar', 'as' => 'usuarios::activar']);
            Route::resource('usuarios', UsuariosController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'usuarios::index',
                    'create'  => 'usuarios::criar',
                    'store'   => 'usuarios::guardar',
                    'edit'    => 'usuarios::editar',
                    'update'  => 'usuarios::actualizar',
                    'destroy' => 'usuarios::apagar',
                ],
            ]);

            // Slides routes...
            Route::resource('slider', SliderController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'slider::index',
                    'create'  => 'slider::criar',
                    'store'   => 'slider::guardar',
                    'edit'    => 'slider::editar',
                    'update'  => 'slider::actualizar',
                    'destroy' => 'slider::apagar',
                ],
            ]);

            // Parceiros routes...
            Route::resource('parceiros', ParceirosController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'parceiros::index',
                    'create'  => 'parceiros::criar',
                    'store'   => 'parceiros::guardar',
                    'edit'    => 'parceiros::editar',
                    'update'  => 'parceiros::actualizar',
                    'destroy' => 'parceiros::apagar',
                ],
            ]);

            // Informacoes routes...
            Route::resource('informacoes', InformacoesController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'informacoes::index',
                    'create'  => 'informacoes::criar',
                    'store'   => 'informacoes::guardar',
                    'edit'    => 'informacoes::editar',
                    'update'  => 'informacoes::actualizar',
                    'destroy' => 'informacoes::apagar',
                ],
            ]);

            // Tutoriais routes...
            Route::resource('tutoriais', TutoriaisController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'tutoriais::index',
                    'create'  => 'tutoriais::criar',
                    'store'   => 'tutoriais::guardar',
                    'edit'    => 'tutoriais::editar',
                    'update'  => 'tutoriais::actualizar',
                    'destroy' => 'tutoriais::apagar',
                ],
            ]);

            // Teorias routes...
            Route::resource('letras', LetrasController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'letras::index',
                    'create'  => 'letras::criar',
                    'store'   => 'letras::guardar',
                    'edit'    => 'letras::editar',
                    'update'  => 'letras::actualizar',
                    'destroy' => 'letras::apagar',
                ],
            ]);

            // Teorias routes...
            Route::resource('teorias', TeoriasController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'teorias::index',
                    'create'  => 'teorias::criar',
                    'store'   => 'teorias::guardar',
                    'edit'    => 'teorias::editar',
                    'update'  => 'teorias::actualizar',
                    'destroy' => 'teorias::apagar',
                ],
            ]);

            // Vlogs routes...
            Route::resource('canaisvlog', CanaisVlogController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'canaisvlog::index',
                    'create'  => 'canaisvlog::criar',
                    'store'   => 'canaisvlog::guardar',
                    'edit'    => 'canaisvlog::editar',
                    'update'  => 'canaisvlog::actualizar',
                    'destroy' => 'canaisvlog::apagar',
                ],
            ]);

            // AMV'S routes...
            Route::resource('amvs', AmvsController::class, [
                'only' => ['index', 'store', 'destroy'],
                'names'  => [
                    'index'   => 'amvs::index',
                    'store'   => 'amvs::guardar',
                    'destroy' => 'amvs::apagar',
                ],
            ]);

            // Fanfic's routes...
            Route::resource('fanfics', FanficsController::class, [
                'except' => ['show'],
                'names'  => [
                    'index'   => 'fanfics::index',
                    'create'  => 'fanfics::criar',
                    'store'   => 'fanfics::guardar',
                    'edit'    => 'fanfics::editar',
                    'update'  => 'fanfics::actualizar',
                    'destroy' => 'fanfics::apagar',
                ],
            ]);

            // Fanfic's Capitulos routes...
            Route::patch('fanfics/capitulosfanfics/{capitulosfanfics}', ['uses' => CapitulosfanficController::class . '@update', 'as' => 'fanfics::capitulos::actualizar']);
            Route::delete('fanfics/capitulosfanfics/{capitulosfanfics}', ['uses' => CapitulosfanficController::class . '@destroy', 'as' => 'fanfics::capitulos::apagar']);
            Route::resource('fanfics/{fanfics}/capitulosfanfics', CapitulosfanficController::class, [
                'except' => ['show', 'index', 'update', 'destroy'],
                'names'  => [
                    'create'  => 'fanfics::capitulos::criar',
                    'store'   => 'fanfics::capitulos::guardar',
                    'edit'    => 'fanfics::capitulos::editar',
                ],
            ]);

        });
    });

    Route::get('/', [
        'uses' => PaginasController::class . '@index',
        'as'   => 'index'
    ]);

    Route::get('amv', [
        'uses' => PaginasController::class . '@amv',
        'as'   => 'amv'
    ]);

    Route::get('arquivo_de_noticias', [
        'uses' => PaginasController::class . '@arquivoDeNoticias',
        'as'   => 'arquivo de noticias'
    ]);

    Route::get('biografia/{id}', [
        'uses' => PaginasController::class . '@biografia',
        'as'   => 'biografia'
    ]);

    Route::get('comentario', [
        'uses' => PaginasController::class . '@comentario',
        'as'   => 'comentario'
    ]);

    Route::get('epionline', [
        'uses' => PaginasController::class . '@epionline',
        'as'   => 'epionline'
    ]);

    Route::get('sagas', [
        'uses' => PaginasController::class . '@sagas',
        'as'   => 'sagas'
    ]);

    Route::get('saga/{id}', [
        'uses' => PaginasController::class . '@sagas',
        'as'   => 'saga'
    ]);

    Route::get('equipe', [
        'uses' => PaginasController::class . '@equipe',
        'as'   => 'equipe'
    ]);

    Route::get('especiais', [
        'uses' => PaginasController::class . '@especiais',
        'as'   => 'especiais'
    ]);

    Route::get('faleconosco', [
        'uses' => PaginasController::class . '@faleconosco',
        'as'   => 'faleconosco'
    ]);

    Route::get('fanfic', [
        'uses' => PaginasController::class . '@fanfic',
        'as'   => 'fanfic'
    ]);

    Route::get('fanfics', [
        'uses' => PaginasController::class . '@fanfics',
        'as'   => 'fanfics'
    ]);

    Route::get('filmes', [
        'uses' => PaginasController::class . '@filmes',
        'as'   => 'filmes'
    ]);

    Route::get('frutas', [
        'uses' => PaginasController::class . '@frutas',
        'as'   => 'frutas'
    ]);

    Route::get('hentais', [
        'uses' => PaginasController::class . '@hentais',
        'as'   => 'hentais'
    ]);

    Route::get('hentai/{id}', [
        'uses' => PaginasController::class . '@hentai',
        'as'   => 'hentai'
    ]);

    Route::get('informacao/{id}', [
        'uses' => PaginasController::class . '@informacao',
        'as'   => 'informacao'
    ]);

    Route::get('jogos', [
        'uses' => PaginasController::class . '@jogos',
        'as'   => 'jogos'
    ]);

    Route::get('jogos/{id}', [
        'uses' => PaginasController::class . '@jogos',
        'as'   => 'jogos'
    ]);

    Route::get('letras', [
        'uses' => PaginasController::class . '@letras',
        'as'   => 'letras'
    ]);

    Route::get('letra/{id}', [
        'uses' => PaginasController::class . '@letra',
        'as'   => 'letra'
    ]);

    Route::get('linkoff', [
        'uses' => PaginasController::class . '@linkoff',
        'as'   => 'linkoff'
    ]);

    Route::get('mangas', [
        'uses' => PaginasController::class . '@mangas',
        'as'   => 'mangas'
    ]);

    Route::get('midia', [
        'uses' => PaginasController::class . '@midia',
        'as'   => 'midia'
    ]);

    Route::get('noticias/{id?}', [
        'uses' => PaginasController::class . '@noticias',
        'as'   => 'noticias'
    ]);

    Route::get('ost', [
        'uses' => PaginasController::class . '@ost',
        'as'   => 'ost'
    ]);

    Route::get('ovas', [
        'uses' => PaginasController::class . '@ovas',
        'as'   => 'ovas'
    ]);

    Route::get('roms', [
        'uses' => PaginasController::class . '@roms',
        'as'   => 'roms'
    ]);

    Route::get('roms/{id}', [
        'uses' => PaginasController::class . '@roms',
        'as'   => 'roms'
    ]);

    Route::get('todabio', [
        'uses' => PaginasController::class . '@todabio',
        'as'   => 'todabio'
    ]);

    Route::get('todainfo', [
        'uses' => PaginasController::class . '@todainfo',
        'as'   => 'todainfo'
    ]);

    Route::get('todasfrutas', [
        'uses' => PaginasController::class . '@todasfrutas',
        'as'   => 'todasfrutas'
    ]);

    Route::get('tutorial/{id}', [
        'uses' => PaginasController::class . '@tutoriais',
        'as'   => 'tutorial'
    ]);

    Route::get('vlog', [
        'uses' => PaginasController::class . '@vlog',
        'as'   => 'vlog'
    ]);
});