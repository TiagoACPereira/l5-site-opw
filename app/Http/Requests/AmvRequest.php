<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AmvRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video_id' => 'required|unique:amvs'
        ];
    }

    public function messages()
    {
        return [
            'video_id.required' => 'Necessita de introduzir o id do video.',
            'video_id.unique'   => 'O video que tentou adicionar já se encontra adicionado.',
        ];
    }

}
