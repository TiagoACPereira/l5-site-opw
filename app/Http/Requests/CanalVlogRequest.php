<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\RecaptchaTrait;

class CanalVlogRequest extends Request
{

    use RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'nome'         => 'required|max:255',
            'id_channel'   => 'required_if:nome_channel,|max:255',
            'nome_channel' => 'required_if:id_channel,|max:255'
        ];

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        return [
            'nome.max'   => 'O nome só poder ter um maximo de 255 caracteres.',
            'nome.required' => 'Necessita de dar um nome ao Canal Vlog.',
            'id_channel.required_if'   => 'Necessita de preencher o id ou nome do canal.',
            'nome_channel.required_if'   => ''
        ];
    }
}
