<?php

namespace App\Http\Requests;


use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\RecaptchaTrait;

class NoticiaRequest extends Request
{

    use RecaptchaTrait, ImagemTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo' => 'required|max:255'
        ];

        if(request()->get('noticiaComponente') != null)
        {
            foreach (request()->get('noticiaComponente') as $noticiaComponente) {
                foreach ($noticiaComponente as $tipo => $componente) {
                    $key = key($componente);
                    switch ($tipo) {
                        case 'conteudo':
                                $regras['noticiaComponente.' . $key . '.conteudo.' . $key . '.strConteudo'] = 'required';
                            break;
                        default:
                            break;
                    }
                }
            }
        }else{
            $regras['componente'] = 'required';
        }

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required' => 'Necessita de dar um Título à Noticia.',
            'titulo.max'      => 'O Título só poder ter um máximo de 255 caracteres.'
        ];

        if(request()->get('noticiaComponente') != null)
        {
            foreach (request()->get('noticiaComponente') as $noticiaComponente) {
                foreach ($noticiaComponente as $tipo => $componente) {
                    $key = key($componente);
                    switch ($tipo) {
                        case 'conteudo':
                            $mensagens['noticiaComponente.' . $key . '.conteudo.' . $key . '.strConteudo.required'] = 'Não pode ter um conteudo vazio';
                            break;
                        default:
                            break;
                    }
                }
            }
        }else{
            $mensagens['componente.required'] = "Necessita de ter pelo menos 1 componente.";
        }

        $mensagens = $this->mensagensImagem($mensagens);

        return $mensagens;
    }
}
