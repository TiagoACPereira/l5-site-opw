<?php

namespace App\Http\Requests;

use App\Traits\Requests\FormatoMidiaTrait;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class EspecialRequest extends Request
{

    use FormatoMidiaTrait;

    use ImagemTrait;

    use RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'     => 'required|max:255',
            'sub_titulo' => 'required|max:255'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::midia::especiais::guardar',
            'sistema::midia::especiais::actualizar');

        $regras = $this->regrasFormatoMidia($regras);

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required'     => 'Necessita de dar um Título ao Especial.',
            'titulo.max'          => 'O Título só poder ter um máximo de 255 caracteres.',
            'sub_titulo.required' => 'Necessita de dar um Sub Título ao Especial.',
            'sub_titulo.max'      => 'O Título só poder ter um máximo de 255 caracteres.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        $mensagens = $this->mensagensFM($mensagens);

        return $mensagens;
    }
}
