<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\MirrorTrait;
use App\Traits\Requests\RecaptchaTrait;

class OstRequest extends Request
{

    use ImagemTrait, MirrorTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'  => 'required|max:255',
            'album'   => 'required|max:255',
            'idioma'  => 'required',
            'tamanho' => 'required',
            'formato' => 'required'
        ];

        $regras = $this->regrasMirror($regras);

        $regras = $this->regrasImagem($regras, 'sistema::midia::osts::guardar',
            'sistema::midia::osts::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required'  => 'Necessita de dar um Título.',
            'titulo.max'       => 'O Título só poder ter um máximo de 255 caracteres.',
            'album.required'   => 'Necessita de dar um Album.',
            'album.max'        => 'O Album só poder ter um máximo de 255 caracteres.',
            'idioma.required'  => 'Falta introduzir/escolher um idioma.',
            'tamanho.required' => 'Falta introduzir/escolher um tamanho.',
            'formato.required' => 'Falta introduzir/escolher um formato.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);
        $mensagens = $this->mensagensMirror($mensagens);

        return $mensagens;
    }
}
