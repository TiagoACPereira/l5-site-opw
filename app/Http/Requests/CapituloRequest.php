<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\MirrorTrait;
use App\Traits\Requests\RecaptchaTrait;

class CapituloRequest extends Request
{

    use ImagemTrait, MirrorTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'    => 'required|max:255',
            'numero'    => 'required|integer',
            'scanlator' => 'required',
        ];

        $regras = $this->regrasMirror($regras);

        $regras = $this->regrasImagem($regras, 'sistema::midia::volumes::capitulos::guardar',
            'sistema::midia::volumes::capitulos::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required'    => 'Necessita de dar um Título.',
            'titulo.max'         => 'O Título só poder ter um máximo de 255 caracteres.',
            'scanlator.required' => 'Falta introduzir/escolher um scanlator.',
            'numero.required'    => 'A introdução de um numero é obrigatório.',
            'numero.integer'     => 'O numero tem de ser número... -.-'
        ];

        $mensagens = $this->mensagensImagem($mensagens);
        $mensagens = $this->mensagensMirror($mensagens);

        return $mensagens;
    }
}
