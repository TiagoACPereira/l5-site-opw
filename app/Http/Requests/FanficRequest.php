<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\RecaptchaTrait;

class FanficRequest extends Request
{

    use RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'        => 'required|max:255',
            'autor'         => 'required|max:255',
            'link_original' => 'url|max:255',
            'status'        => 'required',
        ];

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }


    public function messages()
    {
        return [
            'titulo.required'   => 'Necessita de especificar o Título da Fanfic.',
            'titulo.max'        => 'O Título só poder ter um máximo de 255 caracteres.',
            'autor.required'    => 'Necessita de especificar o autor da Fanfic.',
            'autor.max'         => 'O Autor só poder ter um máximo de 255 caracteres.',
            'link_original.url' => 'O Link introduzido não é válido.',
            'link_original.max' => 'O Link só poder ter um máximo de 255 caracteres.',
            'status.required'   => 'Não especificou o Status atual da fanfic.'
        ];
    }
}
