<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class PlataformaRequest extends Request
{

    use ImagemTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'nome' => 'required',
            'tipo' => 'required'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::midia::plataformas::guardar',
            'sistema::midia::plataformas::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'descricao.required' => 'Necessita de introduzir o nome da Plataforma.',
            'tipo.required'      => 'Erro com o tipo de plataforma (ROM/JOGO)'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        return $mensagens;
    }

}
