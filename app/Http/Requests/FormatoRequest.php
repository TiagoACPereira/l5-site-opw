<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class FormatoRequest extends Request
{

    use ImagemTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $regras = [
            'descricao' => 'required',
            'numero'    => 'required|integer'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::midia::formatos::guardar',
            'sistema::midia::formatos::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'descricao.required' => 'Necessita de introduzir o nome do formato.',
            'numero.required'    => 'Necessita de introduzir o numero do formato.',
            'numero.integer'     => 'O numero tem de ser um numero.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        return $mensagens;
    }
}
