<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\RecaptchaTrait;

class TutorialRequest extends Request
{

    use RecaptchaTrait;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'      => 'required|max:255',
            'sub_titulo'  => 'required|max:255',
            'strConteudo' => 'required'
        ];

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        return [
            'titulo.required'      => 'Necessita de dar um Título ao Tutorial.',
            'titulo.max'           => 'O Título só poder ter um máximo de 255 caracteres.',
            'sub_titulo.required'  => 'Necessita de dar um Sub-Título ao Tutorial.',
            'sub_titulo.max'       => 'O Sub-Título só poder ter um máximo de 255 caracteres.',
            'strConteudo.required' => 'Falta introduzir algum conteudo.',
        ];
    }

}
