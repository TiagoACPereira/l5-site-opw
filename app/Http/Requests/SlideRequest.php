<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class SlideRequest extends Request
{

    use RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'informacao' => 'required|max:255',
            'url'        => 'required|url|max:255'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::slider::guardar', 'sistema::slider::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'informacao.required' => 'Necessita de dar um Título ao Parceiro.',
            'informacao.max'      => 'O Título só poder ter um máximo de 255 caracteres.',
            'url.required'        => 'Não introduziu o url do Parceiro.',
            'url.url'             => 'O Url introduzido não é válido',
            'url.max'             => 'O Url só poder ter um máximo de 255 caracteres.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        return $mensagens;
    }

}
