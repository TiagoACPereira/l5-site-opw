<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class ParceiroRequest extends Request
{

    use ImagemTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'nome' => 'required|max:255',
            'url'  => 'required|url|max:255'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::parceiros::guardar', 'sistema::parceiros::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'informacao.required' => 'Necessita de dar um Título ao Slide.',
            'informacao.max'      => 'O Título só poder ter um máximo de 255 caracteres.',
            'url.required'        => 'Não introduziu o url do Slide.',
            'url.url'             => 'O Url introduzido não é válido',
            'url.max'             => 'O Url só poder ter um máximo de 255 caracteres.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        return $mensagens;
    }

}
