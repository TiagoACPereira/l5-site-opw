<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\MirrorTrait;
use App\Traits\Requests\RecaptchaTrait;

class HentaiRequest extends Request
{

    use ImagemTrait, MirrorTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'     => 'required|max:255',
            'sub_titulo' => 'required|max:255',
            'scan'       => 'required',
            'idioma'     => 'required',
            'tamanho'    => 'required',
            'formato'    => 'required'
        ];

        $regras = $this->regrasMirror($regras);

        $regras = $this->regrasImagem($regras, 'sistema::midia::hentais::guardar',
            'sistema::midia::hentais::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required'     => 'Necessita de dar um Título.',
            'titulo.max'          => 'O Título só poder ter um máximo de 255 caracteres.',
            'sub_titulo.required' => 'Necessita de dar um Sub Título.',
            'sub_titulo.max'      => 'O Sub Título só poder ter um máximo de 255 caracteres.',
            'scan.required'       => 'Falta introduzir/escolher um scan.',
            'idioma.required'     => 'Falta introduzir/escolher um idioma.',
            'tamanho.required'    => 'Falta introduzir/escolher um tamanho.',
            'formato.required'    => 'Falta introduzir/escolher um formato.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);
        $mensagens = $this->mensagensMirror($mensagens);

        return $mensagens;
    }
}
