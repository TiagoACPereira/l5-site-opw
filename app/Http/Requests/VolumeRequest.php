<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\MirrorTrait;
use App\Traits\Requests\RecaptchaTrait;

class VolumeRequest extends Request
{

    use ImagemTrait, MirrorTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'numero'    => 'required|integer',
            'data'      => 'required',
            'scanlator' => 'required',
        ];

        $regras = $this->regrasMirror($regras);

        $regras = $this->regrasImagem($regras, 'sistema::midia::volumes::guardar',
            'sistema::midia::volumes::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'numero.required'    => 'A introdução de um numero é obrigatório.',
            'data.required'      => 'Necessita introduzir data lançamento do Volume.',
            'scanlator.required' => 'Falta introduzir/escolher um scanlator.',
            'numero.integer'     => 'O numero tem de ser número... -.-'
        ];

        $mensagens = $this->mensagensImagem($mensagens);
        $mensagens = $this->mensagensMirror($mensagens);

        return $mensagens;
    }

}
