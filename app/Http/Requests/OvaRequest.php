<?php

namespace App\Http\Requests;

use App\Traits\Requests\FormatoMidiaTrait;
use App\Traits\Requests\ImagemTrait;
use Illuminate\Support\Facades\Route;

class OvaRequest extends Request
{

    use FormatoMidiaTrait;
    use ImagemTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo' => 'required|max:255',
            'numero' => 'required|integer'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::midia::ovas::guardar', 'sistema::midia::ovas::actualizar');

        $regras = $this->regrasFormatoMidia($regras);

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required' => 'Necessita de dar um Título ao Ova.',
            'titulo.max'      => 'O Título só poder ter um máximo de 255 caracteres.',
            'numero.required' => 'Necessita introduzir o número do Ova.',
            'numero.integer'  => 'O numero de Ova tem de ser um número.'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        $mensagens = $this->mensagensFM($mensagens);

        return $mensagens;
    }
}
