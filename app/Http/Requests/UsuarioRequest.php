<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class UsuarioRequest extends Request
{

    use RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = Route::getCurrentRoute()->getName();

        if ($routeName == 'sistema::usuarios::guardar') {
            return [
                'nome'     => 'required|max:255',
                'login'    => 'required|max:255|unique:users',
                'email'    => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
            ];
        }

        if ($routeName == 'sistema::usuarios::actualizar') {
            $regras = [
                'nome'           => 'required|max:255',
                'login'          => 'required|max:255|unique:users,login,' . Request::get('id'),
                'email'          => 'required|email|max:255|unique:users,email,' . Request::get('id'),
                'dataNascimento' => 'date',
                'password'       => 'confirmed|min:6',
                'avatar'         => 'image',
            ];

            $regras = $this->regrasRecaptcha($regras);

            return $regras;
        }

        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'nome.required'       => 'Necessita de dar um nome ao Usuário.',
            'nome.max'            => 'O nome só poder ter um maximo de 255 caracteres.',
            'login.required'      => 'Necessita de dar um login ao Usuário.',
            'login.max'           => 'O login só poder ter um maximo de 255 caracteres.',
            'login.unique'        => 'O login que está a tentar usar já se encontra em uso.',
            'email.required'      => 'Necessita de dar um email ao Usuário.',
            'email.email'         => 'O email tem de ser um email... -.-\'.',
            'email.max'           => 'O email só poder ter um maximo de 255 caracteres.',
            'email.unique'        => 'O email que está a tentar usar já se encontra em uso.',
            'dataNascimento.date' => 'A data de nascimento é inválida.',
            'password.min'        => 'A password tem de ter um minimo de 6 caracteres.',
            'password.confirmed'  => 'As password\'s não são iguais',
            'avatar.image'        => 'A imagem para avatar não é uma imagem válida.'
        ];
    }

}
