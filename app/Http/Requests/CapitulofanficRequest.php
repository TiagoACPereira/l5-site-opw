<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\RecaptchaTrait;

class CapitulofanficRequest extends Request
{

    use RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'titulo'        => 'required|max:255',
            'numero'        => 'required|integer',
            'link_original' => 'url|max:255',
            'strConteudo'   => 'required'
        ];

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        return [
            'titulo.required'      => 'Falta o título do capitulo da fanfic.',
            'titulo.max'           => 'O Título só poder ter um máximo de 255 caracteres.',
            'numero.required'      => 'A introdução de um numero é obrigatório.',
            'numero.integer'       => 'O numero tem de ser número... -.-',
            'link_original.url'    => 'O Link introduzido não é válido.',
            'link_original.max'    => 'O Link só poder ter um máximo de 255 caracteres.',
            'strConteudo.required' => 'Falta introduzir algum conteudo.',
        ];
    }
}
