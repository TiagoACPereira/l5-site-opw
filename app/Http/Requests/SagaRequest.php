<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\Requests\ImagemTrait;
use App\Traits\Requests\RecaptchaTrait;
use Illuminate\Support\Facades\Route;

class SagaRequest extends Request
{

    use ImagemTrait, RecaptchaTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $regras = [
            'titulo' => 'required|max:255',
            'numero' => 'required|integer'
        ];

        $regras = $this->regrasImagem($regras, 'sistema::midia::sagas::guardar', 'sistema::midia::sagas::actualizar');

        $regras = $this->regrasRecaptcha($regras);

        return $regras;
    }

    public function messages()
    {
        $mensagens = [
            'titulo.required' => 'Falta o título da Saga.',
            'titulo.max'      => 'O Título só poder ter um máximo de 255 caracteres.',
            'numero.required' => 'A introdução de um numero é obrigatório.',
            'numero.integer'  => 'O numero tem de ser número... -.-'
        ];

        $mensagens = $this->mensagensImagem($mensagens);

        return $mensagens;
    }
}
