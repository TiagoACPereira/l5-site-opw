<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RedirectUnlessHaveAccessInformacoes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $utilizador = Auth::user();
        //se não for redator ou revisor não pode entrar independentemente da route
        if (!$utilizador->isRevisor() && !$utilizador->isRedator() && !$utilizador->isAdmin()) {
            flash()->overlay('Erro', 'Não tem permissão para aceder às Informações', 'error');
            return redirect()->back();
        }

        $route = Route::getCurrentRoute();

        //explicado em AccessTutoriais
        if (!$utilizador->isAdmin() && !$utilizador->isRevisor() && $route->hasParameter('tutoriais') && !Auth::user()->Infos()->get()->contains($route->getParameter('informacoes')))
        {
            flash()->overlay('Erro', 'Não pode alterar Informações que não foram criados por si.', 'error');
            return redirect()->route('sistema::informacoes::index');
        }

        return $next($request);
    }
}
