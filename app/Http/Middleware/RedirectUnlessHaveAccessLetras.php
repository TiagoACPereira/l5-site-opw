<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RedirectUnlessHaveAccessLetras
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $utilizador = Auth::user();
        //se não for redator ou revisor não pode entrar independentemente da route
        if (!$utilizador->isRevisor() && !$utilizador->isRedator() && !$utilizador->isAdmin() && !$utilizador->isFanArea) {
            flash()->overlay('Erro', 'Não tem permissão para aceder às Teorias', 'error');
            return redirect()->back();
        }

        $route = Route::getCurrentRoute();

        //explicação em AccessTutoriais
        if (!$utilizador->isAdmin() && !$utilizador->isRevisor() && $route->hasParameter('teorias') && !Auth::user()->Teorias()->get()->contains($route->getParameter('teorias')))
        {
            flash()->overlay('Erro', 'Não pode alterar Teorias que não foram criados por si.', 'error');
            return redirect()->route('sistema::teorias::index');
        }

        return $next($request);
    }
}
