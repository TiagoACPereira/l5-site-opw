<?php

namespace App\Http\Middleware;

use Closure;

class RedirectToHttps
{
    /**
     * Verifica se request está em HTTPS e redireciona para
     * HTTPS se não tiver.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->secure() && config('app.https')){
            return redirect()->secure($request->path());
        }

        return $next($request);
    }
}
