<?php

namespace App\Http\Middleware;

use Closure;

class RedirectUnlessHaveAccessSlide
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->user()->isAdmin() || $request->user()->isRevisor())
            return $next($request);

        flash()->overlay('Erro', 'Não tem permissão para aceder aos Slides', 'error');
        return redirect()->back();
    }
}
