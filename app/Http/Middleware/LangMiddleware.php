<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Vai buscar o dominio e todos os subdominios do url atual
        $subdomain = parse_url($request->url())['host'];
        $subdomain_array = explode('.', $subdomain);

        // Verifica se existe um subdominio válido (primeiro subdominio
        // vai ser a linguagem
        switch ($subdomain_array[0]) {
            case 'en':
                App::setLocale('en');
                Carbon::setLocale('en');
                break;

            case 'pt':
                App::setLocale('pt-BR');
                Carbon::setLocale('pt_BR');
                break;

            default:
                // Caso o subdominio não seja valido à que verificar se estamos
                // a verificar se é um url sem subdominio
                // Vai buscar dominio atual ao .env
                $url_array = parse_url(config('app.url'));

                // Se o url no .env não for considerado url não vai devolver host
                // e por isso usa o path
                if(isset($url_array['host']))
                    $domain =  $url_array['host'];
                else
                    $domain =  $url_array['path'];

                // Se o 'subdominio' do url atual NÃO for igual ao dominio no .env
                // que dizer que tentou usar um subdominio não permitido
                // (ADICIONADO NO SWITCH)
                // Neste caso troca o dominio atual pelo o do .env
                if($subdomain != $domain)
                {
                    $url = $request->url();
                    $url = str_replace($subdomain, $domain, $url);

                    return redirect()->guest($url);
                }

                break;
        }

        return $next($request);
    }
}
