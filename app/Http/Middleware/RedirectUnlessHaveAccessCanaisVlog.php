<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectUnlessHaveAccessCanaisVlog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $utilizador = Auth::user();

        if (!$utilizador->isAdmin()) {
            flash()->overlay('Erro', 'Não tem permissão para aceder aos Canais Vlog', 'error');
            return redirect()->back();
        }

        return $next($request);
    }
}
