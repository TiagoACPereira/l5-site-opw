<?php

namespace App\Http\Middleware;

use Closure;

class RedirectUnlessHaveAccessParceiros
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! $request->user()->isAdmin()){
            flash()->overlay('Erro', 'Não tem permissão para aceder aos Parceiros', 'error');
            return redirect()->back();
        }

        return $next($request);
    }
}
