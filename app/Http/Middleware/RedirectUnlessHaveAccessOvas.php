<?php

namespace App\Http\Middleware;

use Closure;

class RedirectUnlessHaveAccessOvas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->user()->isAdmin() || $request->user()->isMidiaUploader())
            return $next($request);

        flash()->overlay('Erro', 'Não tem permissão para aceder aos Ovas!', 'error');
        return redirect()->back();
    }
}
