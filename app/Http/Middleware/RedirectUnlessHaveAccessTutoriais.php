<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RedirectUnlessHaveAccessTutoriais
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $utilizador = Auth::user();
        //se não for redator ou revisor não pode entrar independentemente da route
        if (!$utilizador->isRevisor() && !$utilizador->isRedator() && !$utilizador->isAdmin()) {
            flash()->overlay('Erro', 'Não tem permissão para aceder aos Tutoriais', 'error');
            return redirect()->back();
        }

        $route = Route::getCurrentRoute();

        //se contem tutoriais é pq está a a fazer algo com um tutorial
        //ex: aceder pagina editar, a enviar a edição e a remover
        //    criar e enviar criado não parece estar no parametro...
        //se não for admin e não for revisor é pq só é redator.
        //se tiver a fazer algo a um tutorial existente e não for dele não deixa.
        if (!$utilizador->isAdmin() && !$utilizador->isRevisor() && $route->hasParameter('tutoriais') && !Auth::user()->Tutoriais()->get()->contains($route->getParameter('tutoriais')))
        {
            flash()->overlay('Erro', 'Não pode alterar Tutoriais que não foram criados por si.', 'error');
            return redirect()->route('sistema::tutoriais::index');
        }

        return $next($request);
    }
}
