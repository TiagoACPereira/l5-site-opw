<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class RedirectIfNotActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->active)
            return $next($request);
        else{
            $msg =  Lang::has('auth.deactivated')
                ? Lang::get('auth.deactivated')
                : 'This Users is deactivated.';
            //Auth::guard($this->getGuard())->logout(); //copiado do metodo em getLogout
            return redirect()->route('sistema::login')->withErrors($msg);
        }
    }
}
