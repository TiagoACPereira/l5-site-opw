<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RedirectUnlessHaveAccessUsuarios
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $utilizador = Auth::user();
        $route = Route::getCurrentRoute();
        $routeName = $route->getName();

//        dd($routeName);

        //se não for admin só pode alterar os seus proprios dados (route 'editar' e 'actualizar')
        if (!(str_is('*usuarios::editar', $routeName) || str_is('*usuarios::actualizar',
                    $routeName)) && !$utilizador->isAdmin()
        ) {
            return redirect()->route('sistema::usuarios::editar', ['id' => Auth::user()->id]);
        }

        //se não for admin quer dizer que está em editar ou actualizar
        //se não for null é pq está a alterar alguns dados
        //se não for o mesmo id é pq está a tentar alterar o que não deve
        if ($route->getParameter('usuarios') != null && !$utilizador->isAdmin() && $route->getParameter('usuarios')->id != Auth::user()->id) {
            flash()->overlay('ATENÇÃO', 'NÃO PODER ALTERAR.', 'error');

            return redirect()->route('sistema::usuarios::editar', ['id' => Auth::user()->id]);
        }

        return $next($request);
    }
}
