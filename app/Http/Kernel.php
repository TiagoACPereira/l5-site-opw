<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\LangMiddleware::class,
        \App\Http\Middleware\RedirectToHttps::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                   => \App\Http\Middleware\Authenticate::class,
        'activeUser'        => \App\Http\Middleware\RedirectIfNotActiveUser::class,
        'auth.basic'             => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'                  => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle'               => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'admin'                  => \App\Http\Middleware\RedirectIfNotAnAdmin::class,
        'addPlainTextToHeader'   => \App\Http\Middleware\AddPlainTextToHeader::class,
        'accessSlide'            => \App\Http\Middleware\RedirectUnlessHaveAccessSlide::class,
        'accessParceiros'        => \App\Http\Middleware\RedirectUnlessHaveAccessParceiros::class,
        'accessInformacoes'      => \App\Http\Middleware\RedirectUnlessHaveAccessInformacoes::class,
        'accessTutoriais'        => \App\Http\Middleware\RedirectUnlessHaveAccessTutoriais::class,
        'accessTeorias'          => \App\Http\Middleware\RedirectUnlessHaveAccessTeorias::class,
        'accessLetras'           => \App\Http\Middleware\RedirectUnlessHaveAccessLetras::class,
        'accessCanaisVlog'       => \App\Http\Middleware\RedirectUnlessHaveAccessCanaisVlog::class,
        'accessAmvs'             => \App\Http\Middleware\RedirectUnlessHaveAccessAmvs::class,
        'accessFanfics'          => \App\Http\Middleware\RedirectUnlessHaveAccessFanfics::class,
        'accessCapitulosfanfics' => \App\Http\Middleware\RedirectUnlessHaveAccessCapitulosfanfic::class,
        'accessSagas'            => \App\Http\Middleware\RedirectUnlessHaveAccessSagas::class,
        'accessUsuarios'         => \App\Http\Middleware\RedirectUnlessHaveAccessUsuarios::class,
        'accessEpisodios'        => \App\Http\Middleware\RedirectUnlessHaveAccessEpisodios::class,
        'accessEspeciais'        => \App\Http\Middleware\RedirectUnlessHaveAccessEspeciais::class,
        'accessFilmes'           => \App\Http\Middleware\RedirectUnlessHaveAccessFilmes::class,
        'accessOvas'             => \App\Http\Middleware\RedirectUnlessHaveAccessOvas::class,
        'accessFormatos'         => \App\Http\Middleware\RedirectUnlessHaveAccessFormatos::class,
        'accessPlataformas'      => \App\Http\Middleware\RedirectUnlessHaveAccessPlataformas::class,
        'accessJogos'            => \App\Http\Middleware\RedirectUnlessHaveAccessJogos::class,
        'accessHentais'          => \App\Http\Middleware\RedirectUnlessHaveAccessHentais::class,
        'accessOsts'             => \App\Http\Middleware\RedirectUnlessHaveAccessOsts::class,
        'accessVolumes'          => \App\Http\Middleware\RedirectUnlessHaveAccessVolumes::class,
        'accessCapitulos'        => \App\Http\Middleware\RedirectUnlessHaveAccessCapitulos::class,
        'accessNoticias'         => \App\Http\Middleware\RedirectUnlessHaveAccessNoticias::class,
    ];
}