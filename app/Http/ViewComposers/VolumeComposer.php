<?php

namespace App\Http\ViewComposers;

use App\Models\InformacoesUtilizador;
use App\Models\Tag;
use Illuminate\Contracts\View\View;

class VolumeComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $scanlatorsList = Tag::DeTipo(Tag::$TIPO_SCANLATOR)->lists('nome', 'id');
        $uploadersList = InformacoesUtilizador::lists('nick', 'user_id');

        $view->with('uploadersList', $uploadersList);
        $view->with('scanlatorsList', $scanlatorsList);
    }
}