<?php

namespace App\Http\ViewComposers;

use App\Models\Formato;
use App\Models\InformacoesUtilizador;
use App\Models\Tag;
use Illuminate\Contracts\View\View;

class FormatoMidiaComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $modelName = uniqid("model_");
        $fansubsList = Tag::DeTipo(Tag::$TIPO_FANSUB)->lists('nome', 'id');
        $audiosList = Tag::DeTipo(Tag::$TIPO_AUDIO)->lists('nome', 'id');
        $legendasList = Tag::DeTipo(Tag::$TIPO_LEGENDA)->lists('nome', 'id');
        $tamanhosList = Tag::DeTipo(Tag::$TIPO_TAMANHO)->lists('nome', 'id');
        $extencoesList = Tag::DeTipo(Tag::$TIPO_EXTENCAO)->lists('nome', 'id');
        $uploadersList = InformacoesUtilizador::lists('nick', 'user_id');
        $formatosList = Formato::lists('descricao','id');
        $formatosImagemList = [];
        foreach ( Formato::all() as $formato )
            $formatosImagemList[] = ['id' => $formato->id, 'link' => $formato->getImagem()->getLink()];

        $view->with('modelName', $modelName);
        $view->with('fansubsList', $fansubsList);
        $view->with('audiosList', $audiosList);
        $view->with('legendasList', $legendasList);
        $view->with('tamanhosList', $tamanhosList);
        $view->with('extencoesList', $extencoesList);
        $view->with('uploadersList', $uploadersList);
        $view->with('formatosList', $formatosList);
        $view->with('formatosImagemList', $formatosImagemList);
    }
}