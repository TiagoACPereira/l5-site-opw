<?php

namespace App\Http\ViewComposers;

use App\Models\InformacoesUtilizador;
use App\Models\Tag;
use Illuminate\Contracts\View\View;

class HentaiComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $tamanhosList = Tag::DeTipo(Tag::$TIPO_TAMANHO)->lists('nome', 'id');
        $scansList = Tag::DeTipo(Tag::$TIPO_SCAN)->lists('nome', 'id');
        $idiomasList = Tag::DeTipo(Tag::$TIPO_IDIOMA)->lists('nome', 'id');
        $formatosList = Tag::DeTipo(Tag::$TIPO_FORMATO)->lists('nome', 'id');
        $uploadersList = InformacoesUtilizador::lists('nick', 'user_id');

        $view->with('tamanhosList', $tamanhosList);
        $view->with('idiomasList', $idiomasList);
        $view->with('formatosList', $formatosList);
        $view->with('scansList', $scansList);
        $view->with('uploadersList', $uploadersList);
    }
}