<?php

namespace App\Http\ViewComposers;

use App\Models\InformacoesUtilizador;
use App\Models\Tag;
use Illuminate\Contracts\View\View;

class CapituloComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $scanlatorsList = Tag::DeTipo(Tag::$TIPO_SCANLATOR)->lists('nome', 'id');
        $uploadersList = InformacoesUtilizador::lists('nick', 'user_id');

        $view->with('scanlatorsList', $scanlatorsList);
        $view->with('uploadersList', $uploadersList);
    }
}