<?php
use App\Models\Imagem;
use Illuminate\Support\Facades\Config;

if (!function_exists('gerar_imagem_cache_url')) {
    function gerar_imagem_cache_url(Imagem $imagem)
    {
        if ($imagem->imgur) {
            return $imagem->nome;
        }

        return route('imagecache', [
            'template' => 'original',
            'filename' => $imagem->nome
        ]);
    }
}

if (!function_exists('path_imagem_cache_url')) {
    function path_imagem_cache_url($url)
    {
        if (!isUrlNosso($url)) {
            return '';
        }

        return path_imagem_cache_filename(file_url_name($url));
    }
}

if (!function_exists('path_imagem_cache_filename')) {
    function path_imagem_cache_filename($filename)
    {
        if ($filename == '' || $filename == null) {
            return null;
        }

        foreach (config('imagecache.paths') as $path) {
            $image_path = $path . '/' . str_replace('..', '', $filename);
            if (file_exists($image_path) && is_file($image_path)) {
                return $image_path;
            }
        }

        return null;
    }
}