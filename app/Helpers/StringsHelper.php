<?php
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

if (!function_exists('str_starts_with')) {
    function str_starts_with($haystack, $needle)
    {
        return strpos($haystack, $needle) === 0;
    }
}

if (!function_exists('str_ends_with')) {
    function str_ends_with($haystack, $needle)
    {
        return strrpos($haystack, $needle) + strlen($needle) === strlen($haystack);
    }
}

if (!function_exists('my_domain')) {
    function my_domain()
    {
        $domain = Request::root();

        // in case scheme relative URI is passed, e.g., //www.google.com/
        $domain = trim($domain, '/');

        // If scheme not included, prepend it
        if (!preg_match('#^http(s)?://#', $domain)) {
            $domain = 'http://' . $domain;
        }

        $urlParts = parse_url($domain);

        // remove www
        $domain = preg_replace('/^www\./', '', $urlParts['host']);

        return $domain;
    }
}
if (!function_exists('isUrlImage')) {
    function isUrlImage($url)
    {
        return @getimagesize($url);
    }
}

if (!function_exists('isUrlNosso')) {
    function isUrlNosso($src)
    {
        if (str_contains($src, my_domain())) {
            return true;
        }

        return false;
    }
}

/**
 * @param Carbon $data
 * @return String
 */
function date_diffForHumans_pt_BR(Carbon $data)
{
//    Carbon::setLocale('pt_BR');
    return $data->diffForHumans();
}