<?php

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

if (!function_exists('criarDirSeNaoExistir')) {
    function criarDirSeNaoExistir($caminho, $recursivo = false)
    {
        if (!File::exists(public_path($caminho))) {
            //cria todas as pastas necessárias
            mkdir(public_path($caminho), 0777, $recursivo);
        }

        return $caminho;
    }
}

if (!function_exists('download_temp_image')) {
    function download_temp_image($url)
    {
        if (!isUrlImage($url)) {
            return null;
        }

        criarDirSeNaoExistir('upload/temp/', true);

        if (preg_match('/data:image/', $url)) {
            preg_match('/data:image\/(?<mime>.*?)\;/', $url, $groups);
            $extensao = $groups['mime'];
        }else{
            $extensao = file_url_extension($url);
        }

        $filename = uniqid(). '.' . $extensao;
        $filepath = public_path('upload/temp/' . $filename);

        if(Str::equals('GIF', Str::upper($extensao))){
            $contentOrFalseOnFailure = file_get_contents($url);
            if($contentOrFalseOnFailure != false)
                file_put_contents($filepath, $contentOrFalseOnFailure);
        } else {
            Image::make($url)->save($filepath);
        }

        return $filepath;
    }
}

if (!function_exists('file_url_extension')) {
    function file_url_extension($url)
    {
        return pathinfo($url, PATHINFO_EXTENSION);
    }
}

if (!function_exists('file_url_name')) {
    function file_url_name($url, $extension = true)
    {
        if (!$extension) {
            return pathinfo($url, PATHINFO_FILENAME);
        }

        return pathinfo($url, PATHINFO_BASENAME);
    }
}