<?php

use App\Services\ImgurImage;
use Illuminate\Support\Facades\File;

if (!function_exists('uploadToImgurFromUrl')) {
    function uploadToImgurFromUrl($url, $saveTemp = false)
    {
        if (isUrlNosso($url)) {
            $tempPath = path_imagem_cache_url($url);
        } else {
            $tempPath = download_temp_image($url);
        }

        if ($tempPath == '') {
            return null;
        }

        $dados = (new ImgurImage())->uploadFile($tempPath);

        if (!isUrlNosso($url) && !$saveTemp) {
            if(File::exists($tempPath))
                unlink($tempPath);
        } else {
            $dados['tempPath'] = $tempPath;
        }

        return $dados;
    }
}

if (!function_exists('isUrlImgur')) {
    function isUrlImgur($url)
    {
        return str_contains($url, config('imgur.imgur_url'));
    }
}