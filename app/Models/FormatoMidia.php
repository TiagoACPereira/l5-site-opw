<?php

namespace App\Models;

use App\Traits\Models\MirrorsTrait;
use App\Traits\Models\TagTrait;
use App\Traits\Models\UploadersTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormatoMidia extends Model
{

    use UploadersTrait, MirrorsTrait, TagTrait, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'formatosmidia';

    protected $fillable = [
        'formato_id',
        'uploaders',
        'index'
    ];

    public static $defaults = [];

    public function Formato()
    {
        return $this->belongsTo(Formato::class, 'formato_id', 'id');
    }

    public function Episodio()
    {
        return $this->morphedByMany(Episodio::class, 'formatomidiable');
    }

    public function Filme()
    {
        return $this->morphedByMany(Filme::class, 'formatomidiable');
    }

    public function Especial()
    {
        return $this->morphedByMany(Especial::class, 'formatomidiable');
    }

    public function Ova()
    {
        return $this->morphedByMany(Ova::class, 'formatomidiable');
    }

}
