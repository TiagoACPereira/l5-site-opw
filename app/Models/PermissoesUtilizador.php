<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissoesUtilizador extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'usersPermissions';

    protected $fillable = ['is_Admin', 'is_Revisor', 'is_Redator', 'is_Editor_Noticias', 'is_Midia_Uploader', 'is_Fan_Area'];

    public static $defaults = [
        'is_Admin'           => false,
        'is_Revisor'         => false,
        'is_Redator'         => false,
        'is_Editor_Noticias' => false,
        'is_Midia_Uploader'  => false,
        'is_Fan_Area'  => false
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }
}
