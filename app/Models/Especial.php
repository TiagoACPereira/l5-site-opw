<?php

namespace App\Models;

use App\Traits\Models\FormatoMidiaTrait;
use App\Traits\Models\ImagemTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Especial extends Model implements SluggableInterface
{

    use SluggableTrait;

    use PublishAtTrait;

    use ImagemTrait;

    use FormatoMidiaTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'especiais';

    protected $fillable = [
        'user_id',
        'titulo',
        'sub_titulo',
        'downloads',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update' => true,
    ];

    public static $defaults = [
        'titulo' => 'sem titulo',
        'sub_titulo' => 'sem sub titulo',
        'downloads' => 0
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function scopeOrdenarEspeciais($query)
    {
        $query->orderBy('sub_titulo', 'asc');
    }

}
