<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';

    protected $fillable = ['url', 'isDead'];

    public static $defaults = ['url' => 'google.com', 'isDead' => false];

    public function FormatoMidia()
    {
        return $this->morphedByMany(FormatoMidia::class, 'linkable');
    }

    public function Jogo()
    {
        return $this->morphedByMany(Jogo::class, 'linkable');
    }

    public function Hentai()
    {
        return $this->morphedByMany(Hentai::class, 'linkable');
    }

    public function Ost()
    {
        return $this->morphedByMany(Ost::class, 'linkable');
    }

    public function Volume()
    {
        return $this->morphedByMany(Volume::class, 'linkable');
    }

    public function Capitulo()
    {
        return $this->morphedByMany(Capitulo::class, 'linkable');
    }

}
