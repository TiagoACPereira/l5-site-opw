<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'tags';

    protected $fillable = [
        'tipo',
        'nome'
    ];

    public static $defaults = [
        'tipo' => 'não definido',
        'nome' => ''
    ];

    public static $TIPO_AUDIO = 'audio';
    public static $TIPO_SCAN = 'scan';
    public static $TIPO_SCANLATOR = 'scanlator';
    public static $TIPO_EXTENCAO = 'extencao';
    public static $TIPO_FANSUB = 'fansub';
    public static $TIPO_FORMATO = 'formato';
    public static $TIPO_IDIOMA = 'idioma';
    public static $TIPO_LEGENDA = 'legenda';
    public static $TIPO_TAMANHO = 'tamanho';


    public function scopeDeTipo($query, $tipo)
    {
        $query->where('tipo', $tipo);
    }

    public function FormatosMidia()
    {
        return $this->morphedByMany(FormatoMidia::class, 'tagable');
    }

    public function Jogos()
    {
        return $this->belongsToMany(Jogo::class, 'tagable');
    }

    public function Hentais()
    {
        return $this->belongsToMany(Hentai::class, 'tagable');
    }

    public function Osts()
    {
        return $this->belongsToMany(Ost::class, 'tagable');
    }

    public function Volumes()
    {
        return $this->belongsToMany(Volume::class, 'tagable');
    }

    public function Capitulos()
    {
        return $this->belongsToMany(Capitulo::class, 'tagable');
    }


}
