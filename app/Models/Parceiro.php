<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parceiro extends Model implements SluggableInterface
{

    use SluggableTrait;

    use ImagemTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'parceiros';

    protected $fillable = [
        'user_id',
        'nome',
        'slug',
        'url',
        'hist'
    ];

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update' => false,
    ];

    public static $defaults = [
        'nome' => '',
        'url' => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

}
