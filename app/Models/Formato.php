<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formato extends Model implements SluggableInterface
{

    use SluggableTrait;

    use ImagemTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'formatos';

    protected $fillable = [
        'user_id',
        'descricao'
    ];

    protected $sluggable = [
        'build_from' => 'descricao',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'descricao' => 'sem nome'
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function FormatosMidia()
    {
        return $this->hasMany(FormatoMidia::class, 'formato_id','id');
    }

}
