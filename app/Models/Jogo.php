<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\MirrorsTrait;
use App\Traits\Models\SluggableTrait;
use App\Traits\Models\TagTrait;
use App\Traits\Models\UploadersTrait;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jogo extends Model implements SluggableInterface
{

    use UploadersTrait, MirrorsTrait, TagTrait, SluggableTrait, ImagemTrait, SoftDeletes;

    protected $dates = ['deleted_at', 'data_lancamento'];

    protected $table = 'jogos';

    protected $fillable = [
        'plataforma_id',
        'user_id',
        'titulo',
        'data'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo' => 'sem nome',
        'data_lancamento' => null
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function Plataforma()
    {
        return $this->belongsTo(Plataforma::class, 'plataforma_id', 'id');
    }

    public function getPlataformaAttribute()
    {
        return $this->Plataforma()->firstOrFail();
    }

    public function getDataAttribute()
    {
        if ($this->data_lancamento != null) {
            return $this->data_lancamento->format('Y-m-d');
        }
        return null;
    }

    public function setDataAttribute($data)
    {
        $this->attributes['data_lancamento']  = Carbon::createFromFormat('Y-m-d', $data);
    }

}