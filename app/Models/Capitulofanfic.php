<?php

namespace App\Models;

use App\Traits\Models\ConteudoTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Capitulofanfic extends Model implements SluggableInterface
{

    use SluggableTrait;

    use ConteudoTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'capitulosfanfic';

    protected $fillable = [
        'user_id',
        'titulo',
        'numero',
        'link_original',
        'fanfic_id',
        'forceHost',
        'imgur'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo'     => '',
        'sub_titulo' => ''
    ];

    public function Fanfic()
    {
        return $this->belongsTo(Fanfic::class, 'fanfic_id', 'id');
    }

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function scopeOrdenarCapitulos($query)
    {
        $query->orderBy('numero', 'asc');
    }

}
