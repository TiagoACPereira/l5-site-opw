<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Saga extends Model implements SluggableInterface
{

    use SluggableTrait;

    use ImagemTrait;

    use PublishAtTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'sagas';

    protected $fillable = [
        'user_id',
        'titulo',
        'numero',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo' => null,
        'numero' => 0
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function Episodios()
    {
        return $this->hasMany(Episodio::class, 'saga_id', 'id');
    }

    public function numeroEpisodios()
    {
        return $this->Episodios()->ordenarEpisodios()->count();
    }

    public function scopeOrdenarSagas($query)
    {
        $query->orderBy('numero', 'asc');
    }

}
