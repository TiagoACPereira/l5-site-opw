<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fanfictag extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'tags_fanfics';

    protected $fillable = [
        'nome'
    ];

    public static $defaults = [
        'nome'        => ''
    ];

    public function Fanfics()
    {
        return $this->belongsToMany(Fanfic::class, 'fanfic_tag')->withTimestamps();
    }

}
