<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\MirrorsTrait;
use App\Traits\Models\SluggableTrait;
use App\Traits\Models\TagTrait;
use App\Traits\Models\UploadersTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hentai extends Model implements SluggableInterface
{

    use UploadersTrait, MirrorsTrait, TagTrait, SluggableTrait, ImagemTrait, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'hentais';

    protected $fillable = [
        'user_id',
        'titulo',
        'sub_titulo'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo' => 'sem titulo',
        'subtitulo' => 'sem sub titulo'
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }
}