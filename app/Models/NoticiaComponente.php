<?php

namespace App\Models;

use App\Traits\Controllers\ImagemTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoticiaComponente extends Model
{

    protected $imagePrefix = 'noticia_';
    protected $imageFolder = 'upload/noticias/';
    protected $deletedImageFolder = 'upload/noticias/deleted/';

    use ImagemTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'noticias_componentes';

    protected $fillable = [
        'noticia_id',
        'posicao',
        'noticia_completa',
        'componente',
        'tipo_componente'
    ];

    public static $defaults = [
        'posicao'          => '',
        'noticia_completa' => false
    ];

    public function Noticia()
    {
        return $this->belongsTo(Noticia::class, 'noticia_id', 'id');
    }

    /**
     * Devolve o modelo correspondente a este componente
     *
     * @return mixed|null
     */
    public function getComponente()
    {
        switch($this->tipo_componente)
        {
            case Conteudo::class:
                return $this->Conteudo();
            case Episodio::class:
                return $this->Episodio();
            default:
                return null;
        }
    }

    public function getFormAttribute()
    {
        $componente = $this->getComponente();
        if($componente != null)
            return $componente->formComponenteNoticia($this);
        else
            return '';
    }

    public function setComponenteAttribute($dados)
    {
        switch($this->tipo_componente)
        {
            case Conteudo::class:
                $conteudo = $this->Conteudo(false);

                if($conteudo == null)
                    $conteudo = $this->RelacaoConteudo()->create(Conteudo::$defaults);

                $imagens = $conteudo->getImagens();

                //procura por imagens removidas e apaga-as
                foreach ($imagens as $imagem) {
                    if ($imagem != null && !str_contains($conteudo->strConteudo, $imagem->nome)) {
                        $imagem->delete();
                        $this->deleteUploadedImagem($imagem->nome);
                    }
                }

                $this->saveImagens($conteudo, $dados['strConteudo'], $dados['imgur'], $dados['forceHost']);

                break;

            case Episodio::class:
                $episodio = $this->Episodio();
                if($episodio == null)
                {
                    //caso não tenha ainda associado um episodio
                    $episodio = Episodio::where('id', $dados)->first();
                    $this->RelacaoEpisodio()->attach($episodio);
                }else if($episodio->id != $dados){
                    //caso tenha alterado o episodio
                    $this->RelacaoEpisodio()->detach($episodio);
                    $episodio = Episodio::where('id', $dados)->first();
                    $this->RelacaoEpisodio()->attach($episodio);
                }
                break;
        }
    }

    public function deleteComponent()
    {
        switch($this->tipo_componente) {
            case Conteudo::class:
                $conteudo = $this->getComponente();
                $imagens = $conteudo->getImagens();

                foreach ($imagens as $imagem) {
                    $imagem->delete();
                    $this->deleteUploadedImagem($imagem->nome);
                }

                $conteudo->delete();
                break;
        }
    }

    /*
    |
    |   Relações e funções para ir o modelo correspondente deste Componente
    |
    |*/

    public function RelacaoConteudo()
    {
        return $this->morphedByMany(Conteudo::class, 'noticia_componenteable');
    }

    public function Conteudo()
    {
        return $this->RelacaoConteudo()->first();
    }

    public function RelacaoEpisodio()
    {
        return $this->morphedByMany(Episodio::class, 'noticia_componenteable');
    }

    public function Episodio()
    {
        return $this->RelacaoEpisodio()->first();
    }

}
