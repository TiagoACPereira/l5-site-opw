<?php

namespace App\Models;

use App\Traits\Models\FormatoMidiaTrait;
use App\Traits\Models\ImagemTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filme extends Model implements SluggableInterface
{

    use SluggableTrait;

    use PublishAtTrait;

    use ImagemTrait;

    use FormatoMidiaTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'filmes';

    protected $fillable = [
        'user_id',
        'titulo',
        'numero',
        'downloads',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update' => true,
    ];

    public static $defaults = [
        'titulo' => 'sem titulo',
        'numero' => 0,
        'downloads' => 0
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function scopeOrdenarFilmes($query)
    {
        $query->orderBy('numero', 'asc');
    }

}