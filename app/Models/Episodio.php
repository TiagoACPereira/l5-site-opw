<?php

namespace App\Models;

use App\Traits\Models\FormatoMidiaTrait;
use App\Traits\Models\ImagemTrait;
use App\Traits\Models\NoticiaComponenteTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Episodio extends Model implements SluggableInterface
{

    use SluggableTrait;

    use PublishAtTrait;

    use ImagemTrait;

    use FormatoMidiaTrait;

    use NoticiaComponenteTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'episodios';

    protected $fillable = [
        'user_id',
        'titulo',
        'numero',
        'str_numero',
        'is_Duplo',
        'filler_tag',
        'downloads',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'str_numero',
        'save_to'    => 'slug',
        'on_update' => true,
    ];

    public static $defaults = [
        'titulo' => 'sem titulo',
        'numero' => 0,
        'str_numero' => '0-0',
        'is_Duplo' => false,
        'filler_tag' => false,
        'downloads' => 0
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function RelacaoSaga()
    {
        return $this->belongsTo(Saga::class, 'saga_id', 'id');
    }

    public function getSagaAttribute()
    {
        return $this->RelacaoSaga()->firstOrFail();
    }

    public function scopeOrdenarEpisodios($query)
    {
        $query->orderBy('numero', 'asc');
    }

    public function isDuplo()
    {
        return ($this->is_Duplo == null)? 0 : $this->is_Duplo;
    }

    public function isFiller()
    {
        return ($this->filler_tag == null)? 0 : $this->filler_tag;
    }

}
