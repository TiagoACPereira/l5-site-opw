<?php

namespace App\Models;

use Alaouy\Youtube\Facades\Youtube;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CanalVlog extends Model implements SluggableInterface
{

    use SluggableTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'canaisvlog';

    protected $fillable = [
        'user_id',
        'nome',
        'channel',
        'channel_id',
        'channel_info'
    ];

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo'     => '',
        'channel' => '',
        'channel_id' => false,
        'channel_info'
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function setChannelInfoAttribute($video_info){
        $this->attributes['channel_info'] = serialize($video_info);
    }

    public function getChannelInfoAttribute(){
        return unserialize($this->attributes['channel_info']);
    }

    public function isCanalValido()
    {
        if($this->channel_id)
        {
            return Youtube::getChannelById($this->channel);
        } else {
            return Youtube::getChannelByName($this->channel);
        }

    }

}