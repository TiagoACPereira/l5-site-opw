<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plataforma extends Model implements SluggableInterface
{

    use SluggableTrait;

    use ImagemTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'plataformas';

    protected $fillable = [
        'user_id',
        'tipo',
        'nome',
        'publish_at'
    ];

    public static $TIPO_JOGO = 'Jogo';
    public static $TIPO_ROM = 'Rom';

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update' => false,
    ];

    public static $defaults = [
        'tipo'  => null,
        'nome'  => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function scopeJogos($query)
    {
        $query->where('tipo', Plataforma::$TIPO_JOGO);
    }

    public function scopeROMS($query)
    {
        $query->where('tipo', Plataforma::$TIPO_ROM);
    }

    public function Jogos()
    {
        return $this->hasMany(Jogo::class, 'plataforma_id','id');
    }

    public function getNumeroJogosAttribute()
    {
        return $this->Jogos()->count();
    }
}
