<?php

namespace App\Models;

use App\Traits\Models\ConteudoTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model implements SluggableInterface
{

    use SluggableTrait;

    use PublishAtTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'noticias';

    protected $fillable = [
        'user_id',
        'titulo',
        'forceHost',
        'imgur',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo'     => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function RelacaoComponentes()
    {
        return $this->hasMany(NoticiaComponente::class, 'noticia_id', 'id');
    }

    public function getComponentesAttribute()
    {
        return $this->RelacaoComponentes()->orderBy('posicao')->get();
    }

    public function getNumeroComponentesAttribute()
    {
        return $this->RelacaoComponentes()->count();
    }

    public function isComponente($id)
    {
        return $this->RelacaoComponentes()->where('id', $id)->count() == 1;
    }

    /*
    |
    |   Funções para chamar as relações e todos os componentes de um certo tipo
    |
    |*/
    public function RelacaoConteudos()
    {
        return $this->hasManyThrough(Conteudo::class, NoticiaComponente::class, 'noticia_id', 'id');
    }

    public function getConteudos()
    {
        return $this->RelacaoConteudos()->get();
    }

    public function RelacaoEpisodios()
    {
        return $this->hasManyThrough(Episodio::class, NoticiaComponente::class, 'noticia_id', 'id');
    }

    public function getEpisodios()
    {
        return $this->RelacaoEpisodios()->get();
    }


    /*
    |
    |   Funções para por todos os componentes conteudos com os dados corretos
    |
    |*/

    public function getImgurAttribute()
    {
        $conteudo = $this->RelacaoConteudos()->first();
        if($conteudo == null)
            return 0;
        else
            return $conteudo->attributes['imgur'];
    }

    public function setImgurAttribute($imgur)
    {
        foreach ($this->getConteudos() as $conteudo) {
            $conteudo->attributes['imgur'] = $imgur;
        }
    }

    public function setForceHostAttribute($forceHost)
    {
        foreach ($this->getConteudos() as $conteudo) {
            $conteudo->attributes['forceHost'] = $forceHost;
        }
    }

    public function getForceHostAttribute()
    {
        $conteudo = $this->RelacaoConteudos()->first();
        if($conteudo == null)
            return 0;
        else{
            return $conteudo->attributes['forceHost'];
        }
    }

    public function isImgur()
    {
        return $this->imgur;
    }

    public function isMyHost()
    {
        return !$this->imgur;
    }
}
