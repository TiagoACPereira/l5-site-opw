<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Utilizador extends Authenticatable
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'users';

    protected $fillable = ['active', 'nome', 'login', 'email', 'password', 'nick', 'dataNascimento', 'twitter', 'facebook', 'googleplus', 'youtube'];

    protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    //▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼ INFORMACOES ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼

    public function Informacoes()
    {
        return $this->hasOne(InformacoesUtilizador::class, 'user_id', 'id');
    }

    public function setNickAttribute($nick)
    {
        $this->Informacoes->update([ 'nick' => $nick ]);
    }

    public function getNickAttribute()
    {
        return $this->Informacoes->nick;
    }

    public function setDataNascimentoAttribute($dataNascimento)
    {
        $this->Informacoes->update([ 'dataNascimento' => $dataNascimento ]);
    }

    public function getDataNascimentoAttribute()
    {
        return $this->Informacoes->dataNascimento;
    }

    public function setTwitterAttribute($twitter)
    {
        $this->Informacoes->update([ 'twitter' => $twitter ]);
    }

    public function getTwitterAttribute()
    {
        return $this->Informacoes->twitter;
    }

    public function setFacebookAttribute($facebook)
    {
        $this->Informacoes->update([ 'facebook' => $facebook ]);
    }

    public function getFacebookAttribute()
    {
        return $this->Informacoes->facebook;
    }

    public function setGoogleplusAttribute($googleplus)
    {
        $this->Informacoes->update([ 'googleplus' => $googleplus ]);
    }

    public function getGoogleplusAttribute()
    {
        return $this->Informacoes->googleplus;
    }

    public function setYoutubeAttribute($youtube)
    {
        $this->Informacoes->update([ 'youtube' => $youtube ]);
    }

    public function getYoutubeAttribute()
    {
        return $this->Informacoes->youtube;
    }

    //▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲ INFORMACOES ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
    //••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    //▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼  PERMISSOES ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼

    public function Permissoes()
    {
        return $this->hasOne(PermissoesUtilizador::class, 'user_id', 'id');
    }

    public function temPermissao($permissao)
    {
        return (bool) $this->Permissoes->{$permissao};
    }

    public function isAdmin()
    {
        return $this->temPermissao('is_Admin');
    }

    public function isRevisor()
    {
        return $this->temPermissao('is_Revisor');
    }

    public function isRedator()
    {
        return $this->temPermissao('is_Redator');
    }

    public function isEditorNoticias()
    {
        return $this->temPermissao('is_Editor_Noticias');
    }

    public function isMidiaUploader()
    {
        return $this->temPermissao('is_Midia_Uploader');
    }

    public function isFanArea()
    {
        return $this->temPermissao('is_Fan_Area');
    }

    //▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲ PERMISSOES  ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
    //••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    //▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼  AREA FANS ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼

    public function Teorias()
    {
        return $this->hasMany(Teoria::class, 'user_id', 'id');
    }

    public function Letras()
    {
        return $this->hasMany(Letra::class, 'user_id', 'id');
    }

    public function CanaisVlog()
    {
        return $this->hasMany(CanalVlog::class, 'user_id','id');
    }

    public function Amvs()
    {
        return $this->hasMany(Amv::class, 'user_id','id');
    }

    public function Fanfics()
    {
        return $this->hasMany(Fanfic::class, 'user_id','id');
    }

    public function CapitulosFanfics()
    {
        return $this->hasMany(Capitulofanfic::class, 'user_id','id');
    }

    //▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲ AREA FANS ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
    //••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    //▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼ MULTIMIDA ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼

    public function Sagas()
    {
        return $this->hasMany(Saga::class, 'user_id','id');
    }

    public function Formatos()
    {
        return $this->hasMany(Formato::class, 'user_id','id');
    }

    public function FormatoMidia()
    {
        return $this->morphedByMany(FormatoMidia::class, 'uploaderable');
    }

    public function Episodios()
    {
        return $this->hasMany(Episodio::class, 'user_id', 'id');
    }

    public function Especiais()
    {
        return $this->hasMany(Especial::class, 'user_id', 'id');
    }

    public function Filmes()
    {
        return $this->hasMany(Filme::class, 'user_id', 'id');
    }

    public function Ovas()
    {
        return $this->hasMany(Ova::class, 'user_id', 'id');
    }

    public function Plataformas()
    {
        return $this->hasMany(Plataforma::class, 'user_id','id');
    }

    public function Jogos()
    {
        return $this->hasMany(Jogo::class, 'user_id', 'id');
    }

    public function JogosUploaders()
    {
        return $this->morphedByMany(Jogo::class, 'uploaderable');
    }

    public function Hentais()
    {
        return $this->hasMany(Hentai::class, 'user_id', 'id');
    }

    public function Osts()
    {
        return $this->hasMany(Ost::class, 'user_id', 'id');
    }

    public function Volumes()
    {
        return $this->hasMany(Volume::class, 'user_id', 'id');
    }

    public function Capitulos()
    {
        return $this->hasMany(Capitulo::class, 'user_id', 'id');
    }

    public function Noticias()
    {
        return $this->hasMany(Noticia::class, 'user_id', 'id');
    }

    //▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲ MULTIMIDA ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
    //••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    public function Slides()
    {
        return $this->hasMany(Slide::class, 'user_id', 'id');
    }

    public function Parceiros()
    {
        return $this->hasMany(Parceiro::class, 'user_id', 'id');
    }

    public function Infos()
    {
        return $this->hasMany(Info::class, 'user_id', 'id');
    }

    public function Tutoriais()
    {
        return $this->hasMany(Tutorial::class, 'user_id', 'id');
    }

}
