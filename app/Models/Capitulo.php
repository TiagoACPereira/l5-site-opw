<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\MirrorsTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use App\Traits\Models\TagTrait;
use App\Traits\Models\UploadersTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Capitulo extends Model implements SluggableInterface
{

    use UploadersTrait, MirrorsTrait, TagTrait, SluggableTrait, ImagemTrait, PublishAtTrait, SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'capitulos';

    protected $fillable = [
        'user_id',
        'volume_id',
        'titulo',
        'numero',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'numero',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo' => 'sem titulo',
        'numero' => 0
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }
    public function Volume()
    {
        return $this->belongsTo(Volume::class, 'volume_id', 'id');
    }

    public function scopeOrdenarCapitulos($query)
    {
        $query->orderBy('numero', 'asc');
    }

}
