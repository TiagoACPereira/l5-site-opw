<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imagem extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'imagens';

    protected $fillable = ['nome', 'imgur', 'deletehash'];

    public static $defaults = ['nome' => null, 'imgur' => false, 'deletehash' => ''];

    public function Slide()
    {
        return $this->morphedByMany(Slide::class, 'imageable');
    }

    public function Parceiro()
    {
        return $this->morphedByMany(Parceiro::class, 'imageable');
    }

    public function avatarUtilizador()
    {
        return $this->morphedByMany(InformacoesUtilizador::class, 'imageable');
    }

    public function Saga()
    {
        return $this->morphedByMany(Saga::class, 'imageable');
    }

    public function Formato()
    {
        return $this->morphedByMany(Formato::class, 'imageable');
    }

    public function getLink($template = 'original')
    {
        if ($this->attributes['imgur']) {
            return $this->attributes['nome'];
        } else {
            return route('imagecache', [
                'template' => $template,
                'filename' => $this->attributes['nome']
            ]);
        }
    }

}
