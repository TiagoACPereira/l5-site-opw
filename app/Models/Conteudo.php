<?php

namespace App\Models;

use App\Traits\Models\NoticiaComponenteTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conteudo extends Model
{

    use NoticiaComponenteTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'conteudos';

    protected $fillable = [
        'strConteudo',
        'forceHost',
        'imgur'
    ];

    public static $defaults = [
        'strConteudo' => '',
        'forceHost'       => false,
        'imgur'       => false
    ];

    public function Info()
    {
        return $this->morphedByMany(Info::class, 'conteudable');
    }

    public function Teoria()
    {
        return $this->morphedByMany(Teoria::class, 'conteudable');
    }

    public function Letra()
    {
        return $this->morphedByMany(Letra::class, 'conteudable');
    }

    public function Tutorial()
    {
        return $this->morphedByMany(Tutorial::class, 'conteudable');
    }

    public function Capitulofanfic()
    {
        return $this->morphedByMany(Capitulofanfic::class, 'conteudable');
    }

    public function setConteudoAttribute($strConteudo)
    {
        $this->attributes['strConteudo'] = $strConteudo;
    }

    public function RelacaoImagem()
    {
        //imageable corresponde ao inicio do nome dado na coluna 'imageable_id' e 'imageable_type'
        return $this->morphToMany(Imagem::class, 'imageable');
    }

    public function getImagens()
    {
        return $this->RelacaoImagem()->getResults();
    }

    public function getImagem($nomeImagem)
    {
        return $this->RelacaoImagem()->where('nome', $nomeImagem)->get()->first();
    }

    public function isImagem($nomeImagem)
    {
        return !$this->RelacaoImagem()->where('nome', $nomeImagem)->get()->isEmpty();
    }

    public function formComponenteNoticia(NoticiaComponente $noticiaComponente)
    {
        $key = $noticiaComponente->id;
        $noticia_completa = $noticiaComponente->noticia_completa;
        $strConteudo = $this->strConteudo;
        return view('sistema.noticiascomponentes.partials.conteudo-form',
            compact('key', 'strConteudo', 'noticia_completa'));
    }
}
