<?php

namespace App\Models;

use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fanfic extends Model implements SluggableInterface
{

    use SluggableTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'fanfics';

    protected $fillable = [
        'user_id',
        'titulo',
        'autor',
        'link_original'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo'        => '',
        'autor'         => '',
        'link_original' => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    //▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼ Status ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
    public function FanficStatus()
    {
        return $this->hasOne(FanficStatus::class, 'fanfic_id', 'id');
    }

    public function temStatus($status)
    {
        if($this->FanficStatus == null)
            return false;
        return (bool) $this->FanficStatus->{$status};
    }

    public function isEmAndamento()
    {
        if($this->FanficStatus == null)
            return true;
        return $this->temStatus('em_andamento');
    }

    public function isConcluida()
    {
        return $this->temStatus('concluida');
    }

    //▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲  Status ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
    //••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    //▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼  TAG  ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼

    public function FanficTags()
    {
        return $this->belongsToMany(Fanfictag::class, 'fanfic_tag')->withTimestamps();
    }

    public function getTagsAttribute()
    {
        return $this->FanficTags()->lists('id')->toArray();
    }

    public function allExistingTags()
    {
        return Fanfictag::lists('nome', 'id');
    }

    //▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲  Status ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
    //••••••••••••••••••••••••••••••••••••••••••••••••••••••••••

    public function Capitulos()
    {
        return $this->hasMany(Capitulofanfic::class, 'fanfic_id', 'id')->ordenarcapitulos();
    }

    public function numeroCapitulos()
    {
        return $this->Capitulos()->ordenarcapitulos()->count();
    }

}
