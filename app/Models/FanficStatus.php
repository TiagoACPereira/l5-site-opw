<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FanficStatus extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'fanficsstatus';

    protected $fillable = ['em_andamento', 'concluida'];

    public static $defaults = [
        'em_andamento' => false,
        'concluida'    => false
    ];

    public function Fanfic()
    {
        return $this->belongsTo(Fanfic::class, 'fanfic_id', 'id');
    }

}
