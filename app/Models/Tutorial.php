<?php

namespace App\Models;

use App\Traits\Models\ConteudoTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tutorial extends Model implements SluggableInterface
{

    use SluggableTrait;

    use PublishAtTrait;

    use ConteudoTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'tutoriais';

    protected $fillable = [
        'user_id',
        'titulo',
        'sub_titulo',
        'forceHost',
        'imgur',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'titulo'     => '',
        'sub_titulo' => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

}
