<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slide extends Model implements SluggableInterface
{

    use SluggableTrait;

    use PublishAtTrait;

    use ImagemTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at'];

    protected $table = 'slides';

    protected $fillable = [
        'user_id',
        'informacao',
        'url',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'informacao',
        'save_to'    => 'slug',
        'on_update' => false,
    ];

    public static $defaults = [
        'informacao' => null,
        'url'        => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

}
