<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\MirrorsTrait;
use App\Traits\Models\PublishAtTrait;
use App\Traits\Models\SluggableTrait;
use App\Traits\Models\TagTrait;
use App\Traits\Models\UploadersTrait;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Volume extends Model implements SluggableInterface
{

    use SluggableTrait, TagTrait, ImagemTrait, PublishAtTrait, MirrorsTrait, UploadersTrait, SoftDeletes;

    protected $dates = ['deleted_at', 'publish_at', 'data_lancamento'];

    protected $table = 'volumes';

    protected $fillable = [
        'user_id',
        'numero',
        'str_numero',
        'data',
        'publish_at'
    ];

    protected $sluggable = [
        'build_from' => 'numero',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];

    public static $defaults = [
        'str_numero' => 'Volume 0',
        'numero' => 0,
        'data_lancamento' => null
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function Capitulos()
    {
        return $this->hasMany(Capitulo::class, 'volume_id', 'id');
    }

    public function numeroCapitulos()
    {
        return $this->Capitulos()->ordenarCapitulos()->count();
    }

    public function scopeOrdenarVolumes($query)
    {
        $query->orderBy('numero', 'asc');
    }

    public function getDataAttribute()
    {
        if ($this->data_lancamento != null) {
            return $this->data_lancamento->format('Y-m-d');
        }
        return null;
    }

    public function setDataAttribute($data)
    {
        $this->attributes['data_lancamento']  = Carbon::createFromFormat('Y-m-d', $data);
    }

}