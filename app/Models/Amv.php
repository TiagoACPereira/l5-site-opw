<?php

namespace App\Models;

use Alaouy\Youtube\Facades\Youtube;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Amv extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'amvs';

    protected $fillable = [
        'user_id',
        'video_id',
        'video_info'
    ];

    public static $defaults = [
        'video_id'   => '',
        'video_info' => ''
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function setVideoInfoAttribute($video_info){
        $this->attributes['video_info'] = serialize($video_info);
    }

    public function getVideoInfoAttribute(){
        return unserialize($this->attributes['video_info']);
    }

    public function getTituloAttribute()
    {
        if ($this->video_info) {
            return $this->video_info->snippet->title;
        } else {
            return '';
        }
    }

    public function getLinkAttribute()
    {
        if ($this->video_info) {
            return 'https://www.youtube.com/watch?v=' . $this->video_id;
        } else {
            return '';
        }
    }

    public function getImagemDefaultAttribute()
    {
        if ($this->video_info) {
            return $this->video_info->snippet->thumbnails->default->url;
        } else {
            return '';
        }
    }

    public function getAutorAttribute()
    {
        if ($this->video_info) {
            return $this->video_info->snippet->channelTitle;
        } else {
            return '';
        }
    }

}
