<?php

namespace App\Models;

use App\Traits\Models\ImagemTrait;
use App\Traits\Models\SluggableTrait;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformacoesUtilizador extends Model implements SluggableInterface
{

    use SluggableTrait;

    use ImagemTrait;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'dataNascimento'];

    protected $table = 'usersInformations';

    protected $fillable = [
        'nick',
        'dataNascimento',
        'twitter',
        'facebook',
        'googleplus',
        'youtube'
    ];

    protected $sluggable = [
        'build_from' => 'nick',
        'save_to'    => 'slug',
    ];

    public static $defaults = [
        'nick'           => null,
        'dataNascimento' => null,
        'twitter'        => null,
        'facebook'       => null,
        'googleplus'     => null,
        'youtube'        => null
    ];

    public function Utilizador()
    {
        return $this->belongsTo(Utilizador::class, 'user_id', 'id');
    }

    public function setDataNascimentoAttribute($data)
    {
        $this->attributes['dataNascimento'] = ($data != '') ? Carbon::createFromFormat('Y-m-d', $data) : null;
    }

    public function getDataNascimentoAttribute($date)
    {
        return ($date != null) ? (new Carbon($date))->format('Y-m-d') : null;
    }

    public function getAvatar()
    {
        return $this->RelacaoImagem()->firstOrFail();
    }

}
