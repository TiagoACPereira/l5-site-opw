<?php

namespace App\Contracts;

interface ImageUpload
{
    public function uploadFile($path);

    public function delete($string);
}
