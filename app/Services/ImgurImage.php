<?php

namespace App\Services;

use App\Contracts\ImageUpload;
use Imgur\Client;

class ImgurImage implements ImageUpload{

    public function __construct()
    {
        $this->client = new Client();
        $this->client->setOption('client_id', config('imgur.client_id'));
        $this->client->setOption('client_secret', config('imgur.client_secret'));
    }

    public function delete($hash)
    {
        $this->client->api('image')->deleteImage($hash);
    }

    public function uploadFile($path)
    {
        $basic = $this->client->api('image')->upload([
            'image' => $path,
            'type'  => 'file'
        ]);

        return $basic->getdata();
    }

}
